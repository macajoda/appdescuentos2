﻿Imports System.Net
Imports System.Security.Cryptography
Imports System.IO
'Imports gestioncorreo.FormsAuth
Imports System.DirectoryServices
Public Class Login
    Inherits System.Web.UI.Page
    'Dim ObjUniversal As New CLUniversal
    Dim ObjUniversal As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Session.Abandon()

            '--------------------------------------Captura La IP y el nombre del equipo
            Dim strHostName As String = Dns.GetHostName()
            Dim ipEntry As IPHostEntry = Dns.GetHostEntry(strHostName)

            ViewState("IPAddress") = Convert.ToString(ipEntry.AddressList(ipEntry.AddressList.Length - 1))
            ViewState("HostName") = Convert.ToString(ipEntry.HostName)
            '--------------------------------------FIN Captura La IP y el nombre del equipo
        End If
    End Sub
    Protected Sub lbIngresar_Click(sender As Object, e As EventArgs) Handles lbIngresar.Click

        If txtUser.Text = "" Or txtPass.Text = "" Then
            ObjUniversal.SendNotify(" El usuario y la clave son campos requeridos ", "3")
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniversal.LoginDep(Trim(txtUser.Text))

        If dt.Rows.Count > 0 Then

            Dim ExtraerUsuario() As String = txtUser.Text.Split("@")

            Dim Usuario As String = ExtraerUsuario(0)


            Dim NumLogin As Integer = 0

            ' Dim adPath As String = "LDAP://172.17.101.57:389/DC=UDES,DC=local" 'Path to your LDAP directory server
            'Dim adAuth As LdapAuthentication = New LdapAuthentication(adPath)

            Try
                Dim adsEntry As New DirectoryEntry("LDAP://172.17.101.57:389/DC=UDES,DC=local", Trim(Usuario), txtPass.Text)
                Dim deSearch As New DirectorySearcher(adsEntry)
                Dim properties() As String = {"fullname"}
                deSearch.SearchScope = SearchScope.Subtree
                deSearch.ReferralChasing = ReferralChasingOption.All
                deSearch.PropertiesToLoad.AddRange(properties)
                deSearch.Filter = "(sAMAccountName=" + Trim(Usuario) + ")"

                Dim result As SearchResult
                result = deSearch.FindOne()
                Dim directoryEntry As New DirectoryEntry
                directoryEntry = result.GetDirectoryEntry()

                Me.Session("Nombre") = directoryEntry.Properties("displayname").Value
                Me.Session("Correo") = directoryEntry.Properties("mail").Value
                NumLogin = 1

                Dim iduser As String = dt.Rows(0).Item("DependenciaID")
                Session("IDuser") = dt.Rows(0).Item("DependenciaID")
                Me.Session("id_usuario") = iduser
                Session("NomUser") = dt.Rows(0).Item("NombrePersona")
                Me.Session("Administrador") = dt.Rows(0).Item("Administrador")
                Me.Session("SedeCod") = dt.Rows(0).Item("SedeCod")
                Me.Session("Dependencia") = True

                Response.Redirect("../Form/Modulos.aspx")



            Catch ex As Exception
                If NumLogin <> 1 Then
                    ObjUniversal.SendNotify("El usuario y la contraseña no coinciden", "3")
                End If

            End Try
        Else
            ObjUniversal.SendNotify("El usuario no esta registrado en el sistema", "3")
        End If



    End Sub
    Protected Sub lbOlvido_Click(sender As Object, e As EventArgs) Handles lbOlvido.Click
        Response.Redirect("../Inicio/Recovery.aspx")
    End Sub
    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function

End Class