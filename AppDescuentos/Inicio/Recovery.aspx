﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Secundario.Master" CodeBehind="Recovery.aspx.vb" Inherits="AppDescuentos.Recovery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<%--<asp:Content ID="Content3" ContentPlaceHolderID="main_title" runat="server">
</asp:Content>--%>

<%--<asp:Content ID="Content4" ContentPlaceHolderID="Header" runat="server">
</asp:Content>--%>

<asp:Content ID="Content5" ContentPlaceHolderID="Main_Texto" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="navbar" runat="server">
    <div style="background-color: #1C4C8A">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="" title="Manual"><i class="fa fa-book"></i></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="Main_pagination" runat="server">
</asp:Content>

<%--<asp:Content ID="Content8" ContentPlaceHolderID="main_content" runat="server">
</asp:Content>--%>

<asp:Content ID="Content9" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row" style="margin-top: 50px">
        <div class="col-md-8">
            <div class="jumbotron" style="height: 250px">
                <h2>Solicitud descuentos</h2>
                <p align="justify">
                    <%--Sistema en el cual podrá realizar la gestión de los eventos de educación continua: generar diplomas, 
                    consultar reportes de inscritos y pagos, generar evaluación de satisfacción, generar informes, hacer seguimiento del evento y validar asistencia. --%>
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="modal-content" style="height: 250px">
                <div class="panel-heading">
                    <h3 class="panel-title">Recuperar clave</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <%--<label for="exampleInputEmail1" style="margin-left: 10px; margin-right: 10px; text-align:left">Usuario</label><br />--%>
                        <div style="margin-left: 10px; margin-right: 10px">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="background-color: #d3d3d3"><i class="fa fa-user fa-1x"></i></span>
                                <asp:TextBox ID="txtUser" runat="server" CssClass="form-control" placeholder="Usuario"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-group" style="text-align: center">
                        <asp:LinkButton ID="lbRecu" runat="server" CssClass="btn btn-primary" Text="Ingresar" Height="30px" Width="200px" OnClientClick="ShowBlockWindow();">
                        &nbsp;Enviar Clave&nbsp;<span class="fa fa-key"></span>
                        </asp:LinkButton>
                        <br />
                    </div>
                    <div class="form-group" style="text-align: center">
                        <asp:LinkButton ID="lbVol" runat="server" CssClass="btn btn-success" Text="Ingresar" Height="30px" Width="200px" OnClientClick="ShowBlockWindow();">
                        &nbsp;Volver&nbsp;<span class="fa fa-reply-all"></span>
                        </asp:LinkButton>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

