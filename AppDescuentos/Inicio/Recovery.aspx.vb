﻿Imports System.Security.Cryptography
Imports System.IO
Public Class Recovery
    Inherits System.Web.UI.Page
    'Dim ObjUniversal As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub lbRecu_Click(sender As Object, e As EventArgs) Handles lbRecu.Click
        'If txtUser.Text = "" Then
        '    ObjUniversal.SendNotify("El usuario es un campo requerido", "3")
        '    Exit Sub
        'End If

        'If txtUser.Text.Trim.Length > 0 Then
        '    Dim dtRecu As DataTable = ObjUniversal.Usuario(txtUser.Text, "", "", "", "", 0, 0, 0, 0, 0, "Recuperar")

        '    Select Case dtRecu.Rows(0).Item("Existe")
        '        Case 0
        '            ObjUniversal.SendNotify("El usuario " & txtUser.Text & " No se encuentra registrado", "3")
        '        Case 1
        '            ObjUniversal.SendNotify("El usuario " & txtUser.Text & " No se encuentra activo para acceder al aplicativo", "3")
        '        Case 2

        '            Dim ClaveDecrypt As String = Decrypt(HttpUtility.UrlDecode(dtRecu.Rows(0)("Clave")))
        '            Dim Mensaje As String = ""
        '            Mensaje &= "Usuario : " & dtRecu.Rows(0)("Usuario") & " <br/> "
        '            Mensaje &= "Clave : " & ClaveDecrypt & " <br/> "
        '            Dim envio As Boolean = Notificaciones.enviarNotificacionOlvidoClave(dtRecu.Rows(0)("Usuario"), dtRecu.Rows(0)("Nombre"), Mensaje, Me.Server)

        '            If envio Then

        '                If ConfigurationManager.AppSettings("enviarCorreos") Then
        '                    ' AlertNuevo("Se Ha Enviado Con Exito Los Datos Para El Inicio De Sesión, Por favor Revise Su Correo.", Me)
        '                    txtUser.Text = ""
        '                    '----------------------------------------------------------Eventlog------------------------------------
        '                    ''Dim dtEvent As DataTable = ObRegisEvent.EventLog(0, ViewState("UserRecovery"), "", "RecoverKey", "Recupero La Clave", ViewState("IPAddress"), ViewState("HostName"))
        '                    ObjUniversal.SendNotify("Se ha enviado con éxito los datos para el inicio de sesión, por favor revise su correo.", "1")
        '                    Response.Redirect("../Inicio/Login.aspx")
        '                Else
        '                    '----------------------------------------------------------Eventlog------------------------------------
        '                    ''Dim dtEvent As DataTable = ObRegisEvent.EventLog(0, TxTUsuario.Text, "", "RecoverKey", "No Esta Autorizado El Envio De Mensajes, comuníquese  con el Administrador de la Aplicación", ViewState("IPAddress"), ViewState("HostName"))
        '                    ObjUniversal.SendNotify("No Está Autorizado El Envío De Mensajes, comuníquese  con el Administrador de la Aplicación.", "3")
        '                End If

        '            Else
        '                '----------------------------------------------------------Eventlog------------------------------------
        '                ''Dim dtEvent As DataTable = ObRegisEvent.EventLog(0, TxTUsuario.Text, "", "RecoverKey", "El Correo No Ha sido Enviado, comuníquese  con el Administrador de la Aplicación.", ViewState("IPAddress"), ViewState("HostName"))
        '                ObjUniversal.SendNotify(" No recupero la clave, Inténtelo de nuevo", "4")
        '            End If

        '    End Select

        'End If


    End Sub
    Protected Sub lbVol_Click(sender As Object, e As EventArgs) Handles lbVol.Click
        Response.Redirect("../Inicio/Login.aspx")
    End Sub
    Private Function Decrypt(cipherText As String) As String
        Try
            Dim EncryptionKey As String = "MAKV2SPBNI99212"
            cipherText = cipherText.Replace(" ", "+")
            Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
            Using encryptor As Aes = Aes.Create()
                Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
                encryptor.Key = pdb.GetBytes(32)
                encryptor.IV = pdb.GetBytes(16)
                Using ms As New MemoryStream()
                    Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                        cs.Write(cipherBytes, 0, cipherBytes.Length)
                        cs.Close()
                    End Using
                    cipherText = Encoding.Unicode.GetString(ms.ToArray())
                End Using
            End Using
            Return cipherText

        Catch generatedExceptionName As FormatException
            Dim context As HttpContext = HttpContext.Current
            context.Response.Redirect("ErrorPagina.aspx")
            Return Nothing
        Catch generatedExceptionName As CryptographicException
            Dim context As HttpContext = HttpContext.Current
            context.Response.Redirect("ErrorPagina.aspx")
            Return Nothing
        Catch generatedExceptionName As IndexOutOfRangeException
            Dim context As HttpContext = HttpContext.Current
            context.Response.Redirect("ErrorPagina.aspx")
            Return Nothing
            'Destruimos o finalizamos lo que necesitemos. No olvidar que existe el método Dispose ;)
        Finally
        End Try
    End Function
End Class