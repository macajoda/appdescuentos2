﻿Public Class DocumentosDescuento
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Buscar()
            'lbAddDocumentosDes.Visible = False
            'lbNewDocumentosDesc.Visible = False
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBDocumentosDescuento(0, Trim(txtNombre.Text), 0, ddlActivos.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Documentos") = dt
            GVDocumentos.DataSource = dt
            GVDocumentos.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Documentos") = Nothing
            GVDocumentos.DataSource = Nothing
            GVDocumentos.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    'Public Sub TipoDescuentos()
    '    Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(0, "", "", 0, 0, "Buscar")

    '    If dt.Rows.Count > 0 Then
    '        With Me.ddlTipoDescuentos
    '            .Items.Clear()
    '            .DataSource = dt
    '            .DataValueField = "TipoDescuentoID"
    '            .DataTextField = "Nombre"
    '            .DataBind()
    '            .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
    '        End With

    '        ddlTipoDescuentos.SelectedValue = -1

    '        With Me.ddlTipoDescuentosAdd
    '            .Items.Clear()
    '            .DataSource = dt
    '            .DataValueField = "TipoDescuentoID"
    '            .DataTextField = "Nombre"
    '            .DataBind()
    '            .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
    '        End With

    '        ddlTipoDescuentosAdd.SelectedValue = -1

    '    End If
    'End Sub

    Protected Sub lbNewDocumentosDesc_Click(sender As Object, e As EventArgs) Handles lbNewDocumentosDesc.Click

        Limpiar()
        lblEvento.Text = "Crear Documentos"
        lbAddDocumentosDes.Text = "Agregar"
        ViewState("Accion") = "Crear" 'Crear
        ViewState("DocumentosDescuentoID") = 0
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDocumentosDescuento", "FuncionModalAddDocumentosDescuento();", True)
    End Sub
    Public Sub Crear()
        If Trim(txtNombreAdd.Text) = "" Then
            ObjUniver.SendNotify("Nombre es obligatorio", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDocumentosDescuento", "FuncionModalAddDocumentosDescuento();", True)
            Exit Sub
        End If

        Dim Mensaje As String = ""
        Dim dtValidar As DataTable = ObjUniver.ValidarNombre("", txtNombreAdd.Text, 0, "Documentos")
        Dim dtValidarMod As DataTable = ObjUniver.ValidarNombre("", txtNombreAdd.Text, ViewState("DocumentosDescuentoID"), "DocumentosMod")

        If ViewState("Accion") = "Crear" Then
            If dtValidar.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDocumentosDescuento", "FuncionModalAddDocumentosDescuento();", True)
                Exit Sub
            Else
                Mensaje = "Se creó el registro con éxito"
            End If
        Else
            If dtValidarMod.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDocumentosDescuento", "FuncionModalAddDocumentosDescuento();", True)
                Exit Sub
            Else
                Mensaje = "Se modificó el registro con éxito"
            End If

        End If

        Dim dt As DataTable = ObjUniver.CRUBDocumentosDescuento(ViewState("DocumentosDescuentoID"), Trim(txtNombreAdd.Text), ddlObligatorio.SelectedValue, 0, ViewState("Accion"))
        If dt.Rows.Count > 0 Then
            Limpiar()
            ObjUniver.SendNotify(Mensaje, "1")
            Buscar()
        End If
    End Sub
    Public Sub Limpiar()
        txtNombreAdd.Text = ""
        ddlObligatorio.SelectedValue = 0
    End Sub
    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub
    Protected Sub lbAddDocumentosDes_Click(sender As Object, e As EventArgs) Handles lbAddDocumentosDes.Click
        Crear()
    End Sub

    Protected Sub GVDocumentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDocumentos.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim Obligatorio As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lObligatorio"), System.Web.UI.WebControls.Label)
        Dim btnEstado As System.Web.UI.WebControls.LinkButton = DirectCast(e.Row.FindControl("lbEliminar"), System.Web.UI.WebControls.LinkButton)

        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-danger fa fa-times"
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-success fa fa-check"
            End If

            Estado.Text = ""


            If Obligatorio.Text = True Then
                Obligatorio.CssClass = "fa fa-check fa-1x" : Obligatorio.ForeColor = Drawing.Color.Green
            Else
                Obligatorio.CssClass = "fa fa-times fa-1x" : Obligatorio.ForeColor = Drawing.Color.Red
            End If

            Obligatorio.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GVDocumentos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVDocumentos.RowCommand
        If e.CommandName = "Editar" Then
            Limpiar()
            lblEvento.Text = "Editar Documentos"
            lbAddDocumentosDes.Text = "Actualizar"
            ViewState("Accion") = "Modificar" 'Modificar
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDocumentosDescuento", "FuncionModalAddDocumentosDescuento();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("DocumentosDescuentoID") = Me.GVDocumentos.DataKeys(index).Values(0)


            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")
            Dim dt As DataTable = ObjUniver.CRUBDocumentosDescuento(ViewState("DocumentosDescuentoID"), "", 0, 0, "BuscarID")
            If dt.Rows.Count > 0 Then
                txtNombreAdd.Text = dt.Rows(0).Item("Nombre")
               
                Dim Obligatorio As Integer = 0

                If dt.Rows(0).Item("Obligatorio") = True Then
                    Obligatorio = 1
                Else
                    Obligatorio = 0
                End If
                ddlObligatorio.SelectedValue = Obligatorio

            End If
        End If
    End Sub

    Protected Sub GVDocumentos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVDocumentos.RowDeleting
        Dim ID As Integer = Me.GVDocumentos.DataKeys(e.RowIndex).Value
        Dim dt1 As DataTable = ObjUniver.CRUBDocumentosDescuento(ID, "", 0, 0, "BuscarID")

        Dim SwActivo As Boolean = dt1.Rows(0).Item("SwActivo")
        Dim Mensaje As String = ""

        If SwActivo = True Then
            SwActivo = False
            Mensaje = "Desactivado"
        Else
            SwActivo = True
            Mensaje = "Activado"
        End If

        Dim dt As DataTable = ObjUniver.CRUBDocumentosDescuento(ID, "", 0, SwActivo, "DesActivar")

        ObjUniver.SendNotify(" Registro  " & Mensaje, "1")
        Buscar()
    End Sub

    Protected Sub GVDocumentos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVDocumentos.PageIndexChanged
        Buscar()
    End Sub

    Protected Sub GVDocumentos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVDocumentos.PageIndexChanging
        Me.GVDocumentos.PageIndex = e.NewPageIndex()
        Me.GVDocumentos.DataBind()
    End Sub
End Class