﻿Public Class Usuarios
    Inherits System.Web.UI.Page
     Dim ObjUniversal As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Consulta()
        End If
    End Sub
    Public Sub Consulta()

        Dim dt As DataTable = ObjUniversal.Usuario(0, Trim(txtNombre.Text), "", "", "", "", 0, ddlMaestro.SelectedValue, "", "", ddlEstadoConsul.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("DatosUsuarios") = dt
            GVUsuarios.DataSource = dt
            GVUsuarios.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("DatosUsuarios") = Nothing
            GVUsuarios.DataSource = Nothing
            GVUsuarios.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Consulta()
    End Sub

    Protected Sub lbANewUsuarios_Click(sender As Object, e As EventArgs) Handles lbANewUsuarios.Click
        LimpiarCrear()
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddUsuarios", "FuncionModalAddUsuarios();", True)
    End Sub
    Public Sub LimpiarCrear()
        txtNombreAdd.Text = ""
        txtDireccionAdd.Text = ""
        dtpFechaNacimientoAdd.Value = Nothing
        txtTelefonoAdd.Text = ""
        txtCorreoAdd.Text = ""
        txtClaveAdd.Text = ""
        txtDireccionAdd.Text = ""

    End Sub
    Protected Sub lbAddUsuarios_Click(sender As Object, e As EventArgs) Handles lbAddUsuarios.Click
        If Len(Trim(txtNombreAdd.Text)) = 0 Or Len(Trim(txtCorreoAdd.Text)) = 0 Or Len(Trim(txtClaveAdd.Text)) = 0 Then
            ObjUniversal.SendNotify(" Por favor verificar campos en rojo, son requeridos ", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddUsuarios", "FuncionModalAddUsuarios();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniversal.Usuario(0, Trim(txtNombreAdd.Text), Trim(txtDireccionAdd.Text), Trim(txtTelefonoAdd.Text), Trim(txtCorreoAdd.Text), dtpFechaNacimientoAdd.Value, ddlAdminAdd.SelectedValue, ddlMaestrosAdd.SelectedValue, Trim(txtClaveAdd.Text), "", 1, "Crear")

        If dt.Rows.Count > 0 Then
            ObjUniversal.SendNotify(" Registro creado  ", "1")
            Consulta()
        Else
            ObjUniversal.SendNotify("No fue creado el registro", "3")
        End If
    End Sub

    Protected Sub GVUsuarios_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVUsuarios.RowCommand
        If e.CommandName = "EditarUsuarios" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModalUpdUsuarios", "FuncionModalUpdUsuarios();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("UsuarioID") = Me.GVUsuarios.DataKeys(index).Values(0)
            Limpiarmodificar()
            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")
            Dim dt As DataTable = ObjUniversal.Usuario(ViewState("UsuarioID"), "", "", "", "", "", 0, 0, "", "", 0, "BuscarId")
            If dt.Rows.Count > 0 Then
                txtNombreUpd.Text = dt.Rows(0).Item("Nombre")
                txtClaveUpd.Text = dt.Rows(0).Item("Clave")
                Try
                    dtpFechaNacimientoUpd.Value = dt.Rows(0).Item("FechaNacimiento")
                Catch ex As Exception
                    dtpFechaNacimientoUpd.Value = Nothing
                End Try

                txtCorreoUpd.Text = dt.Rows(0).Item("Correo")
                txtTelefonoUpd.Text = dt.Rows(0).Item("Telefono")
                txtDireccionUpd.Text = dt.Rows(0).Item("Direccion")

                If dt.Rows(0).Item("Admin") = True Then
                    ddlAdminUpd.SelectedValue = 1
                Else
                    ddlAdminUpd.SelectedValue = 0
                End If

                If dt.Rows(0).Item("Maestro") = True Then
                    ddlMaestroUpd.SelectedValue = 1
                Else
                    ddlMaestroUpd.SelectedValue = 0
                End If

            End If
        End If
    End Sub
    Public Sub Limpiarmodificar()
        txtNombreUpd.Text = ""
        txtDireccionUpd.Text = ""
        dtpFechaNacimientoUpd.Value = Nothing
        txtTelefonoUpd.Text = ""
        txtCorreoUpd.Text = ""
        txtClaveUpd.Text = ""
        txtDireccionUpd.Text = ""

    End Sub
    Protected Sub lbActualizar_Click(sender As Object, e As EventArgs) Handles lbActualizar.Click
        If Len(Trim(txtNombreUpd.Text)) = 0 Or Len(Trim(txtCorreoUpd.Text)) = 0 Or Len(Trim(txtClaveUpd.Text)) = 0 Then
            ObjUniversal.SendNotify(" Por favor verificar campos en rojo, son requeridos ", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalUpdUsuarios", "FuncionModalUpdUsuarios();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniversal.Usuario(ViewState("UsuarioID"), Trim(txtNombreUpd.Text), Trim(txtDireccionUpd.Text), Trim(txtTelefonoUpd.Text), Trim(txtCorreoUpd.Text), dtpFechaNacimientoUpd.Value, ddlAdminUpd.SelectedValue, ddlMaestroUpd.SelectedValue, Trim(txtClaveUpd.Text), "", 1, "Modificar")

        ObjUniversal.SendNotify(" Registro actualizado  ", "1")
        Consulta()
    End Sub

    Protected Sub GVUsuarios_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVUsuarios.RowDeleting
        Dim ID As Integer = Me.GVUsuarios.DataKeys(e.RowIndex).Value
        Dim dt1 As DataTable = ObjUniversal.Usuario(ID, "", "", "", "", "", 0, 0, "", "", 0, "BuscarId")

        Dim SwActivo As Boolean = dt1.Rows(0).Item("SwActivo")
        Dim Mensaje As String = ""

        If SwActivo = True Then
            SwActivo = False
            Mensaje = "Desactivado"
        Else
            SwActivo = True
            Mensaje = "Activado"
        End If

        Dim dt As DataTable = ObjUniversal.Usuario(ID, "", "", "", "", "", 0, 0, "", "", SwActivo, "ActivarDesactivar")

        ObjUniversal.SendNotify(" Registro  " & Mensaje, "1")
        Consulta()
    End Sub

    Protected Sub GVUsuarios_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVUsuarios.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
            End If

            Estado.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GVUsuarios_PageIndexChanged(sender As Object, e As EventArgs) Handles GVUsuarios.PageIndexChanged
        Consulta()
    End Sub

    Protected Sub GVUsuarios_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVUsuarios.PageIndexChanging
        Me.GVUsuarios.PageIndex = e.NewPageIndex()
        Me.GVUsuarios.DataBind()
    End Sub
End Class