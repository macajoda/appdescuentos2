﻿Public Class Dependencias
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Buscar()
            'lbAddDependencias.Visible = False
            'lbNewDependencias.Visible = False
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBDependencias(0, Me.Session("SedeCod"), "", Trim(txtNombreDepen.Text), "", "", 0, ddlActivos.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Dependencias") = dt
            GVDependencias.DataSource = dt
            GVDependencias.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Dependencias") = Nothing
            GVDependencias.DataSource = Nothing
            GVDependencias.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbNewDependencias_Click(sender As Object, e As EventArgs) Handles lbNewDependencias.Click
        Limpiar()
        lblEvento.Text = "Crear dependencias"
        lbAddDependencias.Text = "Agregar"
        ViewState("Accion") = "Crear" 'Crear
        ViewState("DependenciaID") = 0
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
    End Sub
    Public Sub Crear()
        If Trim(txtNombreDepenAdd.Text) = "" Or Trim(txtNombrePersonaAdd.Text) = "" Or Trim(txtCorreoAdd.Text) = "" Then
            ObjUniver.SendNotify("Nombre dependencia, nombre persona y correo son obligatorios", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
            Exit Sub
        End If


        Dim Mensaje As String = ""
        Dim dtValidar As DataTable = ObjUniver.ValidarNombre(Me.Session("SedeCod"), txtNombreDepenAdd.Text, 0, "Dependencia")
        Dim dtValidarMod As DataTable = ObjUniver.ValidarNombre(Me.Session("SedeCod"), txtNombreDepenAdd.Text, ViewState("DependenciaID"), "DependenciaMod")
        Dim dtValidarCorreo As DataTable = ObjUniver.ValidarNombre(Me.Session("SedeCod"), txtCorreoAdd.Text, 0, "DependenciaCorreo")
        Dim dtValidarCorreoMod As DataTable = ObjUniver.ValidarNombre(Me.Session("SedeCod"), txtCorreoAdd.Text, ViewState("DependenciaID"), "DependenciaCorreoMod")

        If ViewState("Accion") = "Crear" Then
            If dtValidar.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
                Exit Sub
            Else
                If dtValidarCorreo.Rows.Count > 0 Then
                    ObjUniver.SendNotify("Ya existe ese correo", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
                    Exit Sub
                Else
                    Mensaje = "Se creó el registro con éxito"
                End If

            End If
        Else
            If dtValidarMod.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
                Exit Sub
            Else
                If dtValidarCorreoMod.Rows.Count > 0 Then
                    ObjUniver.SendNotify("Ya existe ese correo", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
                    Exit Sub
                Else
                    Mensaje = "Se modificó el registro con éxito"
                End If

            End If

        End If

       

        Dim dt As DataTable = ObjUniver.CRUBDependencias(ViewState("DependenciaID"), Me.Session("SedeCod"), Trim(txtCodDependenciaAdd.Text.ToUpper), Trim(txtNombreDepenAdd.Text.ToUpper), Trim(txtNombrePersonaAdd.Text.ToUpper), Trim(txtCorreoAdd.Text), ddlAdministrador.SelectedValue, 0, ViewState("Accion"))
        If dt.Rows.Count > 0 Then
            Limpiar()
            ObjUniver.SendNotify(Mensaje, "1")
            Buscar()
        End If
    End Sub
    Public Sub Limpiar()
        txtNombreDepenAdd.Text = ""
        txtNombrePersonaAdd.Text = ""
        txtCorreoAdd.Text = ""
        ddlAdministrador.SelectedValue = 0
    End Sub
    Protected Sub lbAddDependencias_Click(sender As Object, e As EventArgs) Handles lbAddDependencias.Click
        Crear()
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub

    Protected Sub GVDependencias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDependencias.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim btnEstado As System.Web.UI.WebControls.LinkButton = DirectCast(e.Row.FindControl("lbEliminar"), System.Web.UI.WebControls.LinkButton)


        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-danger fa fa-times"

            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
                ' btnEstado.CssClass = "fa fa-check"
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-success fa fa-check"
            End If

            Estado.Text = ""



        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GVDependencias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVDependencias.RowCommand
        If e.CommandName = "Editar" Then
            Limpiar()
            lblEvento.Text = "Editar dependencias"
            lbAddDependencias.Text = "Actualizar"
            ViewState("Accion") = "Modificar" 'Modificar
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddDependencias", "FuncionModalAddDependencias();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("DependenciaID") = Me.GVDependencias.DataKeys(index).Values(0)

            Dim dt As DataTable = ObjUniver.CRUBDependencias(ViewState("DependenciaID"), Me.Session("SedeCod"), "", "", "", "", 0, 0, "BuscarID")
            If dt.Rows.Count > 0 Then
                txtCodDependenciaAdd.Text = dt.Rows(0).Item("CodigoDep")
                txtNombreDepenAdd.Text = dt.Rows(0).Item("NombreDepen")
                txtNombrePersonaAdd.Text = dt.Rows(0).Item("NombrePersona")
                txtCorreoAdd.Text = dt.Rows(0).Item("Correo")

                Dim Administrador As Integer = 0

                If dt.Rows(0).Item("Administrador") = True Then
                    Administrador = 1
                Else
                    Administrador = 0
                End If
                ddlAdministrador.SelectedValue = Administrador
            End If
        End If
    End Sub

    Protected Sub GVDependencias_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVDependencias.RowDeleting
        Dim ID As Integer = Me.GVDependencias.DataKeys(e.RowIndex).Value
        Dim dt1 As DataTable = ObjUniver.CRUBDependencias(ID, Me.Session("SedeCod"), "", "", "", "", 0, 0, "BuscarID")

        Dim SwActivo As Boolean = dt1.Rows(0).Item("SwActivo")
        Dim Mensaje As String = ""

        If SwActivo = True Then
            SwActivo = False
            Mensaje = "Desactivado"
        Else
            SwActivo = True
            Mensaje = "Activado"
        End If

        ' Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(ID, "", "", 0, SwActivo, "DesActivar")
        Dim dt As DataTable = ObjUniver.CRUBDependencias(ID, Me.Session("SedeCod"), "", "", "", "", 0, SwActivo, "DesActivar")

        ObjUniver.SendNotify(" Registro  " & Mensaje, "1")
        Buscar()
    End Sub

    Protected Sub GVDependencias_PageIndexChanged(sender As Object, e As EventArgs) Handles GVDependencias.PageIndexChanged
        Buscar()
    End Sub

    Protected Sub GVDependencias_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVDependencias.PageIndexChanging
        Me.GVDependencias.PageIndex = e.NewPageIndex()
        Me.GVDependencias.DataBind()
    End Sub
End Class