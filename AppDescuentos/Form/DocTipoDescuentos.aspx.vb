﻿Public Class DocTipoDescuentos
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TipoDescuentos()
            Documentos()
            Buscar()
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBDocTipoDescuentos(0, 0, ddlTipoDescuentos.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Documentos") = dt
            GVDocumentos.DataSource = dt
            GVDocumentos.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Documentos") = Nothing
            GVDocumentos.DataSource = Nothing
            GVDocumentos.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    Public Sub TipoDescuentos()
        Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(0, "", "", 0, 1, "Buscar")

        If dt.Rows.Count > 0 Then
            With Me.ddlTipoDescuentos
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuentos.SelectedValue = -1

            With Me.ddlTipoDescuentosAdd
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuentosAdd.SelectedValue = -1

        End If
    End Sub

    Public Sub Documentos()
        Dim dt As DataTable = ObjUniver.CRUBDocumentosDescuento(0, "", 0, 1, "Buscar")

        If dt.Rows.Count > 0 Then
            With Me.ddlDocumentosAdd
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "DocumentosDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione documento", "-1"))
            End With

            ddlDocumentosAdd.SelectedValue = -1

        End If
    End Sub

    Protected Sub lbNewDocTipoDescuento_Click(sender As Object, e As EventArgs) Handles lbNewDocTipoDescuento.Click
        ddlDocumentosAdd.SelectedValue = -1
        ddlTipoDescuentosAdd.SelectedValue = -1
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModaladdDocTipoDescuento", "FuncionModaladdDocTipoDescuento();", True)
    End Sub
    Protected Sub lbAddDocTipoDescuento_Click(sender As Object, e As EventArgs) Handles lbAddDocTipoDescuento.Click
        If ddlDocumentosAdd.SelectedValue = -1 Or ddlTipoDescuentosAdd.SelectedValue = -1 Then
            ObjUniver.SendNotify("Documento y tipo descuento es obligatorio", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModaladdDocTipoDescuento", "FuncionModaladdDocTipoDescuento();", True)
            Exit Sub
        End If

        Dim dt1 As DataTable = ObjUniver.CRUBDocTipoDescuentos(0, ddlDocumentosAdd.SelectedValue, ddlTipoDescuentosAdd.SelectedValue, "Validar")
        If dt1.Rows.Count > 0 Then
            ObjUniver.SendNotify("ya existe Documento y tipo descuento seleccionados", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModaladdDocTipoDescuento", "FuncionModaladdDocTipoDescuento();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniver.CRUBDocTipoDescuentos(0, ddlDocumentosAdd.SelectedValue, ddlTipoDescuentosAdd.SelectedValue, "Crear")

        If dt.Rows.Count > 0 Then
            ObjUniver.SendNotify("Se creó el registro con éxito", "1")
            Buscar()
        End If
    End Sub

    Protected Sub GVDocumentos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVDocumentos.RowDeleting
        Dim ID As Integer = Me.GVDocumentos.DataKeys(e.RowIndex).Value

        Dim dt As DataTable = ObjUniver.CRUBDocTipoDescuentos(ID, 0, 0, "Eliminar")

        ObjUniver.SendNotify(" Registro se eliminó con éxito  ", "1")
        Buscar()
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub

    Protected Sub GVDocumentos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVDocumentos.PageIndexChanged
        Buscar()
    End Sub

    Protected Sub GVDocumentos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVDocumentos.PageIndexChanging
        Me.GVDocumentos.PageIndex = e.NewPageIndex()
        Me.GVDocumentos.DataBind()
    End Sub
End Class