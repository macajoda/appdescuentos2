﻿Public Class TipoDescuentos
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Buscar()
            'lbAddTipoDescuento.Visible = False
            'lbNewTipoDescuento.Visible = False
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(0, Trim(txtNombre.Text), "", 0, ddlActivos.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("TipoDescuento") = dt
            GVTipoDescuento.DataSource = dt
            GVTipoDescuento.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("TipoDescuento") = Nothing
            GVTipoDescuento.DataSource = Nothing
            GVTipoDescuento.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbNewTipoDescuento_Click(sender As Object, e As EventArgs) Handles lbNewTipoDescuento.Click
        Limpiar()
        lblEvento.Text = "Crear tipo descuentos"
        lbAddTipoDescuento.Text = "Agregar"
        ViewState("Accion") = "Crear" 'Crear
        ViewState("TipoDescuentoID") = 0
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoDescuentos", "FuncionModalAddTipoDescuentos();", True)
    End Sub
    Public Sub Crear()
        If Trim(txtNombreAdd.Text) = "" Or Trim(txtPorcentajeAdd1.Text) = "" Then
            ObjUniver.SendNotify("Nombre y Porcentaje son obligatorios", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoDescuentos", "FuncionModalAddTipoDescuentos();", True)
            Exit Sub
        End If

        Try
            Dim Porcentaje As Decimal = txtPorcentajeAdd1.Text
        Catch ex As Exception
            ObjUniver.SendNotify("Porcentaje debe ser númerico", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoDescuentos", "FuncionModalAddTipoDescuentos();", True)
            Exit Sub
        End Try

        Dim Mensaje As String = ""
        Dim dtValidar As DataTable = ObjUniver.ValidarNombre("", txtNombreAdd.Text, 0, "TipoDescuento")
        Dim dtValidarMod As DataTable = ObjUniver.ValidarNombre("", txtNombreAdd.Text, ViewState("TipoDescuentoID"), "TipoDescuentoMod")

        If ViewState("Accion") = "Crear" Then
            If dtValidar.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoDescuentos", "FuncionModalAddTipoDescuentos();", True)
                Exit Sub
            Else
                Mensaje = "Se creó el registro con éxito"
            End If
        Else
            If dtValidarMod.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoDescuentos", "FuncionModalAddTipoDescuentos();", True)
                Exit Sub
            Else
                Mensaje = "Se modificó el registro con éxito"
            End If

        End If

        Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(ViewState("TipoDescuentoID"), Trim(txtNombreAdd.Text.ToUpper), Trim(txtObservacionAdd.Text), Trim(txtPorcentajeAdd1.Text), 0, ViewState("Accion"))
        If dt.Rows.Count > 0 Then
            Limpiar()

            ObjUniver.SendNotify(Mensaje, "1")

            Buscar()
        End If
    End Sub
    Public Sub Limpiar()
        txtNombreAdd.Text = ""
        txtObservacionAdd.Text = ""
        txtPorcentajeAdd1.Text = ""
    End Sub
    Protected Sub lbAddTipoDescuento_Click(sender As Object, e As EventArgs) Handles lbAddTipoDescuento.Click
        Crear()
    End Sub

    'Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
    '    Buscar()
    'End Sub

    Protected Sub GVTipoDescuento_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVTipoDescuento.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim btnEstado As System.Web.UI.WebControls.LinkButton = DirectCast(e.Row.FindControl("lbEliminar"), System.Web.UI.WebControls.LinkButton)


        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-danger fa fa-times"
                btnEstado.ToolTip = "Desactivar"

            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
                ' btnEstado.CssClass = "fa fa-check"
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-success fa fa-check"
                btnEstado.ToolTip = "Activar"
            End If

            Estado.Text = ""


          
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GVTipoDescuento_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVTipoDescuento.RowCommand
        If e.CommandName = "Editar" Then
            Limpiar()
            lblEvento.Text = "Editar tipo descuentos"
            lbAddTipoDescuento.Text = "Actualizar"
            ViewState("Accion") = "Modificar" 'Modificar
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoDescuentos", "FuncionModalAddTipoDescuentos();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("TipoDescuentoID") = Me.GVTipoDescuento.DataKeys(index).Values(0)


            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")
            Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(ViewState("TipoDescuentoID"), "", "", 0, 1, "BuscarID")
            If dt.Rows.Count > 0 Then
                txtNombreAdd.Text = dt.Rows(0).Item("Nombre")
                Try
                    txtObservacionAdd.Text = dt.Rows(0).Item("Observacion")
                Catch ex As Exception
                    txtObservacionAdd.Text = ""
                End Try

                txtPorcentajeAdd1.Text = dt.Rows(0).Item("Porcentaje")
                
            End If
        End If
    End Sub

    Protected Sub GVTipoDescuento_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVTipoDescuento.RowDeleting
        Dim ID As Integer = Me.GVTipoDescuento.DataKeys(e.RowIndex).Value
        Dim dt1 As DataTable = ObjUniver.CRUBTipoDescuento(ID, "", "", 0, 1, "BuscarID")

        Dim SwActivo As Boolean = dt1.Rows(0).Item("SwActivo")
        Dim Mensaje As String = ""

        If SwActivo = True Then
            SwActivo = False
            Mensaje = "Desactivado"
        Else
            SwActivo = True
            Mensaje = "Activado"
        End If

        Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(ID, "", "", 0, SwActivo, "DesActivar")

        ObjUniver.SendNotify(" Registro  " & Mensaje, "1")
        Buscar()
    End Sub

    Protected Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        'Dim Validar As Boolean = ObjUniver.ComprobarFormatoLetras(txtNombre.Text)
        'If Validar = False Then
        '    txtNombre.Text = Left(txtNombre.Text, txtNombre.Text.Length - 1)
        'End If

    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub

    Protected Sub GVTipoDescuento_PageIndexChanged(sender As Object, e As EventArgs) Handles GVTipoDescuento.PageIndexChanged
        Buscar()
    End Sub

    Protected Sub GVTipoDescuento_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVTipoDescuento.PageIndexChanging
        Me.GVTipoDescuento.PageIndex = e.NewPageIndex()
        Me.GVTipoDescuento.DataBind()
    End Sub
End Class