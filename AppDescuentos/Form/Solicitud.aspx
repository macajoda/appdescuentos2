﻿<%--<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Imprenta.Master" CodeBehind="Solicitud.aspx.vb" Inherits="Logistica.Solicitud" %>--%>

<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Inicio.Master" CodeBehind="Solicitud.aspx.vb" Inherits="Logistica.Solicitud" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style9 {
            font-size: medium;
            color: #FFFFFF;
        }

        .auto-style13 {
            height: 10px;
        }

        .auto-style18 {
            font-size: large;
            color: #CC3300;
        }

        .auto-style19 {
            height: 10px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnPrincipalSolicitud" runat="server" Visible="False">
        <asp:Panel ID="pnSolicitudes" runat="server">
            <table width="100%">
                <tr>
                    <td width="100%" bgcolor="#70AD47">&nbsp;<asp:Image ID="Image4" runat="server" ImageUrl="~/Img/IconSolicitud.png" Height="22px" Width="22px" />
                        <span class="auto-style9">Solicitudes</span></td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnGestionSolicitudes" runat="server" BorderColor="Silver" BorderWidth="1px">
            <table class="table">
                <tr>
                    <td width="100%" class="auto-style13" colspan="6"></td>
                </tr>
                <tr>
                    <td width="100%" class="auto-style19" colspan="6">
                        <asp:ImageButton ID="ibNuevo" runat="server" ImageUrl="~/Img/ImgButton/NuevoOff.png" OnClientClick="ShowBlockWindow();" onmouseout="this.src='../Img/ImgButton/NuevoOff.png';" onmouseover="this.src='../Img/ImgButton/NuevoOn.png';" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="auto-style13" colspan="6"></td>
                </tr>
                <tr>
                    <td width="100%" class="auto-style13" colspan="6">
                        <asp:GridView ID="GvSolicitudes" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" CssClass="tablaBlue" DataKeyNames="SolicitudID" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="SolicitudID" HeaderText="#">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Fecha" DataFormatString="{0:d}" HeaderText="Fecha de Solicitud">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaRequiere" DataFormatString="{0:d}" HeaderText="Fecha En Que Se Requiere">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Aprobado Logistica">
                                    <ItemTemplate>
                                        <asp:Label ID="lAproLogistica" runat="server" Text='<%# Bind("EstLog") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="EstRevi" HeaderText="Requiere Revisión Presupuestal ?">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EstPre" HeaderText="Aprobado Presupuesto">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Asignado Imprenta">

                                    <ItemTemplate>
                                        <asp:Label ID="lAsigImprenta" runat="server" Text='<%# Bind("EstAsig") %>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="EstEvalua" HeaderText="Estado Evaluación ">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="Editar" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibEditar" runat="server" CausesValidation="false" CommandArgument='<%# Eval("SolicitudID") %>' CommandName="EditarSol" Text="" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Evaluar" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibEvaluar" runat="server" CausesValidation="false" CommandArgument='<%# Eval("SolicitudID") %>' CommandName="EvaluarServicio" Text="" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:TemplateField>

                                <asp:ButtonField ButtonType="Image" CommandName="VerSolVAF" HeaderText="Ver" ImageUrl="~/Img/SolicitudVAF.png">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:ButtonField>

                                <asp:ButtonField ButtonType="Image" HeaderText="Proceso (Bitácora)" ImageUrl="~/Img/Bitacoraa.png" CommandName="btnBitacora">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:ButtonField>

                                <asp:TemplateField HeaderText="Ver Adjuntos" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibVerAdjunto" runat="server" CausesValidation="false" CommandArgument='<%# Eval("SolicitudID") %>' Text="" CommandName="VerAdjunto" />
                                        <asp:Label ID="lTamano" runat="server" Text='<%# Bind("Tamano")%>'>></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="auto-style13" colspan="6"></td>
                </tr>
            </table>
        </asp:Panel>
        <%--<asp:Panel ID="pnFormulario" runat="server" BorderColor="Silver" BorderWidth="1px" Visible="False">
            <table width="100%">
                <tr>
                    <td width="100%" colspan="20">
                        <p class="MsoNormal" style="text-align: center; font-size: large">
                            <asp:Label ID="lMensaje" runat="server" ForeColor="#990000" Visible="False"></asp:Label>
                            <asp:Label ID="lSolNum" runat="server" ForeColor="#990000" Visible="False"></asp:Label>
                            <strong>SOLICITUD SERVICIO IMPRENTA</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="footer"><span>Preferiblemente, antes de diligenciar y enviar este formato, comuníquese con la imprenta para conocer el proceso concerniente a esta solicitud.<p>Teléfonos: 6959951 – PBX. 6516500 Ext. 1977 – 1975 / e-mail: <a href="mailto:imprenta@udes.edu.co">imprenta@udes.edu.co</a></p>
                    </span></td>
                </tr>
                <tr>
                    <td align="justify" class="Subtitle" colspan="20">
                        <h2>DATOS DEL SOLICITANTE</h2>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">Fecha:</td>
                    <td width="50%" colspan="10">Sede:</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbFecha" runat="server" Enabled="False" Width="192px"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbSede" runat="server" Enabled="False" Width="185px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">Solicitado Por:</td>
                    <td width="50%" colspan="10">Cargo:</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbSolicitado" runat="server" Enabled="False" Width="385px"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbCargo" runat="server" Enabled="False" Width="185px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14"></td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">Dependencia o Filial:</td>
                    <td width="50%" colspan="10">Correo de contacto:</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbDependencia" runat="server" Enabled="False" Width="385px"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbCorreo" runat="server" Enabled="False" Width="385px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Encargado del trabajo:</label></td>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Cargo:</label></td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="tbEncargadoTrabajo" runat="server" Width="385px"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="tbCargoEncargado" runat="server" Width="185px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Telefono de contacto:</label></td>
                    <td width="50%" colspan="10">
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Img/Adjunto.png" />
                        <strong>Adjuntar Archivo:</strong></td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbTelContac" runat="server" Width="185px"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="284px" BackColor="White" />
                        &nbsp;&nbsp;&nbsp;
                        <asp:ImageButton ID="ibSubirAdjunt" runat="server" Height="27px" ImageUrl="~/Img/ImgButton/SubirArchiOff.png" OnClientClick="ShowBlockWindow();" Visible="False" Width="130px" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="10">
                        <label class="importante">
                            Fecha en que se requiere:</label></td>
                    <td colspan="10" rowspan="2">
                        <asp:Label ID="lNota1" runat="server" Font-Size="Small" ForeColor="DarkGray" Text="La Solicitud se debe realizar: &lt;br /&gt; Para Piezas Publicitarias: Al menos 20 días hábiles antes de la fecha del evento &lt;br /&gt; Para Revista o Documentos: Al menos 45 días hábiles antes de su publicación "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="10">
                        <telerik:RadDatePicker ID="rdpFechaRequi" runat="server" Width="329px">
                        </telerik:RadDatePicker>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Lugar en que se requiere:</label></td>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Evento o tema de la solicitud:</label></td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbLugarRequie" runat="server" Width="385px"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbEventoSoli" runat="server" Width="385px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:Label ID="lNota2" runat="server" Font-Size="Small" ForeColor="DarkGray" Text="Indicar Edificio, piso, oficina, etc."></asp:Label>
                    </td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                    <td width="05%">&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style17">
                        <asp:ImageButton ID="inModifEnca" runat="server" ImageUrl="~/Img/ImgButton/ModificarEncaOff.png" Visible="False" ImageAlign="Baseline" OnClientClick="ShowBlockWindow();" />
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14"></td>
                </tr>
                <tr>
                    <td align="justify" class="Subtitle" colspan="20">
                        <h2>MATERIAL SOLICITADO</h2>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="10">
                        <label class="importante">
                            Cantidad:</label></td>
                    <td colspan="10">
                        <label class="importante">
                            Servicio Solicitado:</label></td>
                </tr>
                <tr>
                    <td colspan="10">
                        <telerik:RadTextBox ID="txbCantidad" runat="server" ClientEvents-OnKeyPress="ValNum_OnKeyPress" Width="47px">
                        </telerik:RadTextBox>
                    </td>
                    <td colspan="10">
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <label class="btn btn-default" style="width: 300px">
                                        <asp:CheckBox ID="cbdiseno" runat="server" />
                                        Diseño
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <label class="btn btn-default" style="width: 300px">
                                        <asp:CheckBox ID="cbImpresion" runat="server" />
                                        Impresión
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="checkbox">
                                    <label class="btn btn-default" style="width: 300px">
                                        <asp:CheckBox ID="cbOtro" runat="server" />
                                        Otro
                                    </label>
                                </div>
                            </div>

                        </div>
                        <style>
                            .checkbox .btn,
                            .checkbox-inline .btn {
                                padding-left: 2em;
                                min-width: 8em;
                            }

                            .checkbox label,
                            .checkbox-inline label {
                                text-align: left;
                                padding-left: 0.5em;
                            }
                        </style>
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="auto-style14">&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Descripcion (Volante,plegable,Afiche u Otro)</label></td>
                    <td width="50%" colspan="10">
                        <label class="importante">
                            Describa las caracteristicas especiales o las modificaciones indicadas si ya existe el diseño</label></td>
                </tr>
                <tr>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbDescrip" runat="server" Width="475px" Height="41px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td width="50%" colspan="10">
                        <asp:TextBox ID="txbCaracteristicas" runat="server" Width="475px" Height="41px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td width="5%">&nbsp; &nbsp;</td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                </tr>
                <tr>
                    <td width="100%" colspan="20" class="footer">&nbsp;<asp:ImageButton ID="ibModificarMate" runat="server" ImageUrl="~/Img/ImgButton/ModificarMatOff.png" Visible="False" OnClientClick="ShowBlockWindow();" />
                        &nbsp;&nbsp;
                            <asp:ImageButton ID="ibCancelModMate" runat="server" ImageUrl="~/Img/ImgButton/CancelModmatOff.png" Visible="False" />
                        &nbsp;&nbsp;
                            <asp:ImageButton ID="ibGuardarMate" runat="server" ImageUrl="~/Img/ImgButton/GuardarMateOff.png" onmouseout="this.src='../Img/ImgButton/GuardarMateOff.png';" onmouseover="this.src='../Img/ImgButton/GuardarMateOn.png';" Visible="False" OnClientClick="ShowBlockWindow();" />
                        &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="ibLimpiar" runat="server" ImageUrl="~/Img/ImgButton/LimpiarOff.png" onmouseout="this.src='../Img/ImgButton/LimpiarOff.png';" onmouseover="this.src='../Img/ImgButton/LimpiarOn.png';" Visible="False" />
                        &nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                    <td width="5%"></td>
                </tr>
                <tr>
                    <td width="100%" colspan="20">
                        <asp:GridView ID="GvDetallesSolici" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" CssClass="tablaBlue" Width="100%" DataKeyNames="DetalleID">
                            <Columns>
                                <asp:BoundField DataField="DetalleID" HeaderText="#">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Cantidad" HeaderText="Cantidad">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ServiSolicitado" HeaderText="Servicio Solicitado">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Caracteristicas" HeaderText="Características Especiales">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="20%" />
                                </asp:BoundField>
                                <asp:ButtonField ButtonType="Image" HeaderText="Editar" ImageUrl="~/Img/edit.png" CommandName="Editar">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:ButtonField>
                                <asp:ButtonField ButtonType="Image" ImageUrl="~/Img/Eliminar.png" CommandName="Eliminar" HeaderText="Eliminar">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:ButtonField>
                            </Columns>
                        </asp:GridView>
                        <div class="footer">
                            <br />
                            <asp:ImageButton ID="ibVolver" runat="server" ImageUrl="~/Img/ImgButton/ibVolverOff.png" OnClientClick="ShowBlockWindow();" />
                            <br />
                            <br />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>--%>

        <div class="panel panel-default" id="pnFormulario" runat="server" visible="False">
            <div class="panel-body">
                <div class="panel panel-info" id="pnNewUpdateSol" runat="server" visible="true">
                    <div class="panel-heading">
                        <i class="fa fa-pencil-square-o fa-2x"></i>
                        <%--<p class="MsoNormal" style="text-align: center; font-size: large">--%>
                        <asp:Label ID="lMensaje" runat="server" ForeColor="#990000" Visible="False"></asp:Label><asp:Label ID="lSolNum" runat="server" ForeColor="#990000" Visible="False"></asp:Label>
                        <%--</p>--%>
                    </div>
                    <div class="panel-body">
                        <div class="form-group row">
                            <div class="col-lg-12" style="text-align: center">
                                <h4><strong>SOLICITUD SERVICIO IMPRENTA</strong></h4>
                                <span>Preferiblemente, antes de diligenciar y enviar este formato, comuníquese con la imprenta para conocer el proceso concerniente a esta solicitud.<p>Teléfonos: 6959951 – PBX. 6516500 Ext. 1977 – 1975 / e-mail: <a href="mailto:imprenta@udes.edu.co">imprenta@udes.edu.co</a></p>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12 bg-primary" style="text-align: left">
                                <strong>DATOS DEL SOLICITANTE</strong>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Fecha:</strong>
                                <br />
                                <asp:Label ID="lFecha" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Sede:</strong>
                                <br />
                                <asp:Label ID="lSede" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Solicitado Por:</strong>
                                <br />
                                <asp:Label ID="lSolicitado" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Cargo:</strong>
                                <br />
                                <asp:Label ID="lCargo" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Dependencia o Filial:</strong>
                                <br />
                                <asp:Label ID="lDependencia" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Correo de contacto:</strong>
                                <br />
                                <asp:Label ID="lCorreo" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Encargado del trabajo:</strong>
                                <br />
                                <asp:TextBox ID="tbEncargadoTrabajo" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Cargo:</strong>
                                <br />
                                <asp:TextBox ID="tbCargoEncargado" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Telefono de contacto:</strong>
                                <br />
                                <asp:TextBox ID="txbTelContac" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-3">
                                <strong>Adjuntar Archivo:</strong>
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-lg-1">
                                <br />
                                <asp:LinkButton ID="lbSubirAdjunt" runat="server" CssClass="btn btn-primary"
                                    Text="Subir &lt;i class=&quot;fa fa-upload fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                                </asp:LinkButton>
                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Fecha en que se requiere:</strong>
                                <br />
                                <div class='input-group date' id='rdpFechaRequi'>
                                    <input type='text' class="form-control" id="dtpFechaRequeControl" runat="server" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#rdpFechaRequi').datetimepicker();
                                    });
                                </script>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <p style="color: #999999">
                                    La Solicitud se debe realizar:
                            <br />
                                    Para Piezas Publicitarias: Al menos 20 días hábiles antes de la fecha del evento 
                            Para Revista o Documentos: Al menos 45 días hábiles antes de su publicación 
                                </p>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Lugar en que se requiere:</strong>
                                <br />
                                <asp:TextBox ID="txbLugarRequie" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Evento o tema de la solicitud:</strong>
                                <br />
                                <asp:TextBox ID="txbEventoSoli" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12" style="text-align: center">
                                <asp:LinkButton ID="lbModifEnca" runat="server" CssClass="btn btn-primary" Visible="False" OnClientClick="ShowBlockWindow();"
                                    Text="Modificar encabezado   &lt;i class=&quot;fa fa-refresh fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12 bg-primary" style="text-align: left">
                                <strong>MATERIAL SOLICITADO</strong>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-1">
                                <strong>Cantidad:</strong>
                                <br />
                                <asp:TextBox ID="txbCantidad" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Servicio Solicitado:</strong>
                                <br />
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="checkbox">
                                            <label class="btn btn-default" style="width: 300px">
                                                <asp:CheckBox ID="cbdiseno" runat="server" />
                                                Diseño
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="checkbox">
                                            <label class="btn btn-default" style="width: 300px">
                                                <asp:CheckBox ID="cbImpresion" runat="server" />
                                                Impresión
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="checkbox">
                                            <label class="btn btn-default" style="width: 300px">
                                                <asp:CheckBox ID="cbOtro" runat="server" />
                                                Otro
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <style>
                                    .checkbox .btn,
                                    .checkbox-inline .btn {
                                        padding-left: 2em;
                                        min-width: 8em;
                                    }

                                    .checkbox label,
                                    .checkbox-inline label {
                                        text-align: left;
                                        padding-left: 0.5em;
                                    }
                                </style>
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>
                        <div class="form-group row">
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <strong>Descripcion:</strong>
                                <br />
                                (Volante,plegable,Afiche u Otro)
                                
                                <asp:TextBox ID="txbDescrip" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-4">
                                <strong>Describa las caracteristicas especiales o las modificaciones indicadas si ya existe el diseño:</strong>
                                <br />
                                <asp:TextBox ID="txbCaracteristicas" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12" style="text-align: center">
                                <asp:LinkButton ID="ibModificarMate" runat="server" Visible="False" OnClientClick="ShowBlockWindow();" CssClass="btn btn-primary"
                                    Text="Modificar Material   &lt;i class=&quot;fa fa-floppy-o fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                                </asp:LinkButton>
                                &nbsp;&nbsp;
                             <asp:LinkButton ID="ibCancelModMate" runat="server" Visible="False" CssClass="btn btn-danger"
                                 Text="Cancelar (Modificar Material)   &lt;i class=&quot;fa fa-times fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                             </asp:LinkButton>
                                &nbsp;&nbsp;
                             <asp:LinkButton ID="ibGuardarMate" runat="server" Visible="False" OnClientClick="ShowBlockWindow();" CssClass="btn btn-success"
                                 Text="Guardar Material &lt;i class=&quot;fa fa-floppy-o fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                             </asp:LinkButton>
                                &nbsp;&nbsp;
                             <asp:LinkButton ID="ibLimpiar" runat="server" Visible="False" CssClass="btn btn-primary"
                                 Text="Limpiar   &lt;i class=&quot;fa fa-eraser fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                             </asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <asp:GridView ID="GvDetallesSolici" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" CssClass="tablaBlue" Width="100%" DataKeyNames="DetalleID">
                                    <Columns>
                                        <asp:BoundField DataField="DetalleID" HeaderText="#">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Cantidad" HeaderText="Cantidad">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="2%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="20%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiSolicitado" HeaderText="Servicio Solicitado">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Caracteristicas" HeaderText="Características Especiales">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="20%" />
                                        </asp:BoundField>
                                        <asp:ButtonField ButtonType="Image" HeaderText="Editar" ImageUrl="~/Img/edit.png" CommandName="Editar">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                        </asp:ButtonField>
                                        <asp:ButtonField ButtonType="Image" ImageUrl="~/Img/Eliminar.png" CommandName="Eliminar" HeaderText="Eliminar">
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                        </asp:ButtonField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12" style="text-align: center">
                                <asp:LinkButton ID="lbVolver" runat="server" OnClientClick="ShowBlockWindow();"
                                    CssClass="btn btn-warning" Text="Volver   &lt;i class=&quot;fa fa-reply-all fa-1x&quot; aria-hidden=&quot;true&quot;&gt;&lt;/i&gt;">
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group row">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
    </asp:Panel>
    <asp:Panel ID="pnMensajeSinAcceso" runat="server" BorderColor="Silver" BorderWidth="1px">
        <table width="100%">
            <tr>
                <td align="justify" class="Subtitle">
                    <h2>MODULO PARA GESTIONAR PERMISOS</h2>
                </td>
            </tr>
            <tr>
                <td class="footer" width="10%">
                    <p class="auto-style11" style="text-align: center">
                        <span class="auto-style18"><strong>No Tienes Acceso A Este Módulo Consulta Con El Administrador</strong></span>
                    </p>
                </td>
            </tr>
            <tr>
                <td class="footer" width="10%">&nbsp;<br />
                    <asp:Image ID="imgSinAcceso" runat="server" ImageUrl="~/Img/SinAcceso.png" />
                    &nbsp;<br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
</asp:Content>
