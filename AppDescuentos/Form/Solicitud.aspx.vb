﻿Imports Logistica.DataLayer
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Public Class Solicitud
    Inherits System.Web.UI.Page
    Dim ObjUniversal As New CLUniversal
    Dim ObRegisEvent As New CLUniversal
    Dim ObjImprenta As New ClImprenta
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5))

        If Session("UsuarioID") Is Nothing Then
            Response.Redirect("../Inicio/Login.aspx")
        End If

        '--------------------------------------Captura La IP y el nombre del equipo
        Dim strHostName As String = Dns.GetHostName()
        Dim ipEntry As IPHostEntry = Dns.GetHostEntry(strHostName)

        ViewState("IPAddress") = Convert.ToString(ipEntry.AddressList(ipEntry.AddressList.Length - 1))
        ViewState("HostName") = Convert.ToString(ipEntry.HostName)
        '--------------------------------------FIN Captura La IP y el nombre del equipo
        If Not Page.IsPostBack Then
            CargarSolicitudesXusuario()
        End If
        ValidarPermisos()
    End Sub
    Private Sub ValidarPermisos()
        Dim dtValPermis As DataTable = ObjUniversal.CRUDtabPermisos(Me.Session("UsuarioID"), Me.Session("TipoUser"), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "pnPermisos")
        Try
            If dtValPermis.Rows(0).Item("Solicitudes") = "True" Then
                pnPrincipalSolicitud.Visible = True
                pnMensajeSinAcceso.Visible = False
            Else
                pnPrincipalSolicitud.Visible = False
                pnMensajeSinAcceso.Visible = True
            End If

        Catch ex As Exception
            pnMensajeSinAcceso.Visible = True
        End Try
    End Sub
    Private Sub CargarSolicitudesXusuario()
        Dim dt As DataTable = ObjImprenta.Consultas2(Me.Session("SedeCod"), Me.Session("CentroCostoID"), 0, Me.Session("Usuario"), "SolicitudesUser")
        If dt.Rows.Count > 0 Then
            GvSolicitudes.DataSource = dt
            GvSolicitudes.DataBind()
        Else
            GvSolicitudes.DataSource = Nothing
            GvSolicitudes.DataBind()
        End If
    End Sub
    'Public Sub AlertNuevo(ByVal msg As String, ByRef P As Page)
    '    Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
    '    With strBuilder
    '        .Append("<script language='javascript'>")
    '        .Append("alert('" + msg + ".');")
    '        .Append("</script>")
    '        ClientScript.RegisterStartupScript(Me.GetType(), "Alert", strBuilder.ToString)
    '    End With
    'End Sub
    Protected Sub ibNuevo_Click(sender As Object, e As ImageClickEventArgs) Handles ibNuevo.Click

        LimpiarForm()

        Dim dt As DataTable = ObjImprenta.ValNuevaSol(Me.Session("SedeCod"), Me.Session("CentroCostoID"))

        If dt.Rows.Count > 0 Then

            Dim SolId As Integer = dt.Rows(0).Item("SolicitudID")

            If dt.Rows(0).Item("EstadoLigistica") = 2 Then
                HabilitarNuevaSol()
            Else
                If dt.Rows(0).Item("EstadoLigistica") = 1 Then
                    If dt.Rows(0).Item("EstadoAsignar") = 1 Then
                        If dt.Rows(0).Item("EstadoEvaluacion") = 1 Then
                            ibNuevo.Enabled = True
                            HabilitarNuevaSol()
                        Else
                            ObjUniversal.SendNotify("No se ha evaluado el servicio de la solicitud # " & SolId, "3")
                        End If
                    Else
                        ObjUniversal.SendNotify("Se encuentra en proceso la solicitud # " & SolId, "3")
                    End If
                Else
                    ObjUniversal.SendNotify("Se encuentra en proceso la solicitud # " & SolId, "3")
                End If
            End If
        Else
            ibNuevo.Enabled = True
            HabilitarNuevaSol()
        End If
    End Sub
    Private Sub LimpiarForm()
        'Datos Solicitante
        lFecha.Text = Nothing
        lSede.Text = Nothing
        lSolicitado.Text = Nothing
        lCargo.Text = Nothing
        lDependencia.Text = Nothing
        lCorreo.Text = Nothing
        'Encabezados
        tbEncargadoTrabajo.Text = Nothing
        tbCargoEncargado.Text = Nothing
        txbTelContac.Text = Nothing
        dtpFechaRequeControl.Value = Nothing
        txbLugarRequie.Text = Nothing
        txbEventoSoli.Text = Nothing
        'Limpiando detalles
        txbCantidad.Text = ""
        cbdiseno.Checked = Nothing
        cbImpresion.Checked = Nothing
        cbOtro.Checked = Nothing
        txbDescrip.Text = Nothing
        txbCaracteristicas.Text = Nothing
        'limìar grilla Detalles
        GvDetallesSolici.DataSource = Nothing
        GvDetallesSolici.DataBind()
    End Sub

    Private Sub HabilitarNuevaSol()

        'LisServiSoli()

        lMensaje.Visible = True
        lMensaje.Text = "NUEVA"
        lSolNum.Visible = False
        lSolNum.Text = Nothing

        pnFormulario.Visible = True
        pnGestionSolicitudes.Visible = False

        LimpiarForm()

        lFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")
        lSede.Text = Me.Session("SedeNombre")
        lDependencia.Text = Me.Session("NomCentroCosto")
        lSolicitado.Text = Me.Session("Nombre")
        lCargo.Text = Me.Session("Cargo")
        lCorreo.Text = Me.Session("Correo")

        'habilitar encabezado solicitud
        txbTelContac.Enabled = True
        'dtpFechaRealizaControl.Enabled = True

        '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' ''dtpFechaRequeControl.EnableTheming = True
        txbLugarRequie.Enabled = True
        txbEventoSoli.Enabled = True
        FileUpload1.Enabled = True
        '------------------------------------
        lbModifEnca.Visible = False
        lbSubirAdjunt.Visible = False
        ibModificarMate.Visible = False
        ibCancelModMate.Visible = False
        '-----------------------------------
        ibGuardarMate.Visible = True
        ibLimpiar.Visible = True

        Me.ViewState("SolicitudID") = 0

        Me.ViewState("EstadoAccion") = 0

    End Sub
    Private Sub BuscarDetalleSoli()
        Dim dtDeta As DataTable = ObjImprenta.Consultas(Me.ViewState("SolicitudID"), "ConsulDeta")
        GvDetallesSolici.DataSource = dtDeta
        GvDetallesSolici.DataBind()
    End Sub
    Protected Sub GvDetallesSolici_PageIndexChanged(sender As Object, e As EventArgs) Handles GvDetallesSolici.PageIndexChanged
        BuscarDetalleSoli()
    End Sub
    Protected Sub GvDetallesSolici_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GvDetallesSolici.PageIndexChanging
        Me.GvDetallesSolici.PageIndex = e.NewPageIndex()
        Me.GvDetallesSolici.DataBind()
    End Sub
    Private Sub BuscarSolici()
        Dim dtDeta As DataTable = ObjImprenta.Consultas(Me.ViewState("SolicitudID"), "ConsulDeta")
        GvDetallesSolici.DataSource = dtDeta
        GvDetallesSolici.DataBind()
    End Sub
    'Public Sub LisServiSoli()
    '    Dim dt As DataTable = ObjUniversal.Listas("Servicio")
    '    If dt.Rows.Count > 0 Then
    '        With Me.ddlServicioSoli
    '            .Items.Clear()
    '            .DataSource = dt
    '            .DataValueField = "ServicioID"
    '            .DataTextField = "Nombre"
    '            .DataBind()
    '            .Items.Add(New ListItem("Selecciones el Servicio Solicitado", "-1"))
    '        End With
    '    End If
    '    Me.ddlServicioSoli.SelectedValue = -1
    'End Sub
    Protected Sub GvDetallesSolici_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvDetallesSolici.RowCommand
        If e.CommandName = "Editar" Then

            'Limpiando detalles
            txbCantidad.Text = ""
            cbdiseno.Checked = Nothing
            cbImpresion.Checked = Nothing
            cbOtro.Checked = Nothing
            txbDescrip.Text = ""
            txbCaracteristicas.Text = ""
            '---------------------------

            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim DetalleID As Integer = Me.GvDetallesSolici.DataKeys(index).Values(0)

            Dim ds As DataSet = ObjImprenta.CRUDdetallesDS(DetalleID, 0, "", "", "", 0, "", "Buscar")

            Dim dt1 As DataTable = ds.Tables(0)
            Dim dt2 As DataTable = ds.Tables(1)

            If dt1.Rows.Count > 0 Then

                ibGuardarMate.Visible = False
                ibLimpiar.Visible = False
                ibModificarMate.Visible = True
                ibCancelModMate.Visible = True

                txbCantidad.Text = dt1.Rows(0).Item("Cantidad")

                For i = 0 To dt2.Rows.Count - 1
                    If dt2.Rows(i).Item(0) = 1 Then
                        cbdiseno.Checked = True
                    End If

                    If dt2.Rows(i).Item(0) = 2 Then
                        cbImpresion.Checked = True
                    End If

                    If dt2.Rows(i).Item(0) = 3 Then
                        cbOtro.Checked = True
                    End If
                Next

                txbDescrip.Text() = dt1.Rows(0).Item("Descripcion")
                txbCaracteristicas.Text = dt1.Rows(0).Item("Caracteristicas")
                ViewState("DetalleID") = dt1.Rows(0).Item("DetalleID")

                'EventLog
                Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Realizo Accion Editar DetalleID: " & DetalleID, ViewState("IPAddress"), ViewState("HostName"))

            Else
                ObjUniversal.SendNotify("No se encontró registro", "4")
            End If
        End If


        If e.CommandName = "Eliminar" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim DetalleID As Integer = Me.GvDetallesSolici.DataKeys(index).Values(0)

            Dim dt As DataTable = ObjImprenta.CRUDdetalles(DetalleID, 0, 0, "", "", lSolNum.Text, "", "ElimiProductos")

              Select dt.Rows(0).Item("Existe")
                Case 1
                    BuscarDetalleSoli()
                    ObjUniversal.SendNotify("La solicitud no puede quedar sin detalles ", "3")
                Case 2
                    BuscarDetalleSoli()
                    'EventLog
                    Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Elimino el Registro o (Material) Numero:" & DetalleID, ViewState("IPAddress"), ViewState("HostName"))
                    ObjUniversal.SendNotify("Registro eliminado ", "1")
                Case 3
                    BuscarDetalleSoli()
                    'EventLog
                    Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Intento Eliminar el Registro o (Material) Numero:" & DetalleID, ViewState("IPAddress"), ViewState("HostName"))
                    ObjUniversal.SendNotify("El registro no se eliminado inténtelo de nuevo", "4")
            End Select
        End If

    End Sub
    Protected Sub GvSolicitudes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GvSolicitudes.RowCommand

        If e.CommandName = "EditarSol" Then

            LimpiarForm()

            ' de esta forma capturo de una vez el ID de la solicitud sin necesidad de guiarme por el índice tal como lo hace el código siguiente que esta comentario
            Dim SolicitudID As Integer = Convert.ToInt32(e.CommandArgument)

            'Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            'Dim SolicitudID As Integer = Me.GvSolicitudes.DataKeys(index).Values(0)

            Dim dt As DataTable = ObjImprenta.Consultas(SolicitudID, "SolDet")

            If dt.Rows.Count > 0 Then

                Me.ViewState("SolicitudID") = dt.Rows(0).Item("SolSolicitudID")

                'LisServiSoli()

                lMensaje.Visible = True
                lMensaje.Text = "MODIFICAR SOLICITUD #"
                lSolNum.Visible = True
                lSolNum.Text = dt.Rows(0).Item("SolSolicitudID")


                lbModifEnca.Visible = True
                lbSubirAdjunt.Visible = True
                ibGuardarMate.Visible = True
                ibLimpiar.Visible = True
                pnFormulario.Visible = True

                tbEncargadoTrabajo.Text = True
                tbCargoEncargado.Text = True
                txbTelContac.Enabled = True
                'rdpFechaRequi.Enabled = True
                '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' ''dtpFechaRequeControl.EnableTheming = True
                txbLugarRequie.Enabled = True
                txbEventoSoli.Enabled = True
                FileUpload1.Enabled = True
                Me.ViewState("EstadoAccion") = 1

                ibModificarMate.Visible = False
                ibCancelModMate.Visible = False
                pnGestionSolicitudes.Visible = False

                lFecha.Text = dt.Rows(0).Item("Fecha")
                lSede.Text = dt.Rows(0).Item("NomSede")
                lDependencia.Text = dt.Rows(0).Item("CentroCosto")
                lSolicitado.Text = dt.Rows(0).Item("SolicitadoPor")
                lCargo.Text = dt.Rows(0).Item("Cargo")
                lCorreo.Text = dt.Rows(0).Item("CorreoElectro")
                tbEncargadoTrabajo.Text = dt.Rows(0).Item("EncargadoTrabajo")
                tbCargoEncargado.Text = dt.Rows(0).Item("CargoEncargado")
                txbTelContac.Text = dt.Rows(0).Item("TelContacto")
                dtpFechaRequeControl.Value = dt.Rows(0).Item("FechaRequiere")
                txbLugarRequie.Text = dt.Rows(0).Item("Lugar")
                txbEventoSoli.Text = dt.Rows(0).Item("Evento")

                GvDetallesSolici.DataSource = dt
                GvDetallesSolici.DataBind()
            Else
                ObjUniversal.SendNotify("No se encontro datos de la solicitud # " & SolicitudID, "4")
            End If

        End If

        If e.CommandName = "VerAdjunto" Then

            Dim SolicitudID As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Id As String = HttpUtility.UrlEncode(Encrypt(SolicitudID))

            'Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            'Dim Id As String = HttpUtility.UrlEncode(Encrypt(Me.GvSolicitudes.DataKeys(index).Values(0)))

            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
            With strBuilder
                .Append("<script language='javascript'>")
                .Append("window.open('" & "../SolicitudImprenta/SolicitudGetFile.aspx?Id=" & Id & "', '_blank');")
                .Append("</script>")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
            End With
        End If

        If e.CommandName = "EvaluarServicio" Then
            Dim SolicitudID As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Id As String = HttpUtility.UrlEncode(Encrypt(SolicitudID))

            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder

            With strBuilder
                .Append("<script language='javascript'>")
                .Append("window.open('" & "../SolicitudImprenta/SolicitudEvalua.aspx?Id=" & Id & "', '_blank');")
                .Append("</script>")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
            End With
        End If

        If e.CommandName = "VerSolVAF" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Id As String = HttpUtility.UrlEncode(Encrypt(Me.GvSolicitudes.DataKeys(index).Values(0)))

            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder

            With strBuilder
                .Append("<script language='javascript'>")
                .Append("window.open('" & "../Informes/Solicitud/RptSolicitud.aspx?Id=" & Id & "', '_blank');")
                .Append("</script>")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
            End With
        End If

        If e.CommandName = "btnBitacora" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim Id As String = HttpUtility.UrlEncode(Encrypt(Me.GvSolicitudes.DataKeys(index).Values(0)))

            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder

            With strBuilder
                .Append("<script language='javascript'>")
                .Append("window.open('" & "../SolicitudImprenta/Bitacora.aspx?Id=" & Id & "', '_blank');")
                .Append("</script>")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
            End With
        End If

    End Sub
    Private Function Encrypt(clearText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(clearText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                clearText = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return clearText
    End Function
    Protected Sub GvSolicitudes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GvSolicitudes.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim ibEditar As System.Web.UI.WebControls.ImageButton = DirectCast(e.Row.FindControl("ibEditar"), System.Web.UI.WebControls.ImageButton)
            Dim ibEvaluar As System.Web.UI.WebControls.ImageButton = DirectCast(e.Row.FindControl("ibEvaluar"), System.Web.UI.WebControls.ImageButton)
            Dim ibVerAdjunto As System.Web.UI.WebControls.ImageButton = DirectCast(e.Row.FindControl("ibVerAdjunto"), System.Web.UI.WebControls.ImageButton)

            Dim lAproLogistica As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lAproLogistica"), System.Web.UI.WebControls.Label)
            Dim lAsigImprenta As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lAsigImprenta"), System.Web.UI.WebControls.Label)
            Dim ltamano As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("ltamano"), System.Web.UI.WebControls.Label)

            ' Valido Acceso al Boton Editar 'e5e6e8
            If lAproLogistica.Text = "Pendiente" Then
                ibEditar.Enabled = True
                ibEditar.ImageUrl = "~/Img/EditarSolicitudOn.png"
            Else
                ibEditar.Enabled = False
                ibEditar.ImageUrl = "~/Img/EditarSolicitudOff.png"
            End If

            ' Valido Acceso al Boton Evaluar
            If lAsigImprenta.Text = "Si" Then
                ibEvaluar.Enabled = True
                ibEvaluar.ImageUrl = "~/Img/EvaluarSolicitudOn.png"
            Else
                ibEvaluar.Enabled = False
                ibEvaluar.ImageUrl = "~/Img/EvaluarSolicitudOff.png"
            End If

            'Valido si la solicitud tiene adjunto
            If ltamano.Text = 0 Then
                ibVerAdjunto.Enabled = False
                ibVerAdjunto.ImageUrl = "~/Img/VerAdjunOff.png"
                ltamano.Visible = False
            Else
                ibVerAdjunto.Enabled = True
                ibVerAdjunto.ImageUrl = "~/Img/VerAdjunOn.png"
                ltamano.Visible = False
            End If

        End If
    End Sub
    Protected Sub GvSolicitudes_PageIndexChanged(sender As Object, e As EventArgs) Handles GvSolicitudes.PageIndexChanged
        CargarSolicitudesXusuario()
    End Sub
    Protected Sub GvSolicitudes_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GvSolicitudes.PageIndexChanging
        Me.GvSolicitudes.PageIndex = e.NewPageIndex()
        Me.GvSolicitudes.DataBind()
    End Sub

    Protected Sub lbModifEnca_Click(sender As Object, e As EventArgs) Handles lbModifEnca.Click
        '---------------------Validaciones campos del encabezado--------------------------
        If txbTelContac.Text = "" Then
            ObjUniversal.SendNotify("El teléfono de contacto es obligatorio", "3")
            txbTelContac.Focus()
            Exit Sub
        End If
        If IsDBNull(dtpFechaRequeControl.Value) Then
            ObjUniversal.SendNotify("La fecha en que se requiere es obligatorio", "3")
            Exit Sub
        End If
        If IsNothing(dtpFechaRequeControl.Value) Then
            ObjUniversal.SendNotify("La fecha en que se requiere es obligatorio", "3")
            dtpFechaRequeControl.Focus()
            Exit Sub
        End If
        If txbLugarRequie.Text = "" Then
            ObjUniversal.SendNotify("El lugar en que se requiere es obligatorio", "3")
            txbLugarRequie.Focus()
            Exit Sub
        End If
        If txbEventoSoli.Text = "" Then
            ObjUniversal.SendNotify("El evento o tema de la solicitud es obligatorio", "3")
            txbEventoSoli.Focus()
            Exit Sub
        End If

        '--------------------FIN---Validaciones campos del encabezado--------------------------

        Dim dt As DataTable = ObjImprenta.ModifiEncabezado(Me.ViewState("SolicitudID"), tbEncargadoTrabajo.Text, tbCargoEncargado.Text, txbTelContac.Text,
                                                            dtpFechaRequeControl.Value, txbLugarRequie.Text, txbEventoSoli.Text)

        'EventLog
        Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Modifico Encabezado De La Solicitud Número:" & Me.ViewState("SolicitudID"), ViewState("IPAddress"), ViewState("HostName"))
        ObjUniversal.SendNotify("Los datos del encabezado se actualizaron", "1")
    End Sub

    Protected Sub lbVolver_Click(sender As Object, e As EventArgs) Handles lbVolver.Click
        LimpiarForm()
        pnFormulario.Visible = False
        pnGestionSolicitudes.Visible = True
        CargarSolicitudesXusuario()
    End Sub
    Protected Sub lbSubirAdjunt_Click(sender As Object, e As EventArgs) Handles lbSubirAdjunt.Click
        Dim sExt As String = String.Empty
        Dim sName As String = String.Empty
        Dim intDocFileLength1 As Integer = Me.FileUpload1.PostedFile.ContentLength
        Dim fileExtension As String = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower()


        If FileUpload1.HasFile Then

            'primero Valido Tamaño
            If intDocFileLength1 > 10096000 Then '10MB
                ObjUniversal.SendNotify("El tamaño del archivo excede el límite de 10mb", "3")
                Exit Sub
            End If

            sName = FileUpload1.FileName
            sExt = Path.GetExtension(sName)

            'Luego Valido la extension del archivo 
            If ValidaExtension(sExt) Then
                Dim dt As DataTable = ObjImprenta.SubirAdjunto(Me.ViewState("SolicitudID"), FileUpload1.FileBytes, FileUpload1.FileBytes.Length.ToString(), fileExtension)

                If dt.Rows.Count > 0 Then
                    'EventLog
                    Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Ingreso Por Editar/ Adjunto Archivo A La Solicitud:" & Me.ViewState("SolicitudID"), ViewState("IPAddress"), ViewState("HostName"))
                    ObjUniversal.SendNotify("Se adjuntó el archivo", "1")
                Else
                    'EventLog
                    Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Ingreso Por Editar/ ERROR al tratar de  Adjuntar Archivo A La Solicitud:" & Me.ViewState("SolicitudID"), ViewState("IPAddress"), ViewState("HostName"))
                    ObjUniversal.SendNotify("Error: No se adjuntó el archivo", "4")
                End If
            Else
                ObjUniversal.SendNotify("Solo se acepta imagenes (pdf - doc - docx - xlsx - xls - jpg - gif - tiff - psd - ppt - pptx) ", "3")
                Exit Sub
            End If

        Else
            ObjUniversal.SendNotify("Por favor seleccione el archivo que va adjuntar a la solicitud", "3")
        End If
    End Sub
    Private Function ValidaExtension(ByVal sExtension As String) As Boolean
        Select Case sExtension
            Case ".pdf", ".doc", ".docx", ".xlsx", ".xls", ".jpg", ".gif", ".tiff", ".psd", ".ppt", ".pptx"
                Return True
            Case Else
                Return False
        End Select
    End Function
    Protected Sub ibModificarMate_Click1(sender As Object, e As EventArgs) Handles ibModificarMate.Click
        '--------------------------Validaciones detalles solicitud----------------------------
        'Valida solo numeros 
        If ObjUniversal.ComprobarFormatoNumeros(txbCantidad.Text) = False Then
            txbCantidad.Focus()
            ObjUniversal.SendNotify(" El Campo (Cantidad) debe contener solo numeros", "3")
            Exit Sub
        End If

        If txbCantidad.Text = "" Then
            ObjUniversal.SendNotify("Ingrese la cantidad a solicitar", "3")
            txbCantidad.Focus()
            Exit Sub
        End If
        '----------------------------------------------------------------
        'If ddlServicioSoli.SelectedValue = -1 Then
        '    RadWindowManager1.RadAlert("Selecciones el servicio solicitado", 500, 150, "Modulo de solicitudes", Nothing)
        '    Exit Sub
        'End If

        If cbdiseno.Checked = False And cbImpresion.Checked = False And cbOtro.Checked = False Then
            ObjUniversal.SendNotify("Selecciones Algun servicio solicitado", "3")
            Exit Sub
        End If

        '----------------------------------------------------------------
        If Len(Trim(txbDescrip.Text)) = 0 Then
            ObjUniversal.SendNotify("El campo descripción no debe estar vacío", "3")
            txbDescrip.Focus()
            Exit Sub
        End If
        '----------------------------------------------------------------
        If Len(Trim(txbCaracteristicas.Text)) = 0 Then
            ObjUniversal.SendNotify("Escriba las características", "3")
            txbCaracteristicas.Focus()
            Exit Sub
        End If

        '--------------------------------------------------------------------------------------------------------

        '------------------------------------------------------ DATOS DEL SERVICIO--------------------------------
        Dim Servicio As String = ""
        Dim Diseno As String
        Dim Impresion As String
        Dim Otro As String

        If cbdiseno.Checked = True Then
            Diseno = "1"
            Servicio = Diseno
        End If
        If cbImpresion.Checked = True Then
            Impresion = "2"
            Servicio = Servicio & "-" & Impresion
        End If
        If cbOtro.Checked = True Then
            Otro = "3"
            Servicio = Servicio & "-" & Otro
        End If

        Dim dt As DataTable = ObjImprenta.CRUDdetalles(ViewState("DetalleID"), txbCantidad.Text, Servicio, txbDescrip.Text, txbCaracteristicas.Text, 0, "", "Actualizar")

        If dt.Rows.Count > 0 Then
            BuscarDetalleSoli()
            ibModificarMate.Visible = False
            ibCancelModMate.Visible = False
            ibGuardarMate.Visible = True
            'Limpio Combox despues de haber actualizado
            txbCantidad.Text = ""
            cbdiseno.Checked = Nothing
            cbImpresion.Checked = Nothing
            cbOtro.Checked = Nothing
            txbDescrip.Text = ""
            txbCaracteristicas.Text = ""
            '------------------------------------------
            'EventLog
            Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Modifico registro o (Material) Numero:" & ViewState("DetalleID"), ViewState("IPAddress"), ViewState("HostName"))
            ObjUniversal.SendNotify("Se actualizo el registro", "1")
        Else
            'EventLog
            Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "No Se Modifico registro o (Material) Numero:" & ViewState("DetalleID"), ViewState("IPAddress"), ViewState("HostName"))
            ObjUniversal.SendNotify("El registro no se actualizo inténtelo de nuevo", "4")
        End If
    End Sub
    Protected Sub ibCancelModMate_Click1(sender As Object, e As EventArgs) Handles ibCancelModMate.Click
        'EventLog
        Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Cancelo La Modificacion Del Registro o (Material) Numero:" & ViewState("DetalleID"), ViewState("IPAddress"), ViewState("HostName"))

        'Limpiando detalles
        txbCantidad.Text = ""
        cbdiseno.Checked = Nothing
        cbImpresion.Checked = Nothing
        cbOtro.Checked = Nothing
        txbDescrip.Text = ""
        txbCaracteristicas.Text = ""
        '---------------------------
        ibModificarMate.Visible = False
        ibCancelModMate.Visible = False
        ibGuardarMate.Visible = True
        ibLimpiar.Visible = True
    End Sub

    Protected Sub ibGuardarMate_Click(sender As Object, e As EventArgs) Handles ibGuardarMate.Click
        '---------------------Validaciones campos del encabezado--------------------------

        If Len(Trim(lSolicitado.Text)) = 0 And Len(Trim(lCargo.Text)) = 0 And Len(Trim(lDependencia.Text)) = 0 And Len(Trim(lCorreo.Text)) = 0 Then
            ObjUniversal.SendNotify("Los campos del encabezado son obligatorios", "3")
            Exit Sub
        End If

        If txbTelContac.Text = "" Then
            ObjUniversal.SendNotify("El teléfono de contacto es obligatorio", "3")
            Exit Sub
        End If
        If IsDBNull(dtpFechaRequeControl.Value) Then
            ObjUniversal.SendNotify("La fecha en que se requiere es obligatorio", "3")
            Exit Sub
        End If
        If IsNothing(dtpFechaRequeControl.Value) Then
            ObjUniversal.SendNotify("La fecha en que se requiere es obligatorio", "3")
            Exit Sub
        End If
        If txbLugarRequie.Text = "" Then
            ObjUniversal.SendNotify("El lugar en que se requiere es obligatorio", "3")
            Exit Sub
        End If
        If txbEventoSoli.Text = "" Then
            ObjUniversal.SendNotify("El evento o tema de la solicitud es obligatorio", "3")
            Exit Sub
        End If

        '--------------------FIN---Validaciones campos del encabezado--------------------------

        '--------------------------Validaciones detalles solicitud----------------------------
        'Valida solo numeros 
        If ObjUniversal.ComprobarFormatoNumeros(txbCantidad.Text) = False Then
            txbCantidad.Focus()
            ObjUniversal.SendNotify(" El Campo (Cantidad) debe contener solo numeros", "3")
            Exit Sub
        End If
        If Len(Trim(txbCantidad.Text)) = 0 Then
            ObjUniversal.SendNotify("Ingrese la cantidad a solicitar", "3")
            txbCantidad.Focus()
            Exit Sub
        End If
        '----------------------------------------------------------------
        'If ddlServicioSoli.SelectedValue = -1 Then
        '    RadWindowManager1.RadAlert("Selecciones el servicio solicitado", 500, 150, "Modulo de solicitudes", Nothing)
        '    Exit Sub
        'End If

        If cbdiseno.Checked = False And cbImpresion.Checked = False And cbOtro.Checked = False Then
            ObjUniversal.SendNotify("Selecciones Algun servicio solicitado", "3")
            Exit Sub
        End If

        '----------------------------------------------------------------
        If txbDescrip.Text = "" Then
            ObjUniversal.SendNotify("El campo descripción no debe estar vacío", "3")
            txbDescrip.Focus()
            Exit Sub
        End If
        '----------------------------------------------------------------
        If txbCaracteristicas.Text = "" Then
            ObjUniversal.SendNotify("Escriba las características", "3")
            txbCaracteristicas.Focus()
            Exit Sub
        End If

        '--------------------------------------------------------------------------------------------------------
        'Valido que el control FileUpload no este vacio  
        If FileUpload1.HasFile Then
            Dim intDocFileLength1 As Integer = Me.FileUpload1.PostedFile.ContentLength
            Dim strPostedFileName1 As String = Me.FileUpload1.PostedFile.FileName
            'primero Valido Tamaño
            If intDocFileLength1 > 10096000 Then '10MB
                ObjUniversal.SendNotify("El tamaño del archivo excede el límite de 10mb", "3")
                Exit Sub
            End If
            'Luego Valido la extension del archivo 
            If (strPostedFileName1 <> String.Empty) Then
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName1).ToLower
                If (strExtn <> ".pdf") Then
                    ObjUniversal.SendNotify("Por favor  adjuntar el archivo  en formato pdf", "3")
                    Exit Sub
                End If
            End If
        End If
        '--------------------------------------------------------------------------------------------------------

        '------------------------------------------------------ DATOS DEL SERVICIO--------------------------------
        Dim Servicio As String = ""
        Dim Diseno As String
        Dim Impresion As String
        Dim Otro As String

        If cbdiseno.Checked = True Then
            Diseno = "1"
            Servicio = Diseno
        End If
        If cbImpresion.Checked = True Then
            Impresion = "2"
            Servicio = Servicio & "-" & Impresion
        End If
        If cbOtro.Checked = True Then
            Otro = "3"
            Servicio = Servicio & "-" & Otro
        End If


        Dim SolicitudID As Integer = Me.ViewState("SolicitudID")

        Dim dt As New DataTable
        dt = ObjImprenta.InsertSolicitud(SolicitudID, Me.Session("SedeCod"), Me.Session("Usuario"), Me.Session("TipoUser"), Me.Session("SedeNombre"), Me.Session("CentroCostoID"), Me.Session("NomCentroCosto"), Me.Session("UsuarioID"), Me.Session("Nombre"),
                                          Me.Session("Cargo"), tbEncargadoTrabajo.Text, tbCargoEncargado.Text, txbTelContac.Text, Me.Session("Correo"), dtpFechaRequeControl.Value,
                                          txbLugarRequie.Text, txbEventoSoli.Text, txbCantidad.Text, Servicio, txbDescrip.Text, txbCaracteristicas.Text,
                                          FileUpload1.FileBytes, FileUpload1.FileBytes.Length.ToString())

        If Me.ViewState("SolicitudID") = 0 Then
            'EventLog
            Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Diligencio Una Nueva Solicitud e Inserto Detalle", ViewState("IPAddress"), ViewState("HostName"))
        Else
            'EventLog
            Dim dtEvent As DataTable = ObRegisEvent.EventLog(Me.Session("UsuarioID"), Me.Session("Nombre"), Me.Session("SedeCod"), "Imprenta", "Solicitud", "Inserto Detalle A La Solicitud: " & ViewState("SolicitudID"), ViewState("IPAddress"), ViewState("HostName"))
        End If

        If Me.ViewState("SolicitudID") = 0 Then
            SolicitudID = dt.Rows(0).Item("Id")
            Me.ViewState("SolicitudID") = dt.Rows(0).Item("Id")
        End If

        If Me.ViewState("EstadoAccion") = 0 Then
            tbEncargadoTrabajo.Enabled = False
            tbCargoEncargado.Enabled = False
            txbTelContac.Enabled = False
            'rdpFechaRequi.Enabled = False
            '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' ''dtpFechaRequeControl.EnableTheming = False
            txbLugarRequie.Enabled = False
            txbEventoSoli.Enabled = False
            FileUpload1.Enabled = False
        End If
        If Me.ViewState("EstadoAccion") = 1 Then
            txbTelContac.Enabled = True
            'rdpFechaRequi.Enabled = True
            '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' '' ''dtpFechaRequeControl.EnableTheming = True
            txbLugarRequie.Enabled = True
            txbEventoSoli.Enabled = True
            FileUpload1.Enabled = True

        End If
        'Limpiando detalles
        txbCantidad.Text = ""
        cbdiseno.Checked = Nothing
        cbImpresion.Checked = Nothing
        cbOtro.Checked = Nothing
        txbDescrip.Text = ""
        txbCaracteristicas.Text = ""
        '---------------------------
        BuscarDetalleSoli()
        ObjUniversal.SendNotify("Se realizó la acción verificar el ingreso del nuevo registro", "1")
    End Sub
    Protected Sub ibLimpiar_Click(sender As Object, e As EventArgs) Handles ibLimpiar.Click
        txbCantidad.Text = ""
        cbdiseno.Checked = Nothing
        cbImpresion.Checked = Nothing
        cbOtro.Checked = Nothing
        txbDescrip.Text = ""
        txbCaracteristicas.Text = ""
    End Sub
End Class