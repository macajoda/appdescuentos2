﻿Public Class Grupos
    Inherits System.Web.UI.Page
    Dim ObjUniversal As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Consulta()
            Maestros()
        End If
    End Sub
    Public Sub Maestros()
        Dim dt As DataTable = ObjUniversal.Usuario(0, "", "", "", "", "", 0, 0, "", "", 0, "Maestros")

        If dt.Rows.Count > 0 Then
            With Me.ddlMaestroAdd
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "UsuarioId"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione maestro", "-1"))
            End With

            ddlMaestroAdd.SelectedValue = -1

            With Me.ddlMaestroUpd
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "UsuarioId"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione maestro", "-1"))
            End With

            ddlMaestroUpd.SelectedValue = -1
        End If
    End Sub
    Public Sub Consulta()

        Dim dt As DataTable = ObjUniversal.CRUDGrupos(0, Trim(txtNombre.Text), 0, 0, 0, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Grupos") = dt
            GVGrupos.DataSource = dt
            GVGrupos.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Grupos") = Nothing
            GVGrupos.DataSource = Nothing
            GVGrupos.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Consulta()
    End Sub

    Protected Sub lbNewGrupos_Click(sender As Object, e As EventArgs) Handles lbNewGrupos.Click
        LimpiarCrear()
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddGrupos", "FuncionModalAddGrupos();", True)
    End Sub
    Public Sub LimpiarCrear()
        txtNombreAdd.Text = ""
        txtEdad1Add.Text = ""
        txtEdad2Add.Text = ""
        ddlMaestroAdd.SelectedValue = -1
    End Sub

    Protected Sub lbAddGrupos_Click(sender As Object, e As EventArgs) Handles lbAddGrupos.Click
        If Len(Trim(txtNombreAdd.Text)) = 0 Or Len(Trim(txtEdad1Add.Text)) = 0 Or Len(Trim(txtEdad2Add.Text)) = 0 Or ddlMaestroAdd.SelectedValue = -1 Then
            ObjUniversal.SendNotify(" Por favor verificar campos en rojo, son requeridos ", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddGrupos", "FuncionModalAddGrupos();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniversal.CRUDGrupos(0, Trim(txtNombreAdd.Text), ddlMaestroAdd.SelectedValue, Trim(txtEdad1Add.Text), Trim(txtEdad2Add.Text), "Crear")

        If dt.Rows.Count > 0 Then
            ObjUniversal.SendNotify(" Registro creado  ", "1")
            Consulta()
        Else
            ObjUniversal.SendNotify("No fue creado el registro", "3")
        End If
    End Sub

    Protected Sub GVGrupos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVGrupos.RowCommand
        If e.CommandName = "EditarGrupos" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModalUpdGrupos", "FuncionModalUpdGrupos();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("GrupoId") = Me.GVGrupos.DataKeys(index).Values(0)
            Limpiarmodificar()
            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")
            Dim dt As DataTable = ObjUniversal.CRUDGrupos(ViewState("GrupoId"), "", 0, 0, 0, "BuscarId")

            If dt.Rows.Count > 0 Then
                txtNombreUpd.Text = dt.Rows(0).Item("Nombre")
                txtEdad1Upd.Text = dt.Rows(0).Item("Edad1")
                txtEdad2Upd.Text = dt.Rows(0).Item("Edad2")
                ddlMaestroUpd.Text = dt.Rows(0).Item("UsuarioId")
            End If
        End If
    End Sub
    Public Sub Limpiarmodificar()
        txtNombreUpd.Text = ""
        txtEdad1Upd.Text = ""
        txtEdad2Upd.Text = ""
        ddlMaestroUpd.SelectedValue = -1
    End Sub
    Protected Sub lbActualizar_Click(sender As Object, e As EventArgs) Handles lbActualizar.Click
        If Len(Trim(txtNombreUpd.Text)) = 0 Or Len(Trim(txtEdad1Upd.Text)) = 0 Or Len(Trim(txtEdad2Upd.Text)) = 0 Or ddlMaestroUpd.SelectedValue = -1 Then
            ObjUniversal.SendNotify(" Por favor verificar campos en rojo, son requeridos ", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalUpdGrupos", "FuncionModalUpdGrupos();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniversal.CRUDGrupos(ViewState("GrupoId"), Trim(txtNombreUpd.Text), ddlMaestroUpd.SelectedValue, Trim(txtEdad1Upd.Text), Trim(txtEdad2Upd.Text), "Editar")
        ObjUniversal.SendNotify(" Registro actualizado  ", "1")
        Consulta()
    End Sub
   
    Protected Sub GVGrupos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVGrupos.RowDeleting
        Dim ID As Integer = Me.GVGrupos.DataKeys(e.RowIndex).Value

        Dim dt As DataTable = ObjUniversal.CRUDGrupos(ID, "", 0, 0, 0, "Eliminar")

        ObjUniversal.SendNotify(" Registro  eliminado ", "1")
        Consulta()
    End Sub

    Protected Sub GVGrupos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVGrupos.PageIndexChanged
        Consulta()
    End Sub

    Protected Sub GVGrupos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVGrupos.PageIndexChanging
        Me.GVGrupos.PageIndex = e.NewPageIndex()
        Me.GVGrupos.DataBind()
    End Sub
End Class