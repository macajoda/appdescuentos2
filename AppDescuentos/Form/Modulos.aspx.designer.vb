﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Modulos

    '''<summary>
    '''Control btn_SolicitudDesc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_SolicitudDesc As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_Solicitudes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_Solicitudes As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_Dependencias.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_Dependencias As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_TipoDescuentos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_TipoDescuentos As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_DocumentosReq.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_DocumentosReq As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_DocTipoDescuentos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_DocTipoDescuentos As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_DepTipoDescuentos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_DepTipoDescuentos As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_Estados.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_Estados As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_TipoContrato.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_TipoContrato As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control btn_Informes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btn_Informes As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control CardTemplates.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CardTemplates As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control pnAlert.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnAlert As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control lMensaje1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lMensaje1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lMensaje2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lMensaje2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control lMensajeDanger.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lMensajeDanger As Global.System.Web.UI.WebControls.Label
End Class
