﻿Public Class SolicitudSeguimiento
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ' Buscar()
            'BuscarSolicitudDependencia()
            Periodos()
            TipoDescuentos()
            Estados()
            BuscarSolicitudSeguimiento()
            Dependencias()


        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUDSolicitudes(0, "", 0, 0, 0, "", "", 0, "", "", "", "", "", "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Solicitudes") = dt
            GVSolicitudes.DataSource = dt
            GVSolicitudes.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Solicitudes") = Nothing
            GVSolicitudes.DataSource = Nothing
            GVSolicitudes.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    Public Sub BuscarSolicitudSeguimiento()
        Dim DependenciaID, Administrador As Integer

        If Me.Session("Administrador") = True Then
            Administrador = 1
            DependenciaID = -1
        Else
            Administrador = 0
            DependenciaID = Session("IDuser")
        End If

        'Session("IDuser")

        Dim dt As DataTable = ObjUniver.SolicitudSeguimiento(DependenciaID, ddlTipoDescuentobuscar.SelectedValue, ddlEstado.SelectedValue, ddlPeriodo.SelectedValue, Administrador, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Solicitudes") = dt
            GVSolicitudes.DataSource = dt
            GVSolicitudes.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Solicitudes") = Nothing
            GVSolicitudes.DataSource = Nothing
            GVSolicitudes.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    Public Sub BuscarSolicitudDependencia()
        Dim SolicitudID As Integer = ViewState("SolicitudID")
        'If ddlDependencia.SelectedValue <> -1 Then
        '    SolicitudID = -1
        'End If

        Dim dt As DataTable = ObjUniver.CRUBSolicitudDependencia(0, SolicitudID, ddlDependencia.SelectedValue, "", "", "", "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Documentos") = dt
            GVAsignarDep.DataSource = dt
            GVAsignarDep.DataBind()

            'GvObservaDep.DataSource = dt
            'GvObservaDep.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Documentos") = Nothing
            GVAsignarDep.DataSource = Nothing
            GVAsignarDep.DataBind()

            'GvObservaDep.DataSource = Nothing
            'GvObservaDep.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    Protected Sub GVSolicitudes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVSolicitudes.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        ViewState("SolicitudID") = Me.GVSolicitudes.DataKeys(index).Values(0)
        BuscarDatosPersonales(ViewState("SolicitudID"))
        If e.CommandName = "Dependencias" Then
            txtObservacionCyC.Text = ""
            If Me.Session("Administrador") = True Then
                'ddlDependencia.Visible = True
                'lblDependencia.Visible = True
                ddlDependencia.SelectedValue = -1
                lblTituloModal.Text = "Asignar dependencia"
                lblTituloDep.Text = "ASIGNAR DEPENDENCIA"
                lblObservDep.Text = "Observación C&C"
                lblAsignarDep.Text = "Asignar"

            Else

                GVAsignarDep.Columns(6).Visible = False

                ddlDependencia.SelectedValue = Session("IDuser")
                ddlDependencia.Enabled = False
                lblAsignarDep.Text = "Guardar"
                lblTituloModal.Text = "Emitir concepto"

                lblTituloDep.Text = "EMITIR CONCEPTO POR LA DEPENDENCIA"
                lblObservDep.Text = "Observación dependencia"

            End If
            BuscarSolicitudDependencia()
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)

        End If
        If e.CommandName = "Descuentos" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModalaAplicarDescuento", "FuncionModalaAplicarDescuento();", True)
        End If
        If e.CommandName = "VerRecibo" Then
            'Response.Redirect("https://sigarecibomatricula.udes.edu.co/ImpMatricula.aspx?EstudCod=" & Me.ViewState("EstudCod") & "&SedeCod=" & Me.Session("SedeCod") & "&Periodo=" & Me.ViewState("Periodo"))
            Dim Recibo As String = "https://sigarecibomatricula.udes.edu.co/ImpMatricula.aspx?EstudCod=" & Me.ViewState("EstudCod") & "&SedeCod=" & Me.Session("SedeCod") & "&Periodo=" & Me.ViewState("Periodo")
            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
            With strBuilder
                .Append("<script language='javascript'>")
                .Append("window.open('" & Recibo & "', '');")
                .Append("</script>")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
            End With
        End If
        If e.CommandName = "Adjuntos" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdVerAdjunto", "FuncionModaladdVerAdjunto();", True)

            Dim dt As DataTable = ObjUniver.CRUDSolicitudes(ViewState("SolicitudID"), "", 0, 0, 0, "", "", 0, "", "", "", "", "", "BuscarID")
            If dt.Rows.Count > 0 Then
                lblTitDoc.Text = "DOCUMENTOS ADJUNTOS"
                lblPeriodoVer.Text = dt.Rows(0).Item("Periodo")
                ddlTipoDescuentoVer.SelectedValue = dt.Rows(0).Item("TipoDescuentoID")
                lblSolicitudNroVer.Text = ViewState("SolicitudID")
                'BuscarSolicitudDocumentos()
                GVDocumentosVer.Visible = True
                GVNotificaiones.Visible = False
            End If
        End If
    End Sub
    Public Sub BuscarSolicitudDocumentos()
        Dim dt As DataTable = ObjUniver.CRUBSolicitudDocumentos(ddlTipoDescuentoVer.SelectedValue, ViewState("SolicitudID"), 0, "", "Buscar")
        If dt.Rows.Count > 0 Then
            If dt.Rows.Count > 0 Then
                ViewState("Documentos") = dt
                GVDocumentosVer.DataSource = dt
                GVDocumentosVer.DataBind()

            Else
                ViewState("Documentos") = Nothing
                GVDocumentosVer.DataSource = Nothing
                GVDocumentosVer.DataBind()
            End If
        End If
    End Sub
    Public Sub Periodos()
        Dim dt As DataTable = ObjUniver.LlenarDropDownLists("", "Periodo")

        If dt.Rows.Count > 0 Then
            With Me.ddlPeriodo
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "Periodo"
                .DataTextField = "Periodo"
                .DataBind()
                .Items.Add(New ListItem("Seleccione periodo", "-1"))
            End With

            ddlPeriodo.SelectedValue = -1

        End If
    End Sub
    Public Sub TipoDescuentos()
        Dim dt As DataTable = ObjUniver.LlenarDropDownLists("", "TipoDescuento")

        If dt.Rows.Count > 0 Then
            With Me.ddlTipoDescuentobuscar
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuentobuscar.SelectedValue = -1

        End If
    End Sub
    Public Sub Estados()
        Dim dt As DataTable = ObjUniver.CRUBEstados(0, "", 1, "Buscar")

        If dt.Rows.Count > 0 Then
            With Me.ddlEstado
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "EstadoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione estado", "-1"))
            End With

            ddlEstado.SelectedValue = -1

        End If
    End Sub
    Public Sub Dependencias()
        Dim dt As DataTable = ObjUniver.CRUBDependencias(0, Me.Session("SedeCod"), "", "", "", "", 0, 1, "Buscar")

        If dt.Rows.Count > 0 Then
            With Me.ddlDependencia
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "DependenciaID"
                .DataTextField = "NombreDepen"
                .DataBind()
                .Items.Add(New ListItem("Seleccione dependencia", "-1"))
            End With

            ddlDependencia.SelectedValue = -1

        End If
    End Sub
    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        BuscarSolicitudSeguimiento()
    End Sub
    Protected Sub ddlDependencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDependencia.SelectedIndexChanged
        'ModaladdAsignarDep
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)
        BuscarSolicitudDependencia()
    End Sub
    Protected Sub lblAsignarDep_Click(sender As Object, e As EventArgs) Handles lblAsignarDep.Click
        If Me.Session("Administrador") = True Then
            AsignarDependencia()
        Else
            EmitirConcepto()
        End If
    End Sub
    Public Sub EmitirConcepto()
        If Trim(txtObservacionCyC.Text) = "" Then
            ObjUniver.SendNotify("Digitar observación", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)
            Exit Sub
        End If
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)
        Dim dt As DataTable = ObjUniver.CRUBSolicitudDependencia(0, ViewState("SolicitudID"), ddlDependencia.SelectedValue, "", Trim(txtObservacionCyC.Text), "", "EmitirConcepto")
        If dt.Rows.Count > 0 Then

            Dim Asunto As String = "Concepto dependencia solicitud descuentos"
            Dim Mensaje As String = Trim(txtObservacionCyC.Text)

            Dim dt1 As DataTable = ObjUniver.CRUBDependencias(0, Me.Session("SedeCod"), 0, "", "", "", 1, 0, "BuscarAdmin")
            For i = 0 To dt1.Rows.Count - 1
                If Notificaciones.EnvioCorreos(dt1.Rows(i).Item("Correo"), dt1.Rows(i).Item("NombrePersona"), Asunto, Mensaje, Me.Server) Then
                End If
            Next

            ObjUniver.SendNotify("Se guardo la observación", "1")
        Else
            ObjUniver.SendNotify("No se guardo la observación", "3")
        End If
        BuscarSolicitudDependencia()
    End Sub
    Public Sub AsignarDependencia()
        If ddlDependencia.SelectedValue = -1 Or Trim(txtObservacionCyC.Text) = "" Then
            ObjUniver.SendNotify("Seleccionar dependencia y digitar observación", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniver.CRUBSolicitudDependencia(0, ViewState("SolicitudID"), ddlDependencia.SelectedValue, Trim(txtObservacionCyC.Text), "", "", "Agregar")
        If dt.Rows.Count > 0 Then
            If dt.Rows(0).Item(0) > 0 Then


                Dim dt1 As DataTable = ObjUniver.CRUBDependencias(ddlDependencia.SelectedValue, Me.Session("SedeCod"), "", "", "", "", 0, 0, "BuscarID")
                If dt1.Rows.Count > 0 Then
                    Dim Asunto As String = "Asignar solicitud - App Descuentos"

                    Dim Mensaje As String = "Se le asigno la solicitud nro. " & ViewState("SolicitudID") & " con el siguiente mensaje " & Trim(txtObservacionCyC.Text)

                    If Notificaciones.EnvioCorreos(dt1.Rows(0).Item("Correo"), dt1.Rows(0).Item("NombrePersona"), Asunto, Mensaje, Me.Server) Then

                    End If
                End If

                ObjUniver.SendNotify("Se asigno la dependencia " & ddlDependencia.SelectedItem.Text & " a la solicitud Nro " & ViewState("SolicitudID"), "1")

                BuscarSolicitudDependencia()
            Else
                ObjUniver.SendNotify("Ya esta asignada la dependencia a la solicitud", "3")
            End If
        Else
            ObjUniver.SendNotify("No se asigno la solicitud a la dependencia", "3")
        End If
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)
    End Sub
    Public Sub BuscarDatosPersonales(SolicitudID As Integer)
        Dim dt As DataTable = ObjUniver.CRUDSolicitudes(SolicitudID, "", 0, 0, 0, "", "", 0, "", "", "", "", "", "BuscarAsigDep")
        If dt.Rows.Count > 0 Then
            lblDepPeriodo.Text = dt.Rows(0).Item("Periodo")
            lblDepFechaSolicitud.Text = dt.Rows(0).Item("FechaRegistro")
            lblDepSede.Text = UCase(dt.Rows(0).Item("Sede"))
            lblDepPrograma.Text = dt.Rows(0).Item("Programa")
            lblDepCodigo.Text = dt.Rows(0).Item("Codigo")
            lblDepEstudiante.Text = dt.Rows(0).Item("Estudiante")
            lblDepTipoDescuento.Text = dt.Rows(0).Item("TipoDescuento")
            lblSolicitudNro.Text = ViewState("SolicitudID")

            Me.ViewState("EstudCod") = dt.Rows(0).Item("Codigo")
            Me.ViewState("Periodo") = dt.Rows(0).Item("Periodo")

        End If
    End Sub
    Protected Sub GVAsignarDep_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVAsignarDep.RowDeleting
        Dim ID As Integer = Me.GVAsignarDep.DataKeys(e.RowIndex).Value
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModaladdAsignarDep", "FuncionModaladdAsignarDep();", True)
        ' Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(ID, "", "", 0, SwActivo, "DesActivar")
        Dim dt As DataTable = ObjUniver.CRUBSolicitudDependencia(ID, 0, 0, "", "", "", "Eliminar")

        If dt.Rows.Count > 0 Then
            ObjUniver.SendNotify(" No se elimino el registro, ya tiene observación de la dependencia ", "1")
        Else
            ObjUniver.SendNotify(" Registro eliminado con exito  ", "1")
        End If

        BuscarSolicitudDependencia()

    End Sub

End Class