﻿Public Class TipoContrato
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Buscar()
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBTipoContrato(0, "", 0, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("TipoContrato") = dt
            GVTipoContrato.DataSource = dt
            GVTipoContrato.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("TipoContrato") = Nothing
            GVTipoContrato.DataSource = Nothing
            GVTipoContrato.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbNewTipoContrato_Click(sender As Object, e As EventArgs) Handles lbNewTipoContrato.Click
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddTipoContrato", "FuncionModalAddTipoContrato();", True)
    End Sub

    Protected Sub GVTipoContrato_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVTipoContrato.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)

        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
            End If

            Estado.Text = ""

        Catch ex As Exception

        End Try
    End Sub
End Class