﻿Public Class Estados
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Buscar()
            Dim a As String = ""
            Dim b As String = ""
            'lbAddEstados.Visible = False
            'lbNewEstados.Visible = False
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBEstados(0, Trim(txtNombre.Text), ddlActivos.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Estados") = dt
            GVEstados.DataSource = dt
            GVEstados.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Estados") = Nothing
            GVEstados.DataSource = Nothing
            GVEstados.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbNewEstados_Click(sender As Object, e As EventArgs) Handles lbNewEstados.Click
        Limpiar()
        lblEvento.Text = "Crear estados"
        lbAddEstados.Text = "Agregar"
        ViewState("Accion") = "Crear" 'Crear
        ViewState("EstadoID") = 0
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddEstados", "FuncionModalAddEstados();", True)
    End Sub

    Protected Sub GVEstados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVEstados.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim btnEstado As System.Web.UI.WebControls.LinkButton = DirectCast(e.Row.FindControl("lbEliminar"), System.Web.UI.WebControls.LinkButton)

        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-danger fa fa-times"
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
                btnEstado.ForeColor = Drawing.Color.White
                btnEstado.CssClass = "btn btn-success fa fa-check"
            End If

            Estado.Text = ""

        Catch ex As Exception

        End Try
    End Sub
    Public Sub Crear()
        If Trim(txtNombreAdd.Text) = "" Then
            ObjUniver.SendNotify("Nombre es obligatorio", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddEstados", "FuncionModalAddEstados();", True)
            Exit Sub
        End If

        Dim Mensaje As String = ""
        Dim dtValidar As DataTable = ObjUniver.ValidarNombre("", txtNombreAdd.Text, 0, "Estados")
        Dim dtValidarMod As DataTable = ObjUniver.ValidarNombre("", txtNombreAdd.Text, ViewState("EstadoID"), "EstadosMod")

        If ViewState("Accion") = "Crear" Then
            If dtValidar.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddEstados", "FuncionModalAddEstados();", True)
                Exit Sub
            Else
                Mensaje = "Se creó el registro con éxito"
            End If
        Else
            If dtValidarMod.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe ese nombre", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddEstados", "FuncionModalAddEstados();", True)
                Exit Sub
            Else
                Mensaje = "Se modificó el registro con éxito"
            End If

        End If

        Dim dt As DataTable = ObjUniver.CRUBEstados(ViewState("EstadoID"), Trim(txtNombreAdd.Text), 0, ViewState("Accion"))
        If dt.Rows.Count > 0 Then
            Limpiar()
            ObjUniver.SendNotify(Mensaje, "1")
            Buscar()
        End If
    End Sub
    Public Sub Limpiar()
        txtNombreAdd.Text = ""
    End Sub
    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub
    Protected Sub lbAddEstados_Click(sender As Object, e As EventArgs) Handles lbAddEstados.Click
        Crear()
    End Sub

    Protected Sub GVEstados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVEstados.RowCommand
        If e.CommandName = "Editar" Then
            Limpiar()
            lblEvento.Text = "Editar estados"
            lbAddEstados.Text = "Actualizar"
            ViewState("Accion") = "Modificar" 'Modificar
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddEstados", "FuncionModalAddEstados();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("EstadoID") = Me.GVEstados.DataKeys(index).Values(0)


            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")

            Dim dt As DataTable = ObjUniver.CRUBEstados(ViewState("EstadoID"), "", 0, "BuscarID")
            If dt.Rows.Count > 0 Then
                txtNombreAdd.Text = dt.Rows(0).Item("Nombre")

                Dim Obligatorio As Integer = 0

              

            End If
        End If
    End Sub

    Protected Sub GVEstados_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVEstados.RowDeleting
        Dim ID As Integer = Me.GVEstados.DataKeys(e.RowIndex).Value
        Dim dt1 As DataTable = ObjUniver.CRUBEstados(ID, "", 0, "BuscarID")

        Dim SwActivo As Boolean = dt1.Rows(0).Item("SwActivo")
        Dim Mensaje As String = ""

        If SwActivo = True Then
            SwActivo = False
            Mensaje = "Desactivado"
        Else
            SwActivo = True
            Mensaje = "Activado"
        End If

        Dim dt As DataTable = ObjUniver.CRUBEstados(ID, "", SwActivo, "DesActivar")

        ObjUniver.SendNotify(" Registro  " & Mensaje, "1")
        Buscar()
    End Sub

    Protected Sub GVEstados_PageIndexChanged(sender As Object, e As EventArgs) Handles GVEstados.PageIndexChanged
        Buscar()
    End Sub

    Protected Sub GVEstados_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVEstados.PageIndexChanging
        Me.GVEstados.PageIndex = e.NewPageIndex()
        Me.GVEstados.DataBind()
    End Sub

End Class