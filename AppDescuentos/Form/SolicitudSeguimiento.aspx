﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Principal.Master" CodeBehind="SolicitudSeguimiento.aspx.vb" Inherits="AppDescuentos.SolicitudSeguimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        function FuncionModaladdObservDep() {
            $("#ModaladdObservDep").modal("show")
        }
        function FuncionModaladdAsignarDep() {
            $("#ModaladdAsignarDep").modal("show")
        }
        function FuncionModalaAplicarDescuento() {
            $("#ModalaAplicarDescuento").modal("show")
        }
        function FuncionModalCambiarEstado() {
            $("#ModalCambiarEstado").modal("show")
        }
        function FuncionModaladdVerAdjunto() {
            $("#ModaladdVerAdjunto").modal("show")
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="main_title" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Header" runat="server">
</asp:Content>--%>
<asp:Content ID="Content5" ContentPlaceHolderID="Main_Texto" runat="server">
</asp:Content>
<%--<asp:Content ID="Content6" ContentPlaceHolderID="navbar" runat="server">
</asp:Content>--%>
<asp:Content ID="Content7" ContentPlaceHolderID="Main_pagination" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="Modulos.aspx">Inicio</a></li>
        <li class="breadcrumb-item active">Solicitud seguimiento</li>
    </ol>
</asp:Content>
<%--<asp:Content ID="Content8" ContentPlaceHolderID="main_content" runat="server">
</asp:Content>--%>
<asp:Content ID="Content9" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Panel ID="pnSolicitud" runat="server" Visible="true">
        <div class="panel panel-default" id="xxx" runat="server">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-8">
                        Solicitud seguimiento
                    </div>
                    <div class="col-lg-4" style="text-align: right">
                        <%--<asp:LinkButton ID="lbNewTipoContrato" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-plus"></span>&nbsp;Nuevo 
                        </asp:LinkButton>--%>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Cuadro de Consulta</strong>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                        <strong>Periodo:</strong>
                        <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                            <asp:ListItem Value="A2019" Selected="True">A2019</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-lg-4">
                        <strong>Tipo descuento:</strong>
                        <asp:DropDownList ID="ddlTipoDescuentobuscar" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                            <asp:ListItem Value="1" Selected="True">RENDIMIENTO ACADEMICO</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-lg-4">
                        <strong>Estado:</strong>
                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                            <asp:ListItem Value="1" Selected="True">NUEVO</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="col-lg-2" style="text-align: left">

                        <br>
                        <asp:LinkButton ID="lbBuscar" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Buscar 
                        </asp:LinkButton>
                        <br>
                    </div>
                </div>

                <br />
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Listado</strong>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <asp:GridView ID="GVSolicitudes" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                            DataKeyNames="SolicitudID">
                            <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                            <PagerStyle HorizontalAlign="Center" />
                            <EditRowStyle BackColor="#ffffcc" />
                            <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                            <Columns>
                                <asp:BoundField DataField="SolicitudID" HeaderText="Cod. Interno">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha" DataFormatString="{0:d}">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Periodo" HeaderText="Periodo">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NombreEstud" HeaderText="Estudiante">
                                    <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoDescuento" HeaderText="Tipo Descuento">
                                    <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Estado" HeaderText="Estado">
                                    <ItemStyle Width="15%" />
                                </asp:BoundField>


                                <asp:TemplateField HeaderText="Acciones">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbDependencias" runat="server" CausesValidation="False" CommandName="Dependencias" CssClass="btn btn-primary" Text=""
                                            data-toggle="tooltip" title="Sequimiento Dependencias" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-building"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="lbDescuentos" runat="server" CausesValidation="False" CommandName="Descuentos" CssClass="btn btn-success" Text=""
                                            data-toggle="tooltip" title="Aplicar Descuentos" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-usd"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="lbVerRecibo" runat="server" CausesValidation="False" CommandName="VerRecibo" CssClass="btn btn-info" Text=""
                                            data-toggle="tooltip" title="Ver Recibo" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-file-archive-o"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                          <asp:LinkButton ID="lbAdjuntos" runat="server" CausesValidation="False" CommandName="Adjuntos" CssClass="btn btn-warning" Text=""
                                              data-toggle="tooltip" title="Documentos Adjuntos" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-file-pdf-o"></span>                                        
                                          </asp:LinkButton>
                                        <%-- ============ FIN Alerta de confirmación eliminación de registro ============ --%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="alert alert-info" role="alert" id="Alerta1" runat="server">¡Aviso! No hay resultados para mostrar....</div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>



    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-danger" id="pnMensajeSinAcceso" runat="server" visible="false">
        <strong>
            <asp:Label ID="lMensaje1" runat="server" Text="Sin acceso"></asp:Label></strong><br />
        <strong>
            <asp:Label ID="lMensaje2" runat="server" Text="Atención!"></asp:Label></strong>
        <asp:Label ID="lMensajeDanger" runat="server" Text="No tienes acceso a este módulo consulta con el administrador"></asp:Label>
    </div>

    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-warning" id="pnAlert" runat="server" visible="false" style="text-align: left">
        <div class="form-group row">
            <div class="col-lg-1" style="text-align: right">
                <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
            </div>
            <div class="col-lg-11">
                <strong>
                    <asp:Label ID="Label2" runat="server" Text="Label:"></asp:Label></strong><br />
                <strong>
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></strong>
                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>


    <!-- Modal Para asignar dependencias a una solicitud-->
    <div id="ModaladdAsignarDep" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <asp:Label ID="lblTituloModal" runat="server" Text="Asignar dependencia"></asp:Label></strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DATOS SOLICITANTE</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Periodo:</strong>
                            <asp:Label ID="lblDepPeriodo" runat="server" Text="A2019"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Fecha Solicitud:</strong>
                            <asp:Label ID="lblDepFechaSolicitud" runat="server" Text="28/03/2019"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Sede:</strong>
                            <asp:Label ID="lblDepSede" runat="server" Text="BUCARAMANGA"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Programa:</strong>
                            <asp:Label ID="lblDepPrograma" runat="server" Text="INGENIERIA INDUSTRIAL"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Codigo:</strong>
                            <asp:Label ID="lblDepCodigo" runat="server" Text="17151999"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Estudiante:</strong>
                            <asp:Label ID="lblDepEstudiante" runat="server" Text="CARLOS NOVA"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Tipo descuento:</strong>
                            <asp:Label ID="lblDepTipoDescuento" runat="server" Text="EGRESADO"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Solicitud Nro:</strong>
                            <asp:Label ID="lblSolicitudNro" runat="server" Text="101"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>
                                <asp:Label ID="lblTituloDep" runat="server" Text="ASIGNAR DEPENDENCIA"></asp:Label></strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-10">
                            <strong>
                                <asp:Label ID="lblDependencia" runat="server" Text="Dependencia:"></asp:Label></strong>
                            <asp:DropDownList ID="ddlDependencia" runat="server" CssClass="form-control" Style="text-transform: uppercase" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-10">
                            <strong>
                                <asp:Label ID="lblObservDep" runat="server" Text="Observación C&C"></asp:Label></strong>
                            <asp:TextBox ID="txtObservacionCyC" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-2" style="text-align: left">

                            <br>
                            <asp:LinkButton ID="lblAsignarDep" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Asignar 
                            </asp:LinkButton>
                            <br>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-lg-12">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <asp:GridView ID="GVAsignarDep" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="SolicitudDependenciaID">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="SolicitudDependenciaID" HeaderText="Cod. Interno" Visible="false">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Dependencia" HeaderText="Dependencia">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ObservacionCyC" HeaderText="Observacion C&C">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Obs C&C" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ObservacionDep" HeaderText="Observacion Dep">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaObservDep" HeaderText="Fecha Obs Dep" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Acciones">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEliminarDep" runat="server" CommandName="Delete" CssClass="btn btn-danger" Text="Error"
                                                data-toggle="tooltip" title="Eliminar" OnClientClick="return deletealert2(this, event);">
                                                <span class="fa fa-trash-o fa-lg"></span>
                                            </asp:LinkButton>
                                            <%-- ============ Alerta de confirmación eliminación de registro ============ --%>
                                            <script>
                                                function deletealert2(ctl, event) {
                                                    // STORE HREF ATTRIBUTE OF LINK CTL (THIS) BUTTON
                                                    var defaultAction = $(ctl).prop("href");
                                                    // CANCEL DEFAULT LINK BEHAVIOUR
                                                    event.preventDefault();
                                                    swal({
                                                        title: "¿Está Seguro de eliminar la asignación?",
                                                        text: "",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Si, ¡Eliminar asignación!",
                                                        cancelButtonText: "No, cancelar",
                                                        closeOnConfirm: false,
                                                        //closeOnCancel: false
                                                    },
                                                        function (isConfirm) {
                                                            if (isConfirm) {
                                                                //swal({ title: "Eliminado!", text: "Tu archivo imaginario ha sido eliminado.", type: "success", confirmButtonText: "OK!", closeOnConfirm: false },
                                                                //function () {
                                                                //    // RESUME THE DEFAULT LINK ACTION
                                                                window.location.href = defaultAction;
                                                                return true;
                                                                //});                                                            
                                                            } else {
                                                                //swal("Cancelado", "Tu archivo imaginario es seguro :)", "error");
                                                                return false;
                                                            }
                                                        });
                                                }
                                            </script>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Para agregar HojaVida-->
    <div id="ModalaAplicarDescuento" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Aplicar Descuento</strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DATOS SOLICITANTE</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Periodo:</strong>
                            <asp:Label ID="Label18" runat="server" Text="A2019"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Fecha Solicitud:</strong>
                            <asp:Label ID="Label19" runat="server" Text="28/03/2019"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Sede:</strong>
                            <asp:Label ID="Label20" runat="server" Text="BUCARAMANGA"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Programa:</strong>
                            <asp:Label ID="Label21" runat="server" Text="INGENIERIA INDUSTRIAL"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Codigo:</strong>
                            <asp:Label ID="Label22" runat="server" Text="17151999"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Estudiante:</strong>
                            <asp:Label ID="Label23" runat="server" Text="CARLOS NOVA"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <strong>Tipo descuento:</strong>
                            <asp:Label ID="Label24" runat="server" Text="EGRESADO"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>APLICAR DESCUENTO</strong>
                        </div>
                    </div>
                    <div class="form-group row">

                        <div class="col-lg-2">
                            <strong>Porcentaje</strong>
                            <asp:TextBox ID="txtPorcentaje" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <strong>Valor Descuento</strong>
                            <asp:TextBox ID="txtValorDescuento" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-2" style="text-align: left">

                            <br>
                            <asp:LinkButton ID="lbCalcular" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Calcular 
                            </asp:LinkButton>
                            <br>
                        </div>
                        <div class="col-lg-2" style="text-align: left">

                            <br>
                            <asp:LinkButton ID="lbAplicarDesc" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Aplicar 
                            </asp:LinkButton>
                            <br>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <asp:GridView ID="GVAplicarDesc" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="SolicitudDependenciaID">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="SolicitudDependenciaID" HeaderText="Cod. Interno" Visible="false">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Dependencia" HeaderText="Dependencia">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ObservacionCyC" HeaderText="Observacion C&C">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Obs C&C" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ObservacionDep" HeaderText="Observacion Dep">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaObservDep" HeaderText="Fecha Obs Dep" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Acciones">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEliminarDep" runat="server" CommandName="Delete" CssClass="btn btn-danger" Text="Error"
                                                data-toggle="tooltip" title="Activar/Desactivar" OnClientClick="return deletealert2(this, event);">
                                                <span class="fa fa-trash-o fa-lg"></span>
                                            </asp:LinkButton>
                                            <%-- ============ Alerta de confirmación eliminación de registro ============ --%>
                                            <script>
                                                function deletealert2(ctl, event) {
                                                    // STORE HREF ATTRIBUTE OF LINK CTL (THIS) BUTTON
                                                    var defaultAction = $(ctl).prop("href");
                                                    // CANCEL DEFAULT LINK BEHAVIOUR
                                                    event.preventDefault();
                                                    swal({
                                                        title: "¿Está Seguro de eliminar la asignación?",
                                                        text: "",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Si, ¡Eliminar asignación!",
                                                        cancelButtonText: "No, cancelar",
                                                        closeOnConfirm: false,
                                                        //closeOnCancel: false
                                                    },
                                                        function (isConfirm) {
                                                            if (isConfirm) {
                                                                //swal({ title: "Eliminado!", text: "Tu archivo imaginario ha sido eliminado.", type: "success", confirmButtonText: "OK!", closeOnConfirm: false },
                                                                //function () {
                                                                //    // RESUME THE DEFAULT LINK ACTION
                                                                window.location.href = defaultAction;
                                                                return true;
                                                                //});                                                            
                                                            } else {
                                                                //swal("Cancelado", "Tu archivo imaginario es seguro :)", "error");
                                                                return false;
                                                            }
                                                        });
                                                }
                                            </script>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Cambiar Estado-->
    <div id="ModalCambiarEstado" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Cambiar Estado</strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DATOS SOLICITANTE</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Periodo:</strong>
                            <asp:Label ID="Label25" runat="server" Text="A2019"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Fecha Solicitud:</strong>
                            <asp:Label ID="Label26" runat="server" Text="28/03/2019"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Sede:</strong>
                            <asp:Label ID="Label27" runat="server" Text="BUCARAMANGA"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Programa:</strong>
                            <asp:Label ID="Label28" runat="server" Text="INGENIERIA INDUSTRIAL"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Codigo:</strong>
                            <asp:Label ID="Label29" runat="server" Text="17151999"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Estudiante:</strong>
                            <asp:Label ID="Label30" runat="server" Text="CARLOS NOVA"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <strong>Tipo descuento:</strong>
                            <asp:Label ID="Label31" runat="server" Text="EGRESADO"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>CAMBIAR ESTADO</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Estado:</strong>
                            <asp:DropDownList ID="ddlEstadoCambiar" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                <asp:ListItem Value="A2019" Selected="True">EN REVISION</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-6">
                            <strong>Enviar notificación:</strong>
                            <asp:DropDownList ID="ddlEnviarNotificacion" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                <asp:ListItem Value="SI" Selected="True">SI</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-10">
                            <strong>Observación</strong>
                            <asp:TextBox ID="txtObservacionEstado" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-2" style="text-align: left">

                            <br>
                            <asp:LinkButton ID="lbCambiarEstado" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Cambiar 
                            </asp:LinkButton>
                            <br>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="SolicitudDependenciaID">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="SolicitudDependenciaID" HeaderText="Cod. Interno" Visible="false">
                                        <ItemStyle Width="5%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Dependencia" HeaderText="Dependencia">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ObservacionCyC" HeaderText="Observacion C&C">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha Obs C&C" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ObservacionDep" HeaderText="Observacion Dep">
                                        <ItemStyle Width="25%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaObservDep" HeaderText="Fecha Obs Dep" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Acciones">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbEliminarDep" runat="server" CommandName="Delete" CssClass="btn btn-danger" Text="Error"
                                                data-toggle="tooltip" title="Activar/Desactivar" OnClientClick="return deletealert2(this, event);">
                                                <span class="fa fa-trash-o fa-lg"></span>
                                            </asp:LinkButton>
                                            <%-- ============ Alerta de confirmación eliminación de registro ============ --%>
                                            <script>
                                                function deletealert2(ctl, event) {
                                                    // STORE HREF ATTRIBUTE OF LINK CTL (THIS) BUTTON
                                                    var defaultAction = $(ctl).prop("href");
                                                    // CANCEL DEFAULT LINK BEHAVIOUR
                                                    event.preventDefault();
                                                    swal({
                                                        title: "¿Está Seguro de eliminar la asignación?",
                                                        text: "",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Si, ¡Eliminar asignación!",
                                                        cancelButtonText: "No, cancelar",
                                                        closeOnConfirm: false,
                                                        //closeOnCancel: false
                                                    },
                                                        function (isConfirm) {
                                                            if (isConfirm) {
                                                                //swal({ title: "Eliminado!", text: "Tu archivo imaginario ha sido eliminado.", type: "success", confirmButtonText: "OK!", closeOnConfirm: false },
                                                                //function () {
                                                                //    // RESUME THE DEFAULT LINK ACTION
                                                                window.location.href = defaultAction;
                                                                return true;
                                                                //});                                                            
                                                            } else {
                                                                //swal("Cancelado", "Tu archivo imaginario es seguro :)", "error");
                                                                return false;
                                                            }
                                                        });
                                                }
                                            </script>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Para ver documentos adjuntos de una Solicitud-->
    <div id="ModaladdVerAdjunto" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <asp:Label ID="Label1" runat="server" Text="Ver documentos adjuntos de una solicitud"></asp:Label></strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DATOS SOLICITANTE</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Periodo:</strong>
                            <asp:Label ID="lblPeriodoVer" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Fecha Solicitud:</strong>
                            <asp:Label ID="lblFechaSolicitudVer" runat="server" Text="28/03/2019"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Programa:</strong>
                            <asp:Label ID="lblProgramaVer" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Solicitud Nro:</strong>
                            <asp:Label ID="lblSolicitudNroVer" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Codigo:</strong>
                            <asp:Label ID="lblCodigoVer" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Estudiante:</strong>
                            <asp:Label ID="lblEstudianteVer" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DESCUENTO</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <strong>Tipo descuento</strong>
                            <asp:DropDownList ID="ddlTipoDescuentoVer" runat="server" CssClass="form-control" Style="text-transform: uppercase" AutoPostBack="false" Enabled="false">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong><%--DOCUMENTOS ADJUNTOS--%>
                                <asp:Label ID="lblTitDoc" runat="server" Text="DOCUMENTOS ADJUNTOS"></asp:Label>
                            </strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <asp:GridView ID="GVDocumentosVer" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="DocumentosDescuentoID, Obligatorio">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="DocumentosDescuentoID" HeaderText="Cod. Interno">
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Nombre" HeaderText="Documento">
                                        <ItemStyle Width="35%" />
                                    </asp:BoundField>


                                    <asp:TemplateField HeaderText="Obligatorio">
                                        <ItemTemplate>
                                            <asp:Label ID="lEstado" runat="server" Text='<%# Bind("Obligatorio")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ver">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbAVerjuntarVer" runat="server" CausesValidation="False" CommandName="VerAdjuntarVer" CssClass="btn btn-danger fa fa-file-pdf-o" Text=""
                                                data-toggle="tooltip" title="Ver pdf" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <%--<span class="fa fa-pencil-square-o"></span>   --%>                                     
                                            </asp:LinkButton>
                                            &nbsp;
                                         
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                            <asp:GridView ID="GVNotificaiones" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="NotificacionID">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="NotificacionID" HeaderText="Cod. Interno" Visible="false">
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Observacion" HeaderText="Observación">
                                        <ItemStyle Width="85%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>



                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="lbExportar" runat="server" CssClass="btn btn-success fa fa-file-pdf-o" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Exportar </asp:LinkButton>
                        <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
