﻿Public Class Modulos
    Inherits System.Web.UI.Page
    'Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'OcultarMenu()
            Permisos()
        End If
    End Sub
    Private Sub Permisos()
        If Me.Session("Dependencia") = True Then
            If Me.Session("Administrador") = False Then
                OcultarMenu()
                btn_Solicitudes.Visible = True
            End If
        ElseIf Me.Session("Estudiante") = True Then
            OcultarMenu()
            btn_SolicitudDesc.Visible = True
        Else
            Response.Redirect("../Inicio/Login.aspx")
        End If


    End Sub
    Public Sub OcultarMenu()
        btn_Dependencias.Visible = False
        btn_DocumentosReq.Visible = False
        btn_Estados.Visible = False
        btn_Informes.Visible = False
        btn_SolicitudDesc.Visible = False
        btn_Solicitudes.Visible = False
        'btn_TipoContrato.Visible = False
        btn_TipoDescuentos.Visible = False
        btn_DepTipoDescuentos.Visible = False
        btn_DocTipoDescuentos.Visible = False
    End Sub
End Class