﻿Public Class HojaVida
    Inherits System.Web.UI.Page
    Dim ObjUniversal As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Consulta()
        End If
    End Sub
    Public Sub Consulta()
        Dim Edad As Integer = 0

        If Trim(txtEdad.Text) <> "" Then
            Edad = Trim(txtEdad.Text)
        End If

        Dim dt As DataTable = ObjUniversal.CRUBHojaVida(0, Trim(txtNombre.Text), "", Edad, Trim(txtAcudiente.Text), "", "", ddlEstadoConsul.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            Me.Session("dtDatos") = dt
            GVHojaVida.DataSource = dt
            GVHojaVida.DataBind()
            Alerta1.Visible = False
        Else
            Me.Session("dtDatos") = Nothing
            GVHojaVida.DataSource = Nothing
            GVHojaVida.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Consulta()
    End Sub

    Protected Sub lbAddHojaVida_Click(sender As Object, e As EventArgs) Handles lbAddHojaVida.Click
        LimpiarCrear()
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddHojaVida", "FuncionModalAddHojaVida();", True)
    End Sub
    Protected Sub lbAddHojaVid_Click(sender As Object, e As EventArgs) Handles lbAddHojaVid.Click
        If Len(Trim(txtNombreAdd.Text)) = 0 Or Len(Trim(txtEdadAdd.Text)) = 0 Or Len(Trim(txtAcudienteAdd.Text)) = 0 Then
            ObjUniversal.SendNotify(" Por favor verificar campos en rojo, son requeridos ", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddHojaVida", "FuncionModalAddHojaVida();", True)
            Exit Sub
        End If
        Dim Fecha1 As String
        Fecha1 = Mid(dtpFechaNacimiento.Value, 7, 4) + "-" + Mid(dtpFechaNacimiento.Value, 4, 2) + "-" + Mid(dtpFechaNacimiento.Value, 1, 2)

        Dim dt As DataTable = ObjUniversal.CRUBHojaVida(0, txtNombreAdd.Text.ToUpper, Fecha1, txtEdadAdd.Text, txtAcudienteAdd.Text.ToUpper, txtTelefonoAdd.Text, txtDireccionAdd.Text.ToUpper, 1, "Crear")

        If dt.Rows.Count > 0 Then
            Me.Session("HojaVidaId") = dt.Rows(0).Item(0)
            ObjUniversal.SendNotify(" Registro creado  ", "1")
            Consulta()
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddHojaVida", "FuncionModalAddHojaVida();", True)
        Else
            ObjUniversal.SendNotify("No fue creado el registro", "3")
        End If



    End Sub
    Public Sub LimpiarCrear()
        txtNombreAdd.Text = ""
        txtEdadAdd.Text = ""
        dtpFechaNacimiento.Value = Nothing
        txtEdadAdd.Text = ""
        txtAcudienteAdd.Text = ""
        txtTelefonoAdd.Text = ""
        txtDireccionAdd.Text = ""

    End Sub

    Protected Sub GVHojaVida_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVHojaVida.RowCommand
        If e.CommandName = "EditarHojaVida" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModalUpdHojaVida", "FuncionModalUpdHojaVida();", True)
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            ViewState("HojaVidaID") = Me.GVHojaVida.DataKeys(index).Values(0)

            Limpiarmodificar()
            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")
            Dim dt As DataTable = ObjUniversal.CRUBHojaVida(ViewState("HojaVidaID"), "", "", 0, "", "", "", 1, "BuscarId")
            If dt.Rows.Count > 0 Then
                txtNombreUpd.Text = dt.Rows(0).Item("Nombre")
                txtEdadUpd.Text = dt.Rows(0).Item("Edad")
                Try
                    dpFechaNacimientoUpd.Value = dt.Rows(0).Item("FechaNacimiento")
                Catch ex As Exception
                    dpFechaNacimientoUpd.Value = Nothing
                End Try

                txtAcudienteUpd.Text = dt.Rows(0).Item("Acudiente")
                txtTelefonoUpd.Text = dt.Rows(0).Item("Telefonos")
                txtDireccionUpd.Text = dt.Rows(0).Item("Direccion")
            End If
        End If
        If e.CommandName = "TomarFoto" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Me.Session("HojaVidaId") = Me.GVHojaVida.DataKeys(index).Values(0)
            Response.Redirect("../Form/TomarFoto.aspx")
        End If
    End Sub
    Public Sub Limpiarmodificar()
        txtNombreUpd.Text = ""
        txtEdadUpd.Text = ""
        dpFechaNacimientoUpd.Value = Nothing
        txtEdadUpd.Text = ""
        txtAcudienteUpd.Text = ""
        txtTelefonoUpd.Text = ""
        txtDireccionUpd.Text = ""

    End Sub
    Protected Sub llbUpdHojaVida_Click(sender As Object, e As EventArgs) Handles lbUpdHojaVida.Click
        If Len(Trim(txtNombreUpd.Text)) = 0 Or Len(Trim(txtEdadUpd.Text)) = 0 Or Len(Trim(txtAcudienteUpd.Text)) = 0 Then
            ObjUniversal.SendNotify(" Por favor verificar campos en rojo, son requeridos ", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "ModalUpdHojaVida", "FuncionModalUpdHojaVida();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniversal.CRUBHojaVida(ViewState("HojaVidaID"), txtNombreUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")
        ObjUniversal.SendNotify(" Registro actualizado  ", "1")
        Consulta()
    End Sub

    Protected Sub GVHojaVida_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVHojaVida.RowDeleting
        Dim ID As Integer = Me.GVHojaVida.DataKeys(e.RowIndex).Value
        Dim dt1 As DataTable = ObjUniversal.CRUBHojaVida(ID, "", "", 0, "", "", "", 1, "BuscarId")

        Dim SwActivo As Boolean = dt1.Rows(0).Item("SwActivo")
        Dim Mensaje As String = ""

        If SwActivo = True Then
            SwActivo = False
            Mensaje = "Desactivado"
        Else
            SwActivo = True
            Mensaje = "Activado"
        End If

        Dim dt As DataTable = ObjUniversal.CRUBHojaVida(ID, "", "", 0, "", "", "", SwActivo, "DesactivarActivar")

        ObjUniversal.SendNotify(" Registro  " & Mensaje, "1")
        Consulta()
    End Sub

    Protected Sub GVHojaVida_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVHojaVida.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
            End If

            Estado.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GVHojaVida_PageIndexChanged(sender As Object, e As EventArgs) Handles GVHojaVida.PageIndexChanged
        Consulta()
    End Sub

    Protected Sub GVHojaVida_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GVHojaVida.PageIndexChanging
        Me.GVHojaVida.PageIndex = e.NewPageIndex()
        Me.GVHojaVida.DataBind()
    End Sub
    Protected Sub lbTomarFoto_Click(sender As Object, e As EventArgs) Handles lbTomarFoto.Click
        Response.Redirect("../Form/TomarFoto.aspx")
    End Sub
    Protected Sub lbDescargar_Click(sender As Object, e As EventArgs) Handles lbDescargar.Click
        Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
        With strBuilder
            .Append("<script language='javascript'>")
            .Append("window.open('" & "ExportInfo.aspx', '');")
            .Append("</script>")
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
        End With
    End Sub

    Protected Sub lbAsistencia_Click(sender As Object, e As EventArgs) Handles lbAsistencia.Click
        Response.Redirect("../Form/Asistencia.aspx")
    End Sub
End Class