﻿Imports System.IO
Imports ClosedXML.Excel
Public Class ExportInfo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cultura_anterior As System.Globalization.CultureInfo
        cultura_anterior = System.Globalization.CultureInfo.CurrentCulture
        'Se crea este formato para los valores numericos que tienen comas y punto como separadores
        Dim cultura_invariable As New System.Globalization.CultureInfo("es-CO")
        System.Threading.Thread.CurrentThread.CurrentCulture = cultura_invariable

        Dim nameFile As String = "Datos.xlsx"
        Dim FullPathName As String = System.AppDomain.CurrentDomain.BaseDirectory & "\Archivos\" & nameFile

        Dim dt As New DataTable
        dt = Me.Session("dtDatos")

        If dt.Rows.Count > 0 Then

            ConvertXLSX(dt, FullPathName)
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & nameFile)
            Response.Flush()
            Response.TransmitFile(FullPathName)
            Response.End()

            File.Delete(FullPathName)

            'Se restablece la cultura inicial
            System.Threading.Thread.CurrentThread.CurrentCulture = cultura_anterior

        Else

            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
            With strBuilder
                .Append("<script language='javascript'>")
                .Append("alert('No hay datos para exportar.');")
                .Append("</script>")
                ClientScript.RegisterStartupScript(Me.GetType(), "Ticket", strBuilder.ToString)
            End With
        End If
    End Sub
    Public Sub ConvertXLSX(ByVal dt As DataTable, ByVal FullPathName As String)
        Try
            Dim wb = New XLWorkbook()
            Dim dataTable = dt
            dataTable.TableName = "HojaVida"
            ' Add a DataTable as a worksheet
            wb.Worksheets.Add(dataTable)
            wb.SaveAs(FullPathName)
        Catch ex As Exception
        End Try
    End Sub

End Class