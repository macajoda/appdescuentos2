﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Principal.Master" CodeBehind="Solicitudes.aspx.vb" Inherits="AppDescuentos.Solicitudes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        function FuncionModalAddSolicitudes() {
            $("#ModaladdSolicitudes").modal("show")
        }
        function FuncionModalAddVerAdjunto() {
            $("#ModaladdVerAdjunto").modal("show")
        }
        function FuncionModalAddVerNotificaciones() {
            $("#ModaladdVerNotificaciones").modal("show")
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="main_title" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Header" runat="server">
</asp:Content>--%>
<asp:Content ID="Content5" ContentPlaceHolderID="Main_Texto" runat="server">
</asp:Content>
<%--<asp:Content ID="Content6" ContentPlaceHolderID="navbar" runat="server">
</asp:Content>--%>
<asp:Content ID="Content7" ContentPlaceHolderID="Main_pagination" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="Modulos.aspx">Inicio</a></li>
        <li class="breadcrumb-item active">Solicitud descuento</li>
    </ol>
</asp:Content>
<%--<asp:Content ID="Content8" ContentPlaceHolderID="main_content" runat="server">
</asp:Content>--%>
<asp:Content ID="Content9" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Panel ID="pnSolicitud" runat="server" Visible="true">
        <div class="panel panel-default" id="xxx" runat="server">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-8">
                        Solicitud Descuento
                    </div>
                    <div class="col-lg-4" style="text-align: right">
                        <asp:LinkButton ID="lbNewTipoContrato" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-plus"></span>&nbsp;Nuevo 
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Cuadro de Consulta</strong>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-4">
                        Periodo
                            <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                            </asp:DropDownList>
                    </div>

                    <div class="col-lg-2" style="text-align: left">

                        <br>
                        <asp:LinkButton ID="lbBuscar" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Buscar 
                        </asp:LinkButton>
                        <br>
                    </div>
                </div>

                <br />
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Listado</strong>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <asp:GridView ID="GVSolicitudes" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                            DataKeyNames="SolicitudID">
                            <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                            <PagerStyle HorizontalAlign="Center" />
                            <EditRowStyle BackColor="#ffffcc" />
                            <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                            <Columns>
                                <asp:BoundField DataField="SolicitudID" HeaderText="Cod. Interno">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha" DataFormatString="{0:d}">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Periodo" HeaderText="Periodo">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="TipoDescuento" HeaderText="Tipo Descuento">
                                    <ItemStyle Width="40%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Estado" HeaderText="Estado">
                                    <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <%-- <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="lEstado" runat="server" Text='<%# Bind("SwActivo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>--%>


                                <asp:TemplateField HeaderText="Acciones">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEditar" runat="server" CausesValidation="False" CommandName="Editar" CssClass="btn btn-primary" Text=""
                                            data-toggle="tooltip" title="Editar Datos" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-pencil-square-o"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                          <asp:LinkButton ID="lbAdjuntos" runat="server" CausesValidation="False" CommandName="Adjuntos" CssClass="btn btn-danger" Text=""
                                              data-toggle="tooltip" title="Documentos Adjuntos" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-file-pdf-o"></span>                                        
                                          </asp:LinkButton>
                                        &nbsp;
                                          <asp:LinkButton ID="lbNotificaciones" runat="server" CausesValidation="False" CommandName="Notificaciones" CssClass="btn btn-primary" Text=""
                                              data-toggle="tooltip" title="Ver Notificaciones" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-pencil-square-o"></span>                                        
                                          </asp:LinkButton>
                                        <%-- ============ FIN Alerta de confirmación eliminación de registro ============ --%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="alert alert-info" role="alert" id="Alerta1" runat="server">¡Aviso! No hay resultados para mostrar....</div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>



    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-danger" id="pnMensajeSinAcceso" runat="server" visible="false">
        <strong>
            <asp:Label ID="lMensaje1" runat="server" Text="Sin acceso"></asp:Label></strong><br />
        <strong>
            <asp:Label ID="lMensaje2" runat="server" Text="Atención!"></asp:Label></strong>
        <asp:Label ID="lMensajeDanger" runat="server" Text="No tienes acceso a este módulo consulta con el administrador"></asp:Label>
    </div>

    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-warning" id="pnAlert" runat="server" visible="false" style="text-align: left">
        <div class="form-group row">
            <div class="col-lg-1" style="text-align: right">
                <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
            </div>
            <div class="col-lg-11">
                <strong>
                    <asp:Label ID="Label2" runat="server" Text="Label:"></asp:Label></strong><br />
                <strong>
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></strong>
                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>

    <!-- Modal Para agregar Solicitudes-->
    <div id="ModaladdSolicitudes" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <asp:Label ID="lblEvento" runat="server" Text="Crear solicitudes"></asp:Label></strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DATOS SOLICITANTE</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Periodo:</strong>
                            <asp:Label ID="lbPeriodo" runat="server" Text="A2019"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Fecha Solicitud:</strong>
                            <asp:Label ID="lbFecha" runat="server" Text="28/03/2019"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Sede:</strong>
                            <asp:Label ID="lbSede" runat="server" Text="BUCARAMANGA"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Programa:</strong>
                            <asp:Label ID="lbPrograma" runat="server" Text="INGENIERIA INDUSTRIAL"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Codigo:</strong>
                            <asp:Label ID="lbCodigo" runat="server" Text="17151999"></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Estudiante:</strong>
                            <asp:Label ID="lbEstudiante" runat="server" Text="CARLOS NOVA"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DESCUENTO</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <strong>Tipo descuento</strong>
                            <asp:DropDownList ID="ddlTipoDescuento" runat="server" CssClass="form-control" Style="text-transform: uppercase" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group row" id="UnaAsignatura" runat="server" visible="false">
                        <div class="col-lg-6">
                            <strong>Asignatura:</strong>
                            <asp:TextBox ID="txtAsignatura" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-6">
                            <strong>Total creditos:</strong>
                            <asp:TextBox ID="txtTotalCreditos" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                    </div>
                    <%-- <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Tipo contrato:</strong>
                            <asp:DropDownList ID="ddlTipoContrato" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                <asp:ListItem Value="1" Selected="True">TIEMPO COMPLETO</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-6">
                            <strong>Programa al que pertenece:</strong>
                            <asp:DropDownList ID="ddlProgPertenece" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                <asp:ListItem Value="1" Selected="True">INGENIERIA CIVIL</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>--%>
                    <div class="form-group row" id="FamiliarFuncionario" runat="server" visible="false">
                        <div class="col-lg-6">
                            <strong>Nombre empleado:</strong>
                            <asp:TextBox ID="txtNombreEmpleado" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-6">
                            <strong>Programa o dependencia Adscrito:</strong>
                            <asp:DropDownList ID="ddlProgramaAds" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group row">
                        <%--   <div class="col-lg-6">
                            <strong>Programa Simultaneo:</strong>
                              <asp:DropDownList ID="ddlProgSilmultaneo" runat="server" CssClass="form-control" Style="text-transform: uppercase">                                
                            </asp:DropDownList>
                        </div>--%>
                        <div class="col-lg-6" id="Simultaneidad" runat="server" visible="false">
                            <strong>Código Simultaneo:</strong>
                            <asp:TextBox ID="txtCodigoSimultaneo" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DOCUMENTOS ADJUNTOS</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <asp:GridView ID="GVDocumentos" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="DocumentosDescuentoID, Obligatorio">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="DocumentosDescuentoID" HeaderText="Cod. Interno">
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Nombre" HeaderText="Documento">
                                        <ItemStyle Width="35%" />
                                    </asp:BoundField>

                                    <%--<asp:BoundField DataField="Obligatorio" HeaderText="Obligatorio">
                                        <ItemStyle Width="15%" />
                                    </asp:BoundField>--%>

                                    <asp:TemplateField HeaderText="Obligatorio">
                                        <ItemTemplate>
                                            <asp:Label ID="lEstado" runat="server" Text='<%# Bind("Obligatorio")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Seleccionar pdf">
                                        <ItemTemplate>
                                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="40%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Adjuntar">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbAjuntarIns" runat="server" CausesValidation="False" CommandName="AdjuntarIns" CssClass="btn btn-primary fa fa-upload" Text=""
                                                data-toggle="tooltip" title="Adjuntar pdf" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <%--<span class="fa fa-pencil-square-o"></span> --%>                                       
                                            </asp:LinkButton>
                                            &nbsp;
                                         
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ver">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbAVerjuntarIns" runat="server" CausesValidation="False" CommandName="VerAdjuntarIns" CssClass="btn btn-primary" Text=""
                                                data-toggle="tooltip" title="Ver pdf" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <%--<span class="fa fa-pencil-square-o"></span>   --%>                                     
                                            </asp:LinkButton>
                                            &nbsp;
                                         
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="lbAddSolicitud" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Actualizar </asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lbSalir" runat="server" CssClass="btn btn-danger" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Salir </asp:LinkButton>
                        <%-- <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Para ver documentos adjuntos de una Solicitudes-->
    <div id="ModaladdVerAdjunto" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>
                        <asp:Label ID="Label1" runat="server" Text="Ver documentos adjuntos de una solicitud"></asp:Label></strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DATOS SOLICITANTE</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Periodo:</strong>
                            <asp:Label ID="lblPeriodoVer" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Fecha Solicitud:</strong>
                            <asp:Label ID="lblFechaSolicitudVer" runat="server" Text="28/03/2019"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Programa:</strong>
                            <asp:Label ID="lblProgramaVer" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Solicitud Nro:</strong>
                            <asp:Label ID="lblSolicitudNroVer" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <strong>Codigo:</strong>
                            <asp:Label ID="lblCodigoVer" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-6">
                            <strong>Estudiante:</strong>
                            <asp:Label ID="lblEstudianteVer" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong>DESCUENTO</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <strong>Tipo descuento</strong>
                            <asp:DropDownList ID="ddlTipoDescuentoVer" runat="server" CssClass="form-control" Style="text-transform: uppercase" AutoPostBack="false" Enabled="false">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12 bg-primary">
                            <strong><%--DOCUMENTOS ADJUNTOS--%>
                                <asp:Label ID="lblTitDoc" runat="server" Text="DOCUMENTOS ADJUNTOS"></asp:Label>
                            </strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <asp:GridView ID="GVDocumentosVer" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="DocumentosDescuentoID, Obligatorio">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="DocumentosDescuentoID" HeaderText="Cod. Interno">
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Nombre" HeaderText="Documento">
                                        <ItemStyle Width="35%" />
                                    </asp:BoundField>


                                    <asp:TemplateField HeaderText="Obligatorio">
                                        <ItemTemplate>
                                            <asp:Label ID="lEstado" runat="server" Text='<%# Bind("Obligatorio")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ver">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbAVerjuntarVer" runat="server" CausesValidation="False" CommandName="VerAdjuntarVer" CssClass="btn btn-danger fa fa-file-pdf-o" Text=""
                                                data-toggle="tooltip" title="Ver pdf" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <%--<span class="fa fa-pencil-square-o"></span>   --%>                                     
                                            </asp:LinkButton>
                                            &nbsp;
                                         
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                            <asp:GridView ID="GVNotificaiones" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                                DataKeyNames="NotificacionID">
                                <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                                <PagerStyle HorizontalAlign="Center" />
                                <EditRowStyle BackColor="#ffffcc" />
                                <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                <Columns>
                                    <asp:BoundField DataField="NotificacionID" HeaderText="Cod. Interno" Visible="false">
                                        <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Observacion" HeaderText="Observación">
                                        <ItemStyle Width="85%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaRegistro" HeaderText="Fecha" DataFormatString="{0:d}">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>



                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:LinkButton ID="lbExportar" runat="server" CssClass="btn btn-success fa fa-file-pdf-o" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Exportar </asp:LinkButton>
                        <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
