﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Principal.Master" CodeBehind="Usuarios.aspx.vb" Inherits="KidsPlenitud.Usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        function FuncionModalAddUsuarios() {
            $("#ModaladdUsuarios").modal("show")
        }
        function FuncionModalUpdUsuarios() {
            $("#ModalUpdUsuarios").modal("show")
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="main_title" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Header" runat="server">
</asp:Content>--%>
<asp:Content ID="Content5" ContentPlaceHolderID="Main_Texto" runat="server">
</asp:Content>
<%--<asp:Content ID="Content6" ContentPlaceHolderID="navbar" runat="server">
</asp:Content>--%>
<asp:Content ID="Content7" ContentPlaceHolderID="Main_pagination" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="Modulos.aspx">Inicio</a></li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
</asp:Content>
<%--<asp:Content ID="Content8" ContentPlaceHolderID="main_content" runat="server">
</asp:Content>--%>
<asp:Content ID="Content9" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Panel ID="pnUsuarios" runat="server" Visible="true">
        <div class="panel panel-default" id="xxx" runat="server">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-8">
                        Usuarios
                    </div>
                    <div class="col-lg-4" style="text-align: right">
                        <asp:LinkButton ID="lbANewUsuarios" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-plus"></span>&nbsp;Nuevo 
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Cuadro de Consulta</strong>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        Nombre
                         <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control">
                         </asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        Maestros	
                         <asp:DropDownList ID="ddlMaestro" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                             <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                             <asp:ListItem Value="1">Si</asp:ListItem>
                             <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                         </asp:DropDownList>
                    </div>

                    <div class="col-lg-3">
                        Estado	
                         <asp:DropDownList ID="ddlEstadoConsul" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                             <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                             <asp:ListItem Value="0">Inactivo</asp:ListItem>
                             <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                         </asp:DropDownList>
                    </div>
                </div>


                <div class="form-group row">

                    <div class="row">
                        <div class="col-lg-12" style="text-align: center">
                            <asp:LinkButton ID="lbBuscar" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Buscar 
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <br />
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Listado</strong>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <asp:GridView ID="GVUsuarios" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                            DataKeyNames="UsuarioId,SwActivo">
                            <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                            <PagerStyle HorizontalAlign="Center" />
                            <EditRowStyle BackColor="#ffffcc" />
                            <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                            <Columns>
                                <asp:BoundField DataField="UsuarioId" HeaderText="Cod. Interno">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                    <ItemStyle Width="25%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Telefono" HeaderText="Telefono">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Correo" HeaderText="Correo">
                                    <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Clave" HeaderText="Clave">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Admin" HeaderText="Admin">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Maestro" HeaderText="Maestro">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="lEstado" runat="server" Text='<%# Bind("SwActivo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Acciones">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEditar" runat="server" CausesValidation="False" CommandName="EditarUsuarios" CssClass="btn btn-primary" Text=""
                                            data-toggle="tooltip" title="Editar Datos" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-pencil-square-o"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                          <asp:LinkButton ID="lbEliminar" runat="server" CommandName="Delete" CssClass="btn btn-danger" Text="Error"
                                              data-toggle="tooltip" title="Activar/Desactivar" OnClientClick="return deletealert2(this, event);">
                                                <span class="fa fa-trash-o fa-lg"></span>
                                          </asp:LinkButton>
                                        <%-- ============ Alerta de confirmación eliminación de registro ============ --%>
                                        <script>
                                            function deletealert2(ctl, event) {
                                                // STORE HREF ATTRIBUTE OF LINK CTL (THIS) BUTTON
                                                var defaultAction = $(ctl).prop("href");
                                                // CANCEL DEFAULT LINK BEHAVIOUR
                                                event.preventDefault();
                                                swal({
                                                    title: "¿Está Seguro de cambiar el estado de este registro?",
                                                    text: "",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Si, ¡Cambiar estado!",
                                                    cancelButtonText: "No, cancelar",
                                                    closeOnConfirm: false,
                                                    //closeOnCancel: false
                                                },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        //swal({ title: "Eliminado!", text: "Tu archivo imaginario ha sido eliminado.", type: "success", confirmButtonText: "OK!", closeOnConfirm: false },
                                                        //function () {
                                                        //    // RESUME THE DEFAULT LINK ACTION
                                                        window.location.href = defaultAction;
                                                        return true;
                                                        //});                                                            
                                                    } else {
                                                        //swal("Cancelado", "Tu archivo imaginario es seguro :)", "error");
                                                        return false;
                                                    }
                                                });
                                            }
                                        </script>
                                        <%-- ============ FIN Alerta de confirmación eliminación de registro ============ --%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="alert alert-info" role="alert" id="Alerta1" runat="server">¡Aviso! No hay resultados para mostrar....</div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>



    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-danger" id="pnMensajeSinAcceso" runat="server" visible="false">
        <strong>
            <asp:Label ID="lMensaje1" runat="server" Text="Sin acceso"></asp:Label></strong><br />
        <strong>
            <asp:Label ID="lMensaje2" runat="server" Text="Atención!"></asp:Label></strong>
        <asp:Label ID="lMensajeDanger" runat="server" Text="No tienes acceso a este módulo consulta con el administrador"></asp:Label>
    </div>

    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-warning" id="pnAlert" runat="server" visible="false" style="text-align: left">
        <div class="form-group row">
            <div class="col-lg-1" style="text-align: right">
                <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
            </div>
            <div class="col-lg-11">
                <strong>
                    <asp:Label ID="Label2" runat="server" Text="Label:"></asp:Label></strong><br />
                <strong>
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></strong>
                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>

    <!-- Modal Para agregar Usuarios-->
    <div id="ModaladdUsuarios" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Crear Usuarios</strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            Nombre del Usuario
                            <asp:TextBox ID="txtNombreAdd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-6">
                            Direccion
                            <asp:TextBox ID="txtDireccionAdd" runat="server" CssClass="form-control" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                       
                    </div>
                    <div class="form-group row">
                         <div class="col-lg-4">
                            Telefono
                            <asp:TextBox ID="txtTelefonoAdd" runat="server" CssClass="form-control"> </asp:TextBox>
                        </div>
                        <div class="col-lg-4">
                            Correo
                            <asp:TextBox ID="txtCorreoAdd" runat="server" CssClass="form-control" required=""> </asp:TextBox>
                        </div>
                        <div class="col-lg-4">
                            Clave
                            <asp:TextBox ID="txtClaveAdd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            Fecha nacimiento
                                     <div class='input-group date' id='rdpFechaNacimientoAdd'>
                                         <input type='text' class="form-control" id="dtpFechaNacimientoAdd" runat="server" placeholder="dd/mm/aaaa"
                                             data-toggle="tooltip" title="Formato dd/mm/aaaa" data-placement="left" />
                                         <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                     </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#rdpFechaNacimientoAdd').datetimepicker({
                                        format: 'DD/MM/YYYY'
                                    });
                                });
                            </script>
                        </div>
                        <div class="col-lg-4">
                            Admin
                             <asp:DropDownList ID="ddlAdminAdd" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                 <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                 <asp:ListItem Value="1">Si</asp:ListItem>
                                 <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                             </asp:DropDownList>
                        </div>
                         <div class="col-lg-4">
                            Maestros
                             <asp:DropDownList ID="ddlMaestrosAdd" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                 <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                 <asp:ListItem Value="1">Si</asp:ListItem>
                                 <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                             </asp:DropDownList>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <asp:LinkButton ID="lbAddUsuarios" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Agregar </asp:LinkButton>
                        &nbsp;&nbsp;
                    <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <!-- Modal Para Editar Usuarios-->
    <div id="ModalUpdUsuarios" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Editar Usuarios</strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            Nombre del Usuario
                            <asp:TextBox ID="txtNombreUpd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-6">
                            Direccion
                            <asp:TextBox ID="txtDireccionUpd" runat="server" CssClass="form-control" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                       
                    </div>
                    <div class="form-group row">
                         <div class="col-lg-4">
                            Telefono
                            <asp:TextBox ID="txtTelefonoUpd" runat="server" CssClass="form-control"> </asp:TextBox>
                        </div>
                        <div class="col-lg-4">
                            Correo
                            <asp:TextBox ID="txtCorreoUpd" runat="server" CssClass="form-control" required=""> </asp:TextBox>
                        </div>
                        <div class="col-lg-4">
                            Clave
                            <asp:TextBox ID="txtClaveUpd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            Fecha nacimiento
                                     <div class='input-group date' id='rdpFechaNacimientoUpd'>
                                         <input type='text' class="form-control" id="dtpFechaNacimientoUpd" runat="server" placeholder="dd/mm/aaaa"
                                             data-toggle="tooltip" title="Formato dd/mm/aaaa" data-placement="left" />
                                         <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                     </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#rdpFechaNacimientoUpd').datetimepicker({
                                        format: 'DD/MM/YYYY'
                                    });
                                });
                            </script>
                        </div>
                        <div class="col-lg-4">
                            Admin
                             <asp:DropDownList ID="ddlAdminUpd" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                 <asp:ListItem Value="0">No</asp:ListItem>
                                 <asp:ListItem Value="1">Si</asp:ListItem>
                                 <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                             </asp:DropDownList>
                        </div>
                         <div class="col-lg-4">
                            Maestros
                             <asp:DropDownList ID="ddlMaestroUpd" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                                 <asp:ListItem Value="0">No</asp:ListItem>
                                 <asp:ListItem Value="1">Si</asp:ListItem>
                                 <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                             </asp:DropDownList>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <asp:LinkButton ID="lbActualizar" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Actualizar </asp:LinkButton>
                        &nbsp;&nbsp;
                    <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
