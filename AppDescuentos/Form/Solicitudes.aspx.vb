﻿Imports System.Net

Imports Newtonsoft.Json.Linq.JToken
Imports Newtonsoft.Json.Linq.JContainer
Imports Newtonsoft.Json.Linq.JObject
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports System.Security.Cryptography
Public Class Solicitudes
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Ref_student As String = ""
        Dim SedeCod As String = ""
        If (Request.QueryString.Count > 0) Then
            If Request.QueryString("SedeCod") <> Nothing And Request.QueryString("Ref_student") <> Nothing And Request.QueryString("Periodo") <> Nothing Then
                Try
                    SedeCod = Request.QueryString("SedeCod").ToString()
                    Ref_student = Request.QueryString("Ref_student")
                    Me.ViewState("Periodo") = Request.QueryString("Periodo")
                Catch ex As Exception

                End Try

            End If

        End If

        If Not IsPostBack Then
            If SedeCod <> "" Then
                Dim request As HttpWebRequest
                Dim response As HttpWebResponse = Nothing
                Dim reader As StreamReader

                Dim ServicioSIGAUDES As String = ""

                Select Case SedeCod
                    Case "01"
                        'ServicioSIGAUDES = "http://192.168.3.156:8000/certificates/students_provider/student/discount/4d22e0836283a6c5713af13ef66e7fce8d8d4716/?api_key=afe69536d0124488b5591efbcafe2924"
                        ServicioSIGAUDES = "http://192.168.3.156:8000/certificates/students_provider/student/discount/"
                    Case "02"

                    Case "03"

                    Case "04"

                    Case "05"

                        'SedeCod = "04"
                End Select

                Dim Link As String = ServicioSIGAUDES & Ref_student & "/?api_key=" & MD5Encrypt()

                Try
                    request = DirectCast(WebRequest.Create(Link), HttpWebRequest)
                    response = DirectCast(request.GetResponse(), HttpWebResponse)
                    reader = New StreamReader(response.GetResponseStream())

                    Dim rawresp As String
                    rawresp = reader.ReadToEnd()

                    Dim jResults As Object = JObject.Parse(rawresp)

                    If jResults("msg")("code") Is Nothing Then
                        'Me.Label1.Text = "No se encuentra en el sistema de matriculas"
                        Exit Sub
                    Else

                        Dim EstudCod As String = jResults("msg")("code").ToString
                        Dim Nombre As String = jResults("msg")("name").ToString
                        Dim ProgCod As String = jResults("msg")("program_code").ToString
                        Dim Correo As String = jResults("msg")("email").ToString
                        Dim TipoDocumento As String = jResults("msg")("typeID").ToString
                        Dim Documento As String = jResults("msg")("identification").ToString
                        Dim Departamento As String = jResults("msg")("home_dpt_text").ToString
                        Dim Ciudad As String = jResults("msg")("home_reg_text").ToString
                        Dim Direccion As String = jResults("msg")("address").ToString
                        Dim Barrio As String = jResults("msg")("neighborhood").ToString
                        Dim Telefono As String = jResults("msg")("phone").ToString

                        Dim dt As DataTable = ObjUniver.CRUDEstudiantes(0, Nombre, SedeCod, ProgCod, EstudCod, Correo, TipoDocumento, Documento, Departamento, Ciudad, Direccion, Barrio, Telefono, "Crear")
                        If dt.Rows.Count > 0 Then
                            Me.Session("IDuser") = dt.Rows(0).Item("EstudianteID")
                            Me.Session("id_usuario") = dt.Rows(0).Item("EstudianteID")
                            Me.Session("NomUser") = Nombre
                            Me.Session("Administrador") = False
                            Me.Session("SedeCod") = SedeCod
                            Me.Session("EstudCod") = EstudCod
                            Me.Session("Dependencia") = False

                            DatosBasicos(SedeCod, EstudCod)

                            Periodos()
                            TipoDescuentos()
                            ProgramasDep()
                        End If


                    End If

                Catch ex As Exception

                End Try
            End If
            Buscar()
            'BuscarDocumentos()
        End If
    End Sub
    Public Sub DatosBasicos(SedeCod As String, EstudCod As String)
        Dim dt As DataTable = ObjUniver.CRUDEstudiantes(0, "", SedeCod, "", EstudCod, "", "", "", "", "", "", "", "", "Buscar")
        If dt.Rows.Count > 0 Then
            lbPeriodo.Text = Me.ViewState("Periodo")
            lbSede.Text = dt.Rows(0).Item("SedeNombre")
            lbPrograma.Text = dt.Rows(0).Item("ProgNombre")
            lbCodigo.Text = EstudCod
            lbEstudiante.Text = dt.Rows(0).Item("Nombre")
            lbFecha.Text = DateTime.Now.ToString("dd/MM/yyyy")

            lblPeriodoVer.Text = Me.ViewState("Periodo")
            lblProgramaVer.Text = dt.Rows(0).Item("ProgNombre")
            lblCodigoVer.Text = EstudCod
            lblEstudianteVer.Text = dt.Rows(0).Item("Nombre")
            'lblFechaSolicitudVer.Text = DateTime.Now.ToString("dd/MM/yyyy")

        End If
    End Sub
    Public Function MD5Encrypt()
        Dim PasConMd5 As String = ""
        Dim Cadena As String = "76d80224611fc919a5d54f0ff9fba446" & Year(Now) & Month(Now).ToString.PadLeft(2, "0"c) & Day(Now).ToString.PadLeft(2, "0"c)
        Dim md5 As New MD5CryptoServiceProvider
        Dim bytValue() As Byte
        Dim bytHash() As Byte
        Dim i As Integer

        bytValue = System.Text.Encoding.UTF8.GetBytes(Cadena)

        bytHash = md5.ComputeHash(bytValue)
        md5.Clear()

        For i = 0 To bytHash.Length - 1
            PasConMd5 &= bytHash(i).ToString("x").PadLeft(2, "0")
        Next

        Return PasConMd5

    End Function
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUDSolicitudes(0, ddlPeriodo.SelectedValue, Me.Session("IDuser"), 0, 0, "", "", 0, "", "", "", "", "", "Buscar")
        If dt.Rows.Count > 0 Then
            ViewState("Solicitudes") = dt
            GVSolicitudes.DataSource = dt
            GVSolicitudes.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Solicitudes") = Nothing
            GVSolicitudes.DataSource = Nothing
            GVSolicitudes.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    Public Sub BuscarDocumentos()
        'Dim dt As DataTable = ObjUniver.CRUBDocumentosDescuento(0, "", 0, 0, "Buscar")
        Dim dt As DataTable = ObjUniver.CRUBDocTipoDescuentos(0, 0, ddlTipoDescuento.SelectedValue, "BuscarDocSolicitud")
        If dt.Rows.Count > 0 Then
            ViewState("Documentos") = dt
            GVDocumentos.DataSource = dt
            GVDocumentos.DataBind()

            'GVAdjuntos.DataSource = dt
            'GVAdjuntos.DataBind()
            Alerta1.Visible = False
        Else
            ViewState("Documentos") = Nothing
            GVDocumentos.DataSource = Nothing
            GVDocumentos.DataBind()

            'GVAdjuntos.DataSource = Nothing
            'GVAdjuntos.DataBind()
            Alerta1.Visible = True
        End If
    End Sub

    Protected Sub lbNewTipoContrato_Click(sender As Object, e As EventArgs) Handles lbNewTipoContrato.Click
        Limpiar()
        lbPeriodo.Text = ViewState("Periodo")
        lblEvento.Text = "Crear solicitudes"
        lbAddSolicitud.Text = "Agregar"
        ViewState("Accion") = "Crear" 'Crear
        ViewState("SolicitudID") = 0
        GVDocumentos.DataSource = Nothing
        GVDocumentos.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
    End Sub
    Protected Sub GVDocumentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDocumentos.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim btnVerAdjunto As System.Web.UI.WebControls.LinkButton = DirectCast(e.Row.FindControl("lbAVerjuntarIns"), System.Web.UI.WebControls.LinkButton)

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim dt As DataTable = ObjUniver.CRUBSolicitudDocumentos(0, ViewState("SolicitudID"), e.Row.Cells(0).Text, "", "BuscarSolicitudDoc")
            If dt.Rows.Count > 0 Then
                btnVerAdjunto.ForeColor = Drawing.Color.White
                btnVerAdjunto.CssClass = "btn btn-danger fa fa-file-pdf-o"
            Else
                btnVerAdjunto.CssClass = ""
            End If
        End If


        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
            End If

            Estado.Text = ""

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GVSolicitudes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVSolicitudes.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        ViewState("SolicitudID") = Me.GVSolicitudes.DataKeys(index).Values(0)
        If e.CommandName = "Editar" Then
            Limpiar()
            lblEvento.Text = "Editar solicitud"
            lbAddSolicitud.Text = "Actualizar"
            ViewState("Accion") = "Modificar" 'Modificar
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
           


            'Dim dt As DataTable = ObjUniversal.CRUBHojaVida(index, txtEdadUpd.Text.ToUpper, dpFechaNacimientoUpd.Value, txtEdadUpd.Text, txtAcudienteUpd.Text.ToUpper, txtTelefonoUpd.Text, txtDireccionUpd.Text.ToUpper, 1, "Modificar")

            Dim dt As DataTable = ObjUniver.CRUDSolicitudes(ViewState("SolicitudID"), "", 0, 0, 0, "", "", 0, "", "", "", "", "", "BuscarID")
            If dt.Rows.Count > 0 Then
                lbPeriodo.Text = dt.Rows(0).Item("Periodo")
                ddlTipoDescuento.SelectedValue = dt.Rows(0).Item("TipoDescuentoID")
                txtAsignatura.Text = dt.Rows(0).Item("Asignatura")
                txtCodigoSimultaneo.Text = dt.Rows(0).Item("CodigoSimultanea")
                txtNombreEmpleado.Text = dt.Rows(0).Item("NombreEmpleado")
                txtTotalCreditos.Text = dt.Rows(0).Item("TotalCreditos")
                ddlProgramaAds.SelectedValue = dt.Rows(0).Item("ProgAdscrito")
                OcultarControles()
                MostrarControles()

                BuscarDocEditar(ViewState("SolicitudID"), dt.Rows(0).Item("TipoDescuentoID"))
            End If
        End If
        If e.CommandName = "Adjuntos" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddVerAdjunto", "FuncionModalAddVerAdjunto();", True)

            Dim dt As DataTable = ObjUniver.CRUDSolicitudes(ViewState("SolicitudID"), "", 0, 0, 0, "", "", 0, "", "", "", "", "", "BuscarID")
            If dt.Rows.Count > 0 Then
                lblTitDoc.Text = "DOCUMENTOS ADJUNTOS"
                lblPeriodoVer.Text = dt.Rows(0).Item("Periodo")
                ddlTipoDescuentoVer.SelectedValue = dt.Rows(0).Item("TipoDescuentoID")
                lblSolicitudNroVer.Text = ViewState("SolicitudID")
                BuscarSolicitudDocumentos()
                GVDocumentosVer.Visible = True
                GVNotificaiones.Visible = False
            End If
        End If

        If e.CommandName = "Notificaciones" Then
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddVerAdjunto", "FuncionModalAddVerAdjunto();", True)

            Dim dt As DataTable = ObjUniver.CRUDSolicitudes(ViewState("SolicitudID"), "", 0, 0, 0, "", "", 0, "", "", "", "", "", "BuscarID")
            If dt.Rows.Count > 0 Then
                lblTitDoc.Text = "NOTIFICACIONES"
                lblPeriodoVer.Text = dt.Rows(0).Item("Periodo")
                ddlTipoDescuentoVer.SelectedValue = dt.Rows(0).Item("TipoDescuentoID")
                lblSolicitudNroVer.Text = ViewState("SolicitudID")
                BuscarNotificaciones()
                GVDocumentosVer.Visible = False
                GVNotificaiones.Visible = True
            End If
        End If
    End Sub

    Public Sub Periodos()
        Dim dt As DataTable = ObjUniver.LlenarDropDownLists("", "Periodo")

        If dt.Rows.Count > 0 Then
            With Me.ddlPeriodo
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "Periodo"
                .DataTextField = "Periodo"
                .DataBind()
                .Items.Add(New ListItem("Seleccione periodo", "-1"))
            End With

            ddlPeriodo.SelectedValue = -1

        End If
    End Sub
    Public Sub TipoDescuentos()
        Dim dt As DataTable = ObjUniver.LlenarDropDownLists("", "TipoDescuento")

        If dt.Rows.Count > 0 Then
            With Me.ddlTipoDescuento
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuento.SelectedValue = -1

            With Me.ddlTipoDescuentoVer
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuentoVer.SelectedValue = -1

        End If
    End Sub
    Public Sub ProgramasDep()
        Dim dt As DataTable = ObjUniver.LlenarDropDownLists(Me.Session("SedeCod"), "ProgramasDep")

        If dt.Rows.Count > 0 Then
            With Me.ddlProgramaAds
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "ProgCod"
                .DataTextField = "ProgNombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione programa o dependencia", "-1"))
            End With

            ddlProgramaAds.SelectedValue = -1

        End If
    End Sub

    Protected Sub ddlTipoDescuento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTipoDescuento.SelectedIndexChanged

        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)

       
        OcultarControles()
        MostrarControles()

        BuscarDocumentos()
    End Sub
    Public Sub MostrarControles()
        Select Case ddlTipoDescuento.SelectedValue
            Case 5 'Una Asignatura
                UnaAsignatura.Visible = True
            Case 3 'Familiar Funcionario Udes
                FamiliarFuncionario.Visible = True
            Case 6 'Simultaneidad
                Simultaneidad.Visible = True
        End Select
    End Sub
    Public Sub OcultarControles()
        UnaAsignatura.Visible = False
        FamiliarFuncionario.Visible = False
        Simultaneidad.Visible = False
    End Sub
    Public Sub Crear()
        If ddlTipoDescuento.SelectedValue = -1 Then
            ObjUniver.SendNotify("Seleccionar tipo descuento", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
            Exit Sub
        End If

        Dim ProgSimultaneo As String = ""

        Select Case ddlTipoDescuento.SelectedValue
            Case 5 'Una Asignatura
                If Trim(txtAsignatura.Text) = "" Or Trim(txtTotalCreditos.Text) = "" Then
                    ObjUniver.SendNotify("Asignatura y total creditos no pueden estar vacios", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                    Exit Sub
                End If
            Case 3 'Familiar Funcionario Udes
                If Trim(txtNombreEmpleado.Text) = "" Or ddlProgramaAds.SelectedValue = "-1" Then
                    ObjUniver.SendNotify("Nombre empleado no puede estar vacio y debe seleccionar un programa o dependencia", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                    Exit Sub
                End If
            Case 6 'Simultaneidad
                If Trim(txtCodigoSimultaneo.Text) = "" Then
                    ObjUniver.SendNotify("Simulaneidad no puede estar vacio", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                    Exit Sub
                Else
                    ProgSimultaneo = Mid(txtCodigoSimultaneo.Text, 3, 2)
                End If
                Simultaneidad.Visible = True
        End Select

        Dim Mensaje As String = ""
        Dim dtValidar As DataTable = ObjUniver.CRUDSolicitudes(0, Me.ViewState("Periodo"), Me.Session("IDuser"), ddlTipoDescuento.SelectedValue, 0, "", "", 0, "", "", "", "", "", "ValidarExiste")
        Dim dtValidarMod As DataTable = ObjUniver.CRUDSolicitudes(ViewState("SolicitudID"), Me.ViewState("Periodo"), Me.Session("IDuser"), ddlTipoDescuento.SelectedValue, 0, "", "", 0, "", "", "", "", "", "ValidarExisteMod")
        'Dim dtValidarCorreo As DataTable = ObjUniver.ValidarNombre(Me.Session("SedeCod"), txtCorreoAdd.Text, 0, "DependenciaCorreo")
        'Dim dtValidarCorreoMod As DataTable = ObjUniver.ValidarNombre(Me.Session("SedeCod"), txtCorreoAdd.Text, ViewState("DependenciaID"), "DependenciaCorreoMod")

        If ViewState("Accion") = "Crear" Then
            If dtValidar.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe una solicitud de ese tipo de descuento para el periodo " & Me.ViewState("Periodo"), "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                Exit Sub
            Else
                Mensaje = "Se creó el registro con éxito"
            End If
        Else
            If dtValidarMod.Rows.Count > 0 Then
                ObjUniver.SendNotify("Ya existe una solicitud de ese tipo de descuento para el periodo " & Me.ViewState("Periodo"), "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                Exit Sub
            Else
                Mensaje = "Se modificó el registro con éxito"

            End If

        End If


        'If GVDocumentos.Rows.Count > 0 Then
        '    For i = 0 To GVDocumentos.Rows.Count - 1
        '        Dim sExt As String = String.Empty
        '        Dim sName As String = String.Empty

        '        Dim FileUpl As System.Web.UI.WebControls.FileUpload = DirectCast(GVDocumentos.Rows(i).FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)


        '        If FileUpl.HasFile Then
        '            sName = FileUpl.FileName
        '            sExt = Right(sName, 4)

        '            If sExt <> ".pdf" Then
        '                ObjUniver.SendNotify("Solo se permite adjuntar en formato pdf ", "3")
        '                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
        '                Exit Sub
        '            End If
        '        Else
        '            If GVDocumentos.Rows(i).Cells(2).Text = True Then
        '                ObjUniver.SendNotify("Solo se permite adjuntar en formato pdf ", "3")
        '                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
        '                Exit Sub
        '            End If

        '        End If
        '    Next
        'End If


        'Dim dt As DataTable = ObjUniver.CRUBDependencias(ViewState("DependenciaID"), Me.Session("SedeCod"), Trim(txtCodDependenciaAdd.Text.ToUpper), Trim(txtNombreDepenAdd.Text.ToUpper), Trim(txtNombrePersonaAdd.Text.ToUpper), Trim(txtCorreoAdd.Text), ddlAdministrador.SelectedValue, 0, ViewState("Accion"))
        Dim dt As DataTable = ObjUniver.CRUDSolicitudes(ViewState("SolicitudID"), Me.ViewState("Periodo"), Me.Session("IDuser"), ddlTipoDescuento.SelectedValue, 1, Trim(txtAsignatura.Text), Trim(txtTotalCreditos.Text), 0, "", Trim(txtNombreEmpleado.Text), ddlProgramaAds.SelectedValue, ProgSimultaneo, (txtCodigoSimultaneo.Text), ViewState("Accion"))
        If dt.Rows.Count > 0 Then
           
            'If GVDocumentos.Rows.Count > 0 Then
            '    For i = 0 To GVDocumentos.Rows.Count - 1
            '        Dim SolicitudDocumentoID As String = GVDocumentos.Rows(i).Cells(0).Text
            '        Dim FileUpl As System.Web.UI.WebControls.FileUpload = DirectCast(GVDocumentos.Rows(i).FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
            '        Dim RutaArchivo As String = FileUpl.FileName

            '        ' Dim FileUpl As FileUpload

            '        Dim dtDocumentos As DataTable = ObjUniver.CRUBSolicitudDocumentos(0, dt.Rows(0).Item(0), GVDocumentos.Rows(i).Cells(0).Text, RutaArchivo, "Crear")
            '    Next
            'End If

            ViewState("SolicitudID") = dt.Rows(0).Item(0)
            'ObjUniver.SendNotify(Mensaje, "1")
            'Buscar()
        End If
    End Sub
    Protected Sub lbAddSolicitud_Click(sender As Object, e As EventArgs) Handles lbAddSolicitud.Click
        Dim dt As DataTable = ObjUniver.ValidarAdjuntos(ViewState("SolicitudID"), ddlTipoDescuento.SelectedValue, "ValidarAdjunto")

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item(0) = 0 Then
                ObjUniver.SendNotify("Debe adjuntar todos los documentos requeidos antes de agregar ", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
             
            Else
                If ViewState("Accion") = "Crear" Then
                    Buscar()
                    ObjUniver.SendNotify("Se creó el registro con éxito ", "1")
                Else
                    Crear()
                    Buscar()
                    ObjUniver.SendNotify("Se modificó el registro con éxito ", "1")
                End If

            End If
                'Else
                '    Crear()
                '    ObjUniver.SendNotify("Se modificó el registro con éxito ", "1")
                'End If
            Else
                ObjUniver.SendNotify("No se puede agregar la solicitud, verificar si estan todos los adjuntos ", "3")
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
            End If



        'Crear()
    End Sub
    Protected Sub lbSalir_Click(sender As Object, e As EventArgs) Handles lbSalir.Click
        If ViewState("Accion") = "Crear" Then
            Dim dt As DataTable = ObjUniver.ValidarAdjuntos(ViewState("SolicitudID"), 0, "Eliminarsolicitud")
            Buscar()
        End If

    End Sub
    Public Sub Limpiar()
        txtAsignatura.Text = ""
        txtCodigoSimultaneo.Text = ""
        txtNombreEmpleado.Text = ""
        txtTotalCreditos.Text = ""
        ddlTipoDescuento.SelectedValue = -1
        ddlProgramaAds.SelectedValue = "-1"
    End Sub
   
    Protected Sub GVDocumentos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVDocumentos.RowCommand
        'Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim DocumentosDescuentoID As Integer = Me.GVDocumentos.DataKeys(index).Values(0)
        Dim Obligatorio As Boolean = Me.GVDocumentos.DataKeys(index).Values(1)


        Dim FileUpl As System.Web.UI.WebControls.FileUpload = DirectCast(GVDocumentos.Rows(index).FindControl("FileUpload1"), System.Web.UI.WebControls.FileUpload)
        If e.CommandName = "AdjuntarIns" Then
            If Me.ViewState("CrearSubirArchivo") <> 1 Then
                Crear()
            End If

            Dim sExt As String = String.Empty
            Dim sName As String = String.Empty
            Dim NombreArchivo As String = ViewState("SolicitudID").ToString + DocumentosDescuentoID.ToString


            If FileUpl.HasFile Then
                sName = FileUpl.FileName
                sExt = Right(sName, 4)

                If ValidaExtension(sExt) Then
                    'Dim imagePath As String = String.Format("~/FotoUser/{0}.png", Me.Session("IDuser"))
                    Dim RutaPath As String = String.Format("~/DocAdjuntos/{0}.pdf", NombreArchivo)
                    File.WriteAllBytes(Server.MapPath(RutaPath), FileUpl.FileBytes)

                    Dim dtDocumentos As DataTable = ObjUniver.CRUBSolicitudDocumentos(0, ViewState("SolicitudID"), DocumentosDescuentoID, ResolveUrl(RutaPath), "Crear")
                    If dtDocumentos.Rows.Count > 0 Then
                        Me.ViewState("CrearSubirArchivo") = 1
                        ObjUniver.SendNotify("Se subio el archivo con exito ", "1")
                        BuscarDocumentos()
                        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                    End If
                Else
                    ObjUniver.SendNotify("Solo se permite adjuntar en formato pdf ", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                    Exit Sub
                End If
            Else
                If Obligatorio = True Then
                    ObjUniver.SendNotify("Selecione el documento en pdf ", "3")
                    ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
                    Exit Sub
                End If
               
            End If

        End If
        If e.CommandName = "VerAdjuntarIns" Then
            Dim dt As DataTable = ObjUniver.CRUBSolicitudDocumentos(0, ViewState("SolicitudID"), DocumentosDescuentoID, "", "BuscarSolicitudDoc")
            If dt.Rows.Count > 0 Then
                'Response.Redirect("~" & dt.Rows(0).Item("RutaArchivo"))
                Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
                With strBuilder
                    .Append("<script language='javascript'>")
                    .Append("window.open('" & dt.Rows(0).Item("RutaArchivo") & "', '');")
                    .Append("</script>")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
                End With
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddSolicitudes", "FuncionModalAddSolicitudes();", True)
            End If
        End If
    End Sub
    Private Function ValidaExtension(ByVal sExtension As String) As Boolean
        Select Case sExtension
            Case ".pdf"
                Return True
            Case Else
                Return False
        End Select
    End Function
    Public Sub BuscarDocEditar(SolicitudID As Integer, TipoDescuentoID As Integer)
        Dim dt As DataTable = ObjUniver.CRUBSolicitudDocumentos(TipoDescuentoID, SolicitudID, 0, "", "BuscarSolicitudDocEdit")
        If dt.Rows.Count > 0 Then
            ViewState("Documentos") = dt
            GVDocumentos.DataSource = dt
            GVDocumentos.DataBind()

        Else
            ViewState("Documentos") = Nothing
            GVDocumentos.DataSource = Nothing
            GVDocumentos.DataBind()


        End If
    End Sub
    Public Sub BuscarSolicitudDocumentos()
        Dim dt As DataTable = ObjUniver.CRUBSolicitudDocumentos(ddlTipoDescuentoVer.SelectedValue, ViewState("SolicitudID"), 0, "", "Buscar")
        If dt.Rows.Count > 0 Then
            If dt.Rows.Count > 0 Then
                ViewState("Documentos") = dt
                GVDocumentosVer.DataSource = dt
                GVDocumentosVer.DataBind()

            Else
                ViewState("Documentos") = Nothing
                GVDocumentosVer.DataSource = Nothing
                GVDocumentosVer.DataBind()
            End If
        End If
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub
    Protected Sub GVDocumentosVer_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDocumentosVer.RowDataBound
        Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)

        Try
            If Estado.Text = True Then
                Estado.CssClass = "fa fa-check fa-1x" : Estado.ForeColor = Drawing.Color.Green
            Else
                Estado.CssClass = "fa fa-times fa-1x" : Estado.ForeColor = Drawing.Color.Red
            End If

            Estado.Text = ""

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GVDocumentosVer_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GVDocumentosVer.RowCommand
        'Dim Estado As System.Web.UI.WebControls.Label = DirectCast(e.Row.FindControl("lEstado"), System.Web.UI.WebControls.Label)
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim DocumentosDescuentoID As Integer = Me.GVDocumentosVer.DataKeys(index).Values(0)

        If e.CommandName = "VerAdjuntarVer" Then
            Dim dt As DataTable = ObjUniver.CRUBSolicitudDocumentos(0, ViewState("SolicitudID"), DocumentosDescuentoID, "", "BuscarSolicitudDoc")
            If dt.Rows.Count > 0 Then
                'Response.Redirect("~" & dt.Rows(0).Item("RutaArchivo"))
                Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
                With strBuilder
                    .Append("<script language='javascript'>")
                    .Append("window.open('" & dt.Rows(0).Item("RutaArchivo") & "', '');")
                    .Append("</script>")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
                End With
                ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModalAddVerAdjunto", "FuncionModalAddVerAdjunto();", True)
            End If
        End If
    End Sub
    Public Sub BuscarNotificaciones()
        Dim dt As DataTable = ObjUniver.Notificaciones(0, ViewState("SolicitudID"), "", "Buscar")
        If dt.Rows.Count > 0 Then
            If dt.Rows.Count > 0 Then
                Me.Session("dtDatos") = dt
                GVNotificaiones.DataSource = dt
                GVNotificaiones.DataBind()
            Else
                Me.Session("dtDatos") = Nothing
                GVNotificaiones.DataSource = Nothing
                GVNotificaiones.DataBind()
            End If
        End If
    End Sub
    Protected Sub lbExportar_Click(sender As Object, e As EventArgs) Handles lbExportar.Click

        Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder
        With strBuilder
            .Append("<script language='javascript'>")
            .Append("window.open('" & "ExportInfo.aspx', '');")
            .Append("</script>")
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), " ", strBuilder.ToString, False)
        End With
    End Sub

End Class