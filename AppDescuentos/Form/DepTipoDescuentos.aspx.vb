﻿Public Class DepTipoDescuentos
    Inherits System.Web.UI.Page
    Dim ObjUniver As New CLUniversal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            TipoDescuentos()
            Dependencias()
            Buscar()
        End If
    End Sub
    Public Sub Buscar()
        Dim dt As DataTable = ObjUniver.CRUBDepTipoDescuentos(0, 0, ddlTipoDescuentos.SelectedValue, "Buscar")
        If dt.Rows.Count > 0 Then
            Session("Documentos") = dt
            GVDocumentos.DataSource = dt
            GVDocumentos.DataBind()
            Alerta1.Visible = False
        Else
            Session("Documentos") = Nothing
            GVDocumentos.DataSource = Nothing
            GVDocumentos.DataBind()
            Alerta1.Visible = True
        End If
    End Sub
    Public Sub TipoDescuentos()
        Dim dt As DataTable = ObjUniver.CRUBTipoDescuento(0, "", "", 0, 1, "Buscar")

        If dt.Rows.Count > 0 Then
            With Me.ddlTipoDescuentos
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuentos.SelectedValue = -1

            With Me.ddlTipoDescuentosAdd
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "TipoDescuentoID"
                .DataTextField = "Nombre"
                .DataBind()
                .Items.Add(New ListItem("Seleccione tipo descuento", "-1"))
            End With

            ddlTipoDescuentosAdd.SelectedValue = -1

        End If
    End Sub
    Public Sub Dependencias()
        Dim dt As DataTable = ObjUniver.CRUBDependencias(0, Me.Session("SedeCod"), "", "", "", "", 0, 1, "Buscar")

        If dt.Rows.Count > 0 Then
            With Me.ddlDependenciaAdd
                .Items.Clear()
                .DataSource = dt
                .DataValueField = "DependenciaID"
                .DataTextField = "NombreDepen"
                .DataBind()
                .Items.Add(New ListItem("Seleccione dependencia", "-1"))
            End With

            ddlDependenciaAdd.SelectedValue = -1

        End If
    End Sub

    Protected Sub lbNewDepTipoDescuentos_Click(sender As Object, e As EventArgs) Handles lbNewDepTipoDescuentos.Click
        ddlDependenciaAdd.SelectedValue = -1
        ddlTipoDescuentosAdd.SelectedValue = -1
        ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModaladdDepTipoDescuentos", "FuncionModaladdDepTipoDescuentos();", True)
    End Sub
    Protected Sub lbAddDepTipoDescuentos_Click(sender As Object, e As EventArgs) Handles lbAddDepTipoDescuentos.Click
        If ddlDependenciaAdd.SelectedValue = -1 Or ddlTipoDescuentosAdd.SelectedValue = -1 Then
            ObjUniver.SendNotify("Dependencia y tipo descuento es obligatorio", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModaladdDepTipoDescuentos", "FuncionModaladdDepTipoDescuentos();", True)
            Exit Sub
        End If

        Dim dt1 As DataTable = ObjUniver.CRUBDepTipoDescuentos(0, ddlDependenciaAdd.SelectedValue, ddlTipoDescuentosAdd.SelectedValue, "Validar")
        If dt1.Rows.Count > 0 Then
            ObjUniver.SendNotify("ya existe Dependencia y tipo descuento seleccionados", "3")
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType, "FuncionModaladdDepTipoDescuentos", "FuncionModaladdDepTipoDescuentos();", True)
            Exit Sub
        End If

        Dim dt As DataTable = ObjUniver.CRUBDepTipoDescuentos(0, ddlDependenciaAdd.SelectedValue, ddlTipoDescuentosAdd.SelectedValue, "Crear")

        If dt.Rows.Count > 0 Then
            ObjUniver.SendNotify("Se creó el registro con éxito", "1")
            Buscar()
        End If
    End Sub

    Protected Sub GVDocumentos_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles GVDocumentos.RowDeleting
        Dim ID As Integer = Me.GVDocumentos.DataKeys(e.RowIndex).Value

        Dim dt As DataTable = ObjUniver.CRUBDepTipoDescuentos(ID, 0, 0, "Eliminar")

        ObjUniver.SendNotify(" Registro se eliminó con éxito  ", "1")
        Buscar()
    End Sub

    Protected Sub lbBuscar_Click(sender As Object, e As EventArgs) Handles lbBuscar.Click
        Buscar()
    End Sub


End Class