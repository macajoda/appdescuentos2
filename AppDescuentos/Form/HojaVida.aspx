﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Principal.Master" CodeBehind="HojaVida.aspx.vb" Inherits="KidsPlenitud.HojaVida" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <script>
        function FuncionModalAddHojaVida() {
            $("#ModaladdHojaVida").modal("show")
        }
        function FuncionModalUpdHojaVida() {
            $("#ModalUpdHojaVida").modal("show")
        }
    </script>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="main_title" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Header" runat="server">
</asp:Content>--%>
<asp:Content ID="Content5" ContentPlaceHolderID="Main_Texto" runat="server">
</asp:Content>
<%--<asp:Content ID="Content6" ContentPlaceHolderID="navbar" runat="server">
</asp:Content>--%>
<asp:Content ID="Content7" ContentPlaceHolderID="Main_pagination" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="Modulos.aspx">Inicio</a></li>
        <li class="breadcrumb-item active">Hoja de vida</li>
    </ol>
</asp:Content>
<%--<asp:Content ID="Content8" ContentPlaceHolderID="main_content" runat="server">
</asp:Content>--%>
<asp:Content ID="Content9" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Panel ID="pnHojaVida" runat="server" Visible="true">
        <div class="panel panel-default" id="xxx" runat="server">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-8">
                        Hoja de Vida
                    </div>
                    <div class="col-lg-4" style="text-align: right">
                        <asp:LinkButton ID="lbAddHojaVida" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-plus"></span>&nbsp;Nuevo 
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Cuadro de Consulta</strong>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        Nombre
                         <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control">
                         </asp:TextBox>
                    </div>
                    <div class="col-lg-4">
                        Acudiente	
                         <asp:TextBox ID="txtAcudiente" runat="server" CssClass="form-control">
                         </asp:TextBox>
                    </div>
                    <div class="col-lg-2">
                        Edad	
                         <asp:TextBox ID="txtEdad" runat="server" CssClass="form-control">
                         </asp:TextBox>
                    </div>
                    <div class="col-lg-2">
                        Estado	
                         <asp:DropDownList ID="ddlEstadoConsul" runat="server" CssClass="form-control" Style="text-transform: uppercase">
                             <asp:ListItem Value="1" Selected="True">Activo</asp:ListItem>
                             <asp:ListItem Value="0">Inactivo</asp:ListItem>
                             <%--<asp:ListItem Value="2">Todos</asp:ListItem>--%>
                         </asp:DropDownList>
                    </div>
                </div>


                <div class="form-group row">

                    <div class="row">
                        <div class="col-lg-12" style="text-align: center">
                            <asp:LinkButton ID="lbBuscar" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-search"></span>&nbsp;Buscar 
                            </asp:LinkButton>
                            &nbsp; &nbsp;
                            <asp:LinkButton ID="lbDescargar" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-file-excel-o"></span>&nbsp;Descargar 
                            </asp:LinkButton>
                            &nbsp; &nbsp;
                             <asp:LinkButton ID="lbAsistencia" runat="server" CssClass="btn btn-info" OnClientClick="ShowBlockWindow();"> 
                            <span class="fa fa-file-text-o"></span>&nbsp;Asistencia 
                            </asp:LinkButton>
                        </div>
                        
                    </div>
                </div>
                <br />
                <div class="form-group row" style="background-color: #F2F2F2">
                    <div class="col-lg-12">
                        <strong>Listado</strong>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <asp:GridView ID="GVHojaVida" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CssClass="table table-striped table-bordered table-hover" Font-Size="Small" AllowPaging="True"
                            DataKeyNames="HojaVidaId,SwActivo">
                            <HeaderStyle BackColor="#E6E6E6" Font-Bold="True" ForeColor="#424242" />
                            <PagerStyle HorizontalAlign="Center" />
                            <EditRowStyle BackColor="#ffffcc" />
                            <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                            <Columns>
                                <asp:BoundField DataField="HojaVidaId" HeaderText="Cod. Interno">
                                    <ItemStyle Width="5%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Nombre" HeaderText="Nombre">
                                    <ItemStyle Width="35%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Edad" HeaderText="Edad">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Acudiente" HeaderText="Acudiente">
                                    <ItemStyle Width="30%" />
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="lEstado" runat="server" Text='<%# Bind("SwActivo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Acciones">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbEditar" runat="server" CausesValidation="False" CommandName="EditarHojaVida" CssClass="btn btn-primary" Text=""
                                            data-toggle="tooltip" title="Editar Datos" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-pencil-square-o"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                         <asp:LinkButton ID="lbFoto" runat="server" CausesValidation="False" CommandName="TomarFoto" CssClass="btn btn-primary" Text=""
                                            data-toggle="tooltip" title="Tomar Foto" OnClientClick="ShowBlockWindow();" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'> 
                                            <span class="fa fa-address-book"></span>                                        
                                        </asp:LinkButton>
                                        &nbsp;
                                          <asp:LinkButton ID="lbEliminar" runat="server" CommandName="Delete" CssClass="btn btn-danger" Text="Error"
                                              data-toggle="tooltip" title="Activar/Desactivar" OnClientClick="return deletealert2(this, event);">
                                                <span class="fa fa-trash-o fa-lg"></span>
                                          </asp:LinkButton>
                                        <%-- ============ Alerta de confirmación eliminación de registro ============ --%>
                                        <script>
                                            function deletealert2(ctl, event) {
                                                // STORE HREF ATTRIBUTE OF LINK CTL (THIS) BUTTON
                                                var defaultAction = $(ctl).prop("href");
                                                // CANCEL DEFAULT LINK BEHAVIOUR
                                                event.preventDefault();
                                                swal({
                                                    title: "¿Está Seguro de cambiar el estado de este registro?",
                                                    text: "",
                                                    type: "warning",
                                                    showCancelButton: true,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Si, ¡Cambiar estado!",
                                                    cancelButtonText: "No, cancelar",
                                                    closeOnConfirm: false,
                                                    //closeOnCancel: false
                                                },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        //swal({ title: "Eliminado!", text: "Tu archivo imaginario ha sido eliminado.", type: "success", confirmButtonText: "OK!", closeOnConfirm: false },
                                                        //function () {
                                                        //    // RESUME THE DEFAULT LINK ACTION
                                                        window.location.href = defaultAction;
                                                        return true;
                                                        //});                                                            
                                                    } else {
                                                        //swal("Cancelado", "Tu archivo imaginario es seguro :)", "error");
                                                        return false;
                                                    }
                                                });
                                            }
                                        </script>
                                        <%-- ============ FIN Alerta de confirmación eliminación de registro ============ --%>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="alert alert-info" role="alert" id="Alerta1" runat="server" visible="false">¡Aviso! No hay resultados para mostrar....</div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>



    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-danger" id="pnMensajeSinAcceso" runat="server" visible="false">
        <strong>
            <asp:Label ID="lMensaje1" runat="server" Text="Sin acceso"></asp:Label></strong><br />
        <strong>
            <asp:Label ID="lMensaje2" runat="server" Text="Atención!"></asp:Label></strong>
        <asp:Label ID="lMensajeDanger" runat="server" Text="No tienes acceso a este módulo consulta con el administrador"></asp:Label>
    </div>

    <%-- Manejo de mensajes alertas --%>
    <div class="alert alert-warning" id="pnAlert" runat="server" visible="false" style="text-align: left">
        <div class="form-group row">
            <div class="col-lg-1" style="text-align: right">
                <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
            </div>
            <div class="col-lg-11">
                <strong>
                    <asp:Label ID="Label2" runat="server" Text="Label:"></asp:Label></strong><br />
                <strong>
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></strong>
                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>

    <!-- Modal Para agregar HojaVida-->
    <div id="ModaladdHojaVida" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Crear Hoja de Vida Niño</strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-7">
                            Nombre del niño
                            <asp:TextBox ID="txtNombreAdd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            Fecha nacimiento
                                     <div class='input-group date' id='rdpFechaNacimiento'>
                                         <input type='text' class="form-control" id="dtpFechaNacimiento" runat="server" placeholder="dd/mm/aaaa"
                                             data-toggle="tooltip" title="Formato dd/mm/aaaa" data-placement="left" />
                                         <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                     </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#rdpFechaNacimiento').datetimepicker({
                                        format: 'DD/MM/YYYY'
                                    });
                                });
                            </script>
                        </div>
                        <div class="col-lg-2">
                            Edad
                            <asp:TextBox ID="txtEdadAdd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase" pattern="^[0-9]*$"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            Acudiente
                            <asp:TextBox ID="txtAcudienteAdd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            Telefono
                            <asp:TextBox ID="txtTelefonoAdd" runat="server" CssClass="form-control"> </asp:TextBox>
                        </div>
                        <div class="col-lg-5">
                            Dirección
                            <asp:TextBox ID="txtDireccionAdd" runat="server" CssClass="form-control" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <asp:LinkButton ID="lbAddHojaVid" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Agregar </asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lbTomarFoto" runat="server" CssClass="btn btn-success" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Tomar Foto </asp:LinkButton>
                        &nbsp;&nbsp;
                    <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Para Editar HojaVida-->
    <div id="ModalUpdHojaVida" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>Modificar Hoja de Vida Niño</strong>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-lg-7">
                            Nombre del niño
                            <asp:TextBox ID="txtNombreUpd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            Fecha nacimiento
                                     <div class='input-group date' id='rdpFechaNacimientoUpd'>
                                         <input type='text' class="form-control" id="dpFechaNacimientoUpd" runat="server" placeholder="dd/mm/aaaa"
                                             data-toggle="tooltip" title="Formato dd/mm/aaaa" data-placement="left" />
                                         <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                     </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#rdpFechaNacimientoUpd').datetimepicker({
                                        format: 'DD/MM/YYYY'
                                    });
                                });
                            </script>
                        </div>
                        <div class="col-lg-2">
                            Edad
                            <asp:TextBox ID="txtEdadUpd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase" pattern="^[0-9]*$"> </asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            Acudiente
                            <asp:TextBox ID="txtAcudienteUpd" runat="server" CssClass="form-control" required="" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            Telefono
                            <asp:TextBox ID="txtTelefonoUpd" runat="server" CssClass="form-control"> </asp:TextBox>
                        </div>
                        <div class="col-lg-5">
                            Dirección
                            <asp:TextBox ID="txtDireccionUpd" runat="server" CssClass="form-control" Style="text-transform: uppercase"> </asp:TextBox>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <asp:LinkButton ID="lbUpdHojaVida" runat="server" CssClass="btn btn-primary" OnClientClick="ShowBlockWindow();"> <%--<span class="fa fa-plus"></span>--%>&nbsp;Actualizar </asp:LinkButton>
                        &nbsp;&nbsp;
                    <a href="#" data-dismiss="modal" class="btn btn-danger">Salir</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
