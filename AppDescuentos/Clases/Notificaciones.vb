﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Net.Mail

Public Class Notificaciones

    Public Shared Function enviarNotificacionOlvidoClave(ByVal Responsable As String, ByVal NombreResponsable As String, ByVal DatosMensaje As String, ByVal ObjServer As HttpServerUtility) As Boolean

        Try

            If ConfigurationManager.AppSettings("enviarCorreos") Then
                'Responsable = "caritokceres@hotmail.com"
                Dim smtpServer As String = ConfigurationManager.AppSettings("smtpServer")
                Dim userSmtp As String = ConfigurationManager.AppSettings("userSmtp")
                Dim passSmtp As String = ConfigurationManager.AppSettings("passSmtp")

                Dim Asunto As String = "Aplicativo educación continua UDES - Olvido de Clave"

                Dim rutaPlantillaCorreo As String = ObjServer.MapPath("~\PlantillasCorreos\olvidoClave.html")
                Dim contenidoCorreo As String = File.ReadAllText(rutaPlantillaCorreo)

                contenidoCorreo = contenidoCorreo.Replace("@DESTINATARIO", "Cordial Saludo ,  " & NombreResponsable)
                contenidoCorreo = contenidoCorreo.Replace("@MENSAJE", DatosMensaje)

                Dim mensaje As New System.Net.Mail.MailMessage(userSmtp, Responsable, Asunto, contenidoCorreo)
                mensaje.IsBodyHtml = True

                Dim clienteSMTP As New System.Net.Mail.SmtpClient(smtpServer, 587)
                clienteSMTP.EnableSsl = True
                clienteSMTP.Credentials = New NetworkCredential(userSmtp, passSmtp)
                clienteSMTP.Send(mensaje)



                'Dim objLogCorreos As New LogCorreos
                'objLogCorreos.Registrar(ConfigurationManager.AppSettings("userSmtp"),
                '                    Responsable, "Cordial Saludo ,  " & NombreResponsable & " <br/><br/>" & DatosMensaje,
                '                    "Aplicativo Pedidos Almacen UDES - Olvido de Clave",
                '                    1,
                '                    "",
                '                    ObjServer)


                Return True

            Else

                'Dim objLogCorreos As New LogCorreos
                'objLogCorreos.Registrar(ConfigurationManager.AppSettings("userSmtp"),
                '                        Responsable, "Cordial Saludo ,  " & NombreResponsable & " <br/><br/>" & DatosMensaje,
                '                        "Aplicativo Pedidos Almacen UDES - Olvido de Clave",
                '                        0,
                '                        "La Etiqueta del web.config enviarCorreos esta con valor False, no esta autorizado el envio de mensajes",
                '                        ObjServer)

                Return True

            End If


        Catch ex As Exception

            'Dim objLogCorreos As New LogCorreos
            'objLogCorreos.Registrar(ConfigurationManager.AppSettings("userSmtp"),
            '                        Responsable,
            '                        "Cordial Saludo ,  " & NombreResponsable & " <br/><br/>" & DatosMensaje,
            '                        "Aplicativo Pedidos Almacen UDES - Olvido de Clave",
            '                        0,
            '                        ex.Message.ToString(),
            '                        ObjServer)

            Return False

        End Try

    End Function

    Public Shared Function EnvioCorreos(ByVal CorreoResponsable As String,
                                        ByVal NombreResponsable As String,
                                        ByVal Asunto As String,
                                        ByVal DatosMensaje As String,
                                        ByVal ObjServer As HttpServerUtility) As Boolean


        Try

            If ConfigurationManager.AppSettings("enviarCorreos") Then
                'Responsable = "caritokceres@hotmail.com"
                Dim smtpServer As String = ConfigurationManager.AppSettings("smtpServer")
                Dim userSmtp As String = ConfigurationManager.AppSettings("userSmtp")
                Dim passSmtp As String = ConfigurationManager.AppSettings("passSmtp")


                Dim rutaPlantillaCorreo As String = ObjServer.MapPath("~\PlantillasCorreos\Notificaciones.html")
                Dim contenidoCorreo As String = File.ReadAllText(rutaPlantillaCorreo)

                contenidoCorreo = contenidoCorreo.Replace("@DESTINATARIO", "Reciba un cordial saludo,  " & NombreResponsable)
                contenidoCorreo = contenidoCorreo.Replace("@MENSAJE1", DatosMensaje)


                Dim mensaje As New System.Net.Mail.MailMessage(userSmtp, CorreoResponsable, Asunto, contenidoCorreo)
                mensaje.IsBodyHtml = True

                Dim clienteSMTP As New System.Net.Mail.SmtpClient(smtpServer, 587)
                clienteSMTP.EnableSsl = True
                clienteSMTP.Credentials = New NetworkCredential(userSmtp, passSmtp)
                clienteSMTP.Send(mensaje)

                Return True

            Else

                'Dim objLogCorreos As New LogCorreos
                'objLogCorreos.Registrar(ConfigurationManager.AppSettings("userSmtp"),
                '                        Responsable, "Cordial Saludo ,  " & NombreResponsable & " <br/><br/>" & DatosMensaje,
                '                        "Aplicativo Pedidos Almacen UDES - Olvido de Clave",
                '                        0,
                '                        "La Etiqueta del web.config enviarCorreos esta con valor False, no esta autorizado el envio de mensajes",
                '                        ObjServer)

                Return False

            End If


        Catch ex As Exception
            Dim Error1 As String = ex.Message.ToString()

            Dim Prueba As String = ""
            'Dim objLogCorreos As New LogCorreos
            'objLogCorreos.Registrar(ConfigurationManager.AppSettings("userSmtp"),
            '                        Responsable,
            '                        "Cordial Saludo ,  " & NombreResponsable & " <br/><br/>" & DatosMensaje,
            '                        "Aplicativo Pedidos Almacen UDES - Olvido de Clave",
            '                        0,
            '                        ex.Message.ToString(),
            '                        ObjServer)

            Return False

        End Try

    End Function

    Public Shared Function enviarConAdjuntos(ByVal Logo As String,
                                             ByVal CorreoResponsable As String,
                                             ByVal NombreResponsable As String,
                                             ByVal Asunto As String,
                                             ByVal DatosMensaje1 As String,
                                             ByVal DatosMensaje2 As String,
                                             ByVal DatosMensaje3 As String,
                                             ByVal Adjunto As [Byte](),
                                             ByVal ObjServer As HttpServerUtility) As Boolean


        Try
            If ConfigurationManager.AppSettings("enviarCorreos") Then

                'Responsable = "caritokceres@hotmail.com"
                Dim smtpServer As String = ConfigurationManager.AppSettings("smtpServer")
                Dim userSmtp As String = ConfigurationManager.AppSettings("userSmtp")
                Dim passSmtp As String = ConfigurationManager.AppSettings("passSmtp")


                Dim rutaPlantillaCorreo As String = ObjServer.MapPath("~\PlantillasCorreos\Diploma.html")
                Dim contenidoCorreo As String = File.ReadAllText(rutaPlantillaCorreo)

                contenidoCorreo = contenidoCorreo.Replace("@IMAGEN", Logo)
                contenidoCorreo = contenidoCorreo.Replace("@DESTINATARIO", "Reciba un cordial saludo,  " & NombreResponsable)
                contenidoCorreo = contenidoCorreo.Replace("@MENSAJE1", DatosMensaje1)
                contenidoCorreo = contenidoCorreo.Replace("@MENSAJE2", DatosMensaje2)
                contenidoCorreo = contenidoCorreo.Replace("@MENSAJE3", DatosMensaje3)

                Dim mensaje As New System.Net.Mail.MailMessage(userSmtp, CorreoResponsable, Asunto, contenidoCorreo)
                mensaje.Attachments.Add(New Attachment(New MemoryStream(Adjunto), "Diploma.pdf"))
                mensaje.IsBodyHtml = True

                Dim clienteSMTP As New System.Net.Mail.SmtpClient(smtpServer, 587)
                clienteSMTP.EnableSsl = True
                clienteSMTP.Credentials = New NetworkCredential(userSmtp, passSmtp)
                clienteSMTP.Send(mensaje)

            End If

            Return True
        Catch ex As Exception


            Return False

        End Try

    End Function

End Class
