﻿Imports AppDescuentos.DataLayer

Public Class CLUniversal
    Public Function Usuario(ByVal VarString1 As String,
                            ByVal VarString2 As String,
                            ByVal VarString3 As String,
                            ByVal VarString4 As String,
                            ByVal VarString5 As String,
                            ByVal VarInteger1 As Integer,
                            ByVal VarInteger2 As Integer,
                            ByVal VarInteger3 As Integer,
                            ByVal VarInteger4 As Integer,
                            ByVal VarInteger5 As Integer,
                            ByVal Busqueda As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@VarString1", VarString1, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarString2", VarString2, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarString3", VarString3, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarString4", VarString4, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarString5", VarString5, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarInteger1", VarInteger1, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarInteger2", VarInteger2, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarInteger3", VarInteger3, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarInteger4", VarInteger4, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@VarInteger5", VarInteger5, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Busqueda", Busqueda, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_Usuarios", ObjCollection)
        Return dt
    End Function
    Public Function CRUBTipoDescuento(ByVal TipoDescuentoID As Integer,
                               ByVal Nombre As String,
                               ByVal Observacion As String,
                               ByVal Porcentaje As Decimal,
                               ByVal SwActivo As Integer,
                               ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@TipoDescuentoID", TipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Nombre", Nombre, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Observacion", Observacion, SqlDbType.NVarChar, 5000)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Porcentaje", Porcentaje, SqlDbType.Decimal)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SwActivo", SwActivo, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_TipoDescuento", ObjCollection)
        Return dt
    End Function
    Public Function ValidarNombre(ByVal SedeCod As String,
                               ByVal Nombre As String,
                               ByVal Id As Integer,
                               ByVal Filtro As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@SedeCod", SedeCod, SqlDbType.NVarChar, 5)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Nombre", Nombre, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Id", Id, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Filtro", Filtro, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_ValidarNombre", ObjCollection)
        Return dt
    End Function
    Public Function CRUBDependencias(ByVal DependenciaID As Integer,
                               ByVal SedeCod As String,
                               ByVal CodigoDep As String,
                               ByVal NombreDepen As String,
                               ByVal NombrePersona As String,
                               ByVal Correo As String,
                               ByVal Administrador As Integer,
                               ByVal SwActivo As Integer,
                               ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@DependenciaID", DependenciaID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SedeCod", SedeCod, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@CodigoDep", CodigoDep, SqlDbType.NVarChar, 10)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@NombreDepen", NombreDepen, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@NombrePersona", NombrePersona, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Correo", Correo, SqlDbType.NVarChar, 200)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Administrador", Administrador, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SwActivo", SwActivo, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_Dependencias", ObjCollection)
        Return dt
    End Function
    Public Function CRUBDocumentosDescuento(ByVal DocumentosDescuentoID As Integer,
                             ByVal Nombre As String,
                             ByVal Obligatorio As Integer,
                             ByVal SwActivo As Integer,
                             ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@DocumentosDescuentoID", DocumentosDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Nombre", Nombre, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Obligatorio", Obligatorio, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SwActivo", SwActivo, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_DocumentosDescuento", ObjCollection)
        Return dt
    End Function
    Public Function CRUBEstados(ByVal EstadoID As Integer,
                              ByVal Nombre As String,
                              ByVal SwActivo As Integer,
                              ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@EstadoID", EstadoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Nombre", Nombre, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SwActivo", SwActivo, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_Estados", ObjCollection)
        Return dt
    End Function
    Public Function CRUBTipoContrato(ByVal TipoContratoID As Integer,
                             ByVal Nombre As String,
                             ByVal SwActivo As Integer,
                             ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@TipoContratoID", TipoContratoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Nombre", Nombre, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SwActivo", SwActivo, SqlDbType.Bit)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_TipoContrato", ObjCollection)
        Return dt
    End Function

    Public Function CRUBSolicitudDocumentos(ByVal SolicitudDocumentoID As Integer,
                            ByVal SolicitudID As Integer,
                            ByVal DocumentosDescuentoID As Integer,
                            ByVal RutaArchivo As String,
                            ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@SolicitudDocumentoID", SolicitudDocumentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SolicitudID", SolicitudID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@DocumentosDescuentoID", DocumentosDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@RutaArchivo", RutaArchivo, SqlDbType.NVarChar, 5000)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_SolicitudDocumentos", ObjCollection)
        Return dt
    End Function
    Public Function Notificaciones(ByVal NotificacionID As Integer,
                           ByVal SolicitudID As Integer,
                           ByVal Observacion As String,
                           ByVal Accion As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@NotificacionID", NotificacionID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SolicitudID", SolicitudID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Observacion", Observacion, SqlDbType.NVarChar, 2000)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_Notificaciones", ObjCollection)
        Return dt
    End Function

    Public Function CRUDSolicitudes(ByVal SolicitudID As Integer,
                                ByVal Periodo As String,
                                ByVal EstudianteID As Integer,
                                ByVal TipoDescuentoID As Integer,
                                ByVal EstadoID As Integer,
                                ByVal Asignatura As String,
                                ByVal TotalCreditos As String,
                                ByVal TipoContratoID As Integer,
                                ByVal ProgPertenece As String,
                                ByVal NombreEmpleado As String,
                                ByVal ProgAdscrito As String,
                                ByVal ProgramaSimultaneo As String,
                                ByVal CodigoSimultanea As String,
                                ByVal Accion As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@SolicitudID", SolicitudID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Periodo", Periodo, SqlDbType.NVarChar, 15)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@EstudianteID", EstudianteID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoDescuentoID", TipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@EstadoID", EstadoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Asignatura", Asignatura, SqlDbType.NVarChar, 200)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TotalCreditos", TotalCreditos, SqlDbType.NVarChar, 10)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoContratoID", TipoContratoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@ProgPertenece", ProgPertenece, SqlDbType.NVarChar, 5)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@NombreEmpleado", NombreEmpleado, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@ProgAdscrito", ProgAdscrito, SqlDbType.NVarChar, 5)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@ProgramaSimultaneo", ProgramaSimultaneo, SqlDbType.NVarChar, 200)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@CodigoSimultanea", CodigoSimultanea, SqlDbType.NVarChar, 50)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_Solicitudes", ObjCollection)
        Return dt
    End Function
    Public Function CRUBSolicitudDependencia(ByVal SolicitudDependenciaID As Integer,
                               ByVal SolicitudID As Integer,
                               ByVal DependenciaID As Integer,
                               ByVal ObservacionCyC As String,
                               ByVal ObservacionDep As String,
                               ByVal FechaObservDep As String,
                               ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@SolicitudDependenciaID", SolicitudDependenciaID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SolicitudID", SolicitudID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@DependenciaID", DependenciaID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@ObservacionCyC", ObservacionCyC, SqlDbType.NVarChar, 3000)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@ObservacionDep", ObservacionDep, SqlDbType.NVarChar, 3000)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@FechaObservDep", FechaObservDep, SqlDbType.NVarChar, 10)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_SolicitudDependencia", ObjCollection)
        Return dt
    End Function
    Public Function SolicitudSeguimiento(ByVal DependenciaID As Integer,
                              ByVal TipoDescuentoID As Integer,
                              ByVal EstadoID As Integer,
                              ByVal Periodo As String,
                              ByVal Administrador As String,
                              ByVal Accion As String) As DataTable
        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@DependenciaID", DependenciaID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoDescuentoID", TipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@EstadoID", EstadoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Periodo", Periodo, SqlDbType.NVarChar, 15)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Administrador", Administrador, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_SolicitudSeguimiento", ObjCollection)
        Return dt
    End Function

    Public Function CRUBDocTipoDescuentos(ByVal DocTipoDescuentoID As Integer,
                             ByVal DocumentosDescuentoID As Integer,
                             ByVal TipoDescuentoID As Integer,
                             ByVal Accion As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@DocTipoDescuentoID", DocTipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@DocumentosDescuentoID", DocumentosDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoDescuentoID", TipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_DocTipoDescuentos", ObjCollection)
        Return dt
    End Function

    Public Function CRUBDepTipoDescuentos(ByVal DepTipoDescuentoID As Integer,
                            ByVal DependenciaID As Integer,
                            ByVal TipoDescuentoID As Integer,
                            ByVal Accion As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@DepTipoDescuentoID", DepTipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@DependenciaID", DependenciaID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoDescuentoID", TipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_DepTipoDescuentos", ObjCollection)
        Return dt
    End Function
    Public Function LoginDep(ByVal Correo As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@Correo", Correo, SqlDbType.NVarChar, 200)
        ObjCollection.Add(objParametro)


        Dim dt As DataTable = Datos.GetDatatable("Sp_LoginDep", ObjCollection)
        Return dt
    End Function
    Public Function CRUDEstudiantes(ByVal EstudianteID As Integer,
                                ByVal Nombre As String,
                                ByVal SedeCod As String,
                                ByVal ProgCod As String,
                                ByVal EstudCod As String,
                                ByVal Correo As String,
                                ByVal TipoDocumento As String,
                                ByVal Documento As String,
                                ByVal Departamento As String,
                                ByVal Ciudad As String,
                                ByVal Direccion As String,
                                ByVal Barrio As String,
                                ByVal Telefono As String,
                                ByVal Filtro As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@EstudianteID", EstudianteID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Nombre", Nombre, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@SedeCod", SedeCod, SqlDbType.NVarChar, 5)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@ProgCod", ProgCod, SqlDbType.NVarChar, 5)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@EstudCod", EstudCod, SqlDbType.NVarChar, 20)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Correo", Correo, SqlDbType.NVarChar, 100)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoDocumento", TipoDocumento, SqlDbType.NVarChar, 50)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Documento", Documento, SqlDbType.NVarChar, 20)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Departamento", Departamento, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Ciudad", Ciudad, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Direccion", Direccion, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Barrio", Barrio, SqlDbType.NVarChar, 500)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Telefono", Telefono, SqlDbType.NVarChar, 20)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Filtro", Filtro, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_Estudiantes", ObjCollection)
        Return dt
    End Function
    Public Function LlenarDropDownLists(ByVal SedeCod As String, ByVal Accion As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@SedeCod", SedeCod, SqlDbType.NVarChar, 5)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("Sp_DropDownLists", ObjCollection)
        Return dt
    End Function
    Public Function ValidarAdjuntos(ByVal SolicitudID As Integer,
                            ByVal TipoDescuentoID As Integer,
                            ByVal Accion As String) As DataTable

        Dim ObjCollection As New Collection
        Dim objParametro As New SpParametro("@SolicitudID", SolicitudID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@TipoDescuentoID", TipoDescuentoID, SqlDbType.Int)
        ObjCollection.Add(objParametro)
        objParametro = New SpParametro("@Accion", Accion, SqlDbType.NVarChar, 30)
        ObjCollection.Add(objParametro)

        Dim dt As DataTable = Datos.GetDatatable("SP_ValidarAdjuntos", ObjCollection)
        Return dt
    End Function


    ''::: Validaciones y Alertas :::
    Public Function ComprobarFormaFecha(FechaComprobar As String) As Boolean
        Dim sFormato As [String]
        sFormato = "^(?:(?:0?[1-9]|1\d|2[0-8])(\/|-)(?:0?[1-9]|1[0-2]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:31(\/|-)(?:0?[13578]|1[02]))|(?:(?:29|30)(\/|-)(?:0?[1,3-9]|1[0-2])))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(29(\/|-)0?2)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$"
        If Regex.IsMatch(FechaComprobar, sFormato) Then
            If Regex.Replace(FechaComprobar, sFormato, [String].Empty).Length = 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function ComprobarFormatoLetras(seLETRASComprobar As String) As Boolean
        Dim sFormato As [String]
        sFormato = "^[A-Za-z]*$"
        If Regex.IsMatch(seLETRASComprobar, sFormato) Then
            If Regex.Replace(seLETRASComprobar, sFormato, [String].Empty).Length = 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function ComprobarFormatoLetrasNumeros(seLETRAS_NUMEROSComprobar As String) As Boolean
        Dim sFormato As [String]
        sFormato = "^[0-9A-Za-z]*$"
        If Regex.IsMatch(seLETRAS_NUMEROSComprobar, sFormato) Then
            If Regex.Replace(seLETRAS_NUMEROSComprobar, sFormato, [String].Empty).Length = 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function ComprobarFormatoNombres(seNOMBRESComprobar As String) As Boolean
        Dim sFormato As [String]
        sFormato = "^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"
        If Regex.IsMatch(seNOMBRESComprobar, sFormato) Then
            If Regex.Replace(seNOMBRESComprobar, sFormato, [String].Empty).Length = 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function ComprobarFormatoNumeros(seNUMEROSComprobar As String) As Boolean
        Dim sFormato As [String]
        sFormato = "^[0-9]*$"
        If Regex.IsMatch(seNUMEROSComprobar, sFormato) Then
            If Regex.Replace(seNUMEROSComprobar, sFormato, [String].Empty).Length = 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function ComprobarFormatoEmail(seMailAComprobar As String) As Boolean
        Dim sFormato As [String]
        sFormato = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        If Regex.IsMatch(seMailAComprobar, sFormato) Then
            If Regex.Replace(seMailAComprobar, sFormato, [String].Empty).Length = 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Sub SendNotify(msg As String, noti As String)
        With System.Web.HttpContext.Current.Session
            .Add("NombreVariable", "Notificacion")
            .Add("Msg", msg)
            .Add("TipoNotificacion", noti)
        End With
    End Sub
End Class
