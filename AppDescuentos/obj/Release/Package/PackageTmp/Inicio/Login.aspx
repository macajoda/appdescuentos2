﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PlantillasMaster/Secundario.Master" CodeBehind="Login.aspx.vb" Inherits="AppDescuentos.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<%--<asp:Content ID="Content3" ContentPlaceHolderID="main_title" runat="server">
</asp:Content>--%>

<%--<asp:Content ID="Content4" ContentPlaceHolderID="Header" runat="server">
</asp:Content>--%>

<asp:Content ID="Content5" ContentPlaceHolderID="Main_Texto" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="navbar" runat="server">
    <div style="background-color: #1C4C8A">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <%--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>--%>
                    <a class="navbar-brand" href="" title="Manual" data-toggle="modal" data-target="#myModal"><i class="fa fa-book"></i></a>
                    <%--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--%>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <%--<li id="li1" runat="server"><a href="../ConsultaDiploma/Validar.aspx" target="_blank">Validar diploma</a></li>
                        <li id="li2" runat="server"><a href="../Inscripcion/Inicio.aspx" target="_blank">Proceso de Inscripción </a></li>--%>
                    </ul>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="Main_pagination" runat="server">
</asp:Content>

<%--<asp:Content ID="Content8" ContentPlaceHolderID="main_content" runat="server">
</asp:Content>--%>

<asp:Content ID="Content9" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row" style="margin-top: 50px">
        <div class="col-md-8">
            <div class="jumbotron" style="height: 250px">
                <h2>Solicitud descuentos</h2>
                <p align="justify">
                    <%--Sistema en el cual podrá realizar la gestión de los eventos de educación continua:
                     generar diplomas, consultar reportes de inscritos y pagos, generar evaluación de satisfacción, 
                    generar informes, hacer seguimiento del evento y validar asistencia.--%>
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="modal-content" style="height: 250px">
                <div class="panel-heading">
                    <h3 class="panel-title">Inicio de sesión</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div style="margin-left: 10px; margin-right: 10px">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="background-color: #d3d3d3"><i class="fa fa-user fa-1x"></i></span>
                                <asp:TextBox ID="txtUser" runat="server" CssClass="form-control" placeholder="Usuario"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div style="margin-left: 10px; margin-right: 10px">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon2" style="background-color: #d3d3d3"><i class="fa fa-unlock fa-1x"></i></span>
                                <asp:TextBox ID="txtPass" runat="server" CssClass="form-control" placeholder="Contraseña" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <div style="margin-left: 10px; margin-right: 10px; text-align: right">
                                    <asp:LinkButton ID="lbOlvido" runat="server">¿Olvidó su clave?</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center">
                        <asp:LinkButton ID="lbIngresar" runat="server" CssClass="btn btn-success" Text="Ingresar" Height="30px" Width="200px"
                            OnClientClick="ShowBlockWindow();"> &nbsp;Ingresar&nbsp;<span class="fa fa-sign-in"></span> 
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
